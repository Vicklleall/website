function objInit(o,p,s,x,y,w=32,h=32){
	o.obj_ind=p;o.id=p.length;
	o.spr=s;o.x=x;o.y=y;o.w=w;o.h=h;
}
function GG(a,b){
	return Math.sqrt(a*a+b*b)
}
function centerSpr(o,a=false){
	if(a){
		o.sprX=o.spr[0].width>>1;
		o.sprY=o.spr[0].height>>1;
		o.w=Math.ceil(GG(o.spr[0].width,o.spr[0].height));
		o.h=o.w;
	}else{
		o.w=o.spr[0].width;o.h=o.spr[0].height;
	}
	o.Ox=o.w>>1;o.Oy=o.h>>1;
}
Trans=0;
const objWarp=class{
	constructor(x,y,e=false){
		this._x=x;this._y=y;
		this.cX=x+16;this.cY=y+16;
		this.warpX=this.cX;this.warpY=this.cY;
		this.w=32;this.h=32;this.savejn=1;
		this.spr=sprWarp;this.pxmask=true;
		this.img_ang=0;this.k=Trans;this.timer=0;
		if(this.spr[0].src.includes('/NANG/')){
			this.spd=1;
		}else{this.spd=0}
		if(!e){
			this.obj_ind=warp;this.id=warp.length;
		}
	}
	Warp(){
		this.savejn=player.jumpNum;
		switch(this.k){
			case 0:
				FlashCol='#000';toFlAl=100;FlashSpd=4;this.timer=30;player=null;
			break;
			case 1:
				this.px=player.x;this.py=player.y;this.timer=100;player=null;
			break;
			case 2:
				WarpR=this;this.sc=0;this.ang=0;this.timer=64;
				objEf.push(new objParticle(player.x-1*player.image_xscale,player.y-7,player.spr,0,0,0.05,player.image_xscale,1));
				player=null;
			break;
			case 3:
				player.x=this.warpX;player.y=this.warpY;
			break;
		}
	}
	move(){
		if(this.timer>0){
			this.timer-=1;
			if(this.k===1){
				if(Math.abs(this.px-this.warpX)<1 && Math.abs(this.py-this.warpY)<1){
					this.timer=0;
				}else{
					var hs=(this.warpX-this.px)/4;
					var vs=(this.warpY-this.py)/4;
					if(hs>40){
						vs=vs/hs*40;hs=40;
					}
					if(vs>40){
						hs=hs/vs*40;vs=40;
					}
					this.px+=hs;this.py+=vs;
					if(ViewMode===0){
					ViewX=Math.min(RoomW-800,Math.max(0,(this.px/800<<0)*800));
					ViewY=Math.min(RoomH-608,Math.max(0,(this.py/608<<0)*608));
					}else{
					toVX=Math.max(400,Math.min(this.px,RoomW-400))-400;
					toVY=Math.max(304,Math.min(this.py,RoomH-304))-304;
					}
					if(ViewX!==PViewX||ViewY!==PViewY)activate();
					if(this.timer)objEf.push(new objParticle(this.px-1,this.py-7,sprPlayerFall))
				}
			}else if(this.k===2){
				this.sc+=10;this.ang+=0.075;
			}
			if(this.timer===0){
				player=new objPlayer(this.warpX,this.warpY);
				player.jumpNum=this.savejn;
				switch(this.k){
					case 0:
						toFlAl=0;FlashSpd=4;
					break;
					case 2:
						FlashCol='#000';FlashSpd=5;
						FlashAl=100;toFlAl=0;WarpR=null;
					break;
				}
			}
		}
		if(this.spd!==0)this.img_ang+=this.spd;
	}
	draw_self(s){
		draw_sprite_angle(s,this.spr[0],this.cX,this.cY,this._ang,16,16);
		if(edit && this.id!==undefined){
			scr.globalAlpha=0.7;
			scr.beginPath();
			scr.moveTo(this.cX,this.cY);
			scr.lineTo(this.warpX,this.warpY);
			scr.arc(this.warpX,this.warpY,1,0,PI*2);
			scr.arc(this.warpX,this.warpY,2,0,PI*2);
			scr.stroke();
			scr.globalAlpha=1;
		}
	}
	get img_ang(){return this._ang}
	set img_ang(a){this._ang=a;this.setWH()}
	get x(){return this._x}
	set x(a){this._x=a;this.cX=a+16}
	get y(){return this._y}
	set y(a){this._y=a;this.cY=a+16}
	setWH(){
		var a=this._ang*PI/180;
		let xx=16*Math.sin(a);
		let yy=16*Math.cos(a);
		this.drawW=Number(Math.max(Math.abs(yy-xx),Math.abs(yy+xx)).toFixed(0));
		this.drawH=this.drawW;
	}
}
const objBlock=class{
	constructor(x,y,w,h){
		objInit(this,block,sprFloor,x,y,w,h);
		this.hspeed=0;this.vspeed=0;
	}
	setScale(w,h){
		this.w=Math.max(32,Math.round(w/32)*32);
		this.h=Math.max(32,Math.round(h/32)*32);
	}
	draw_self(){
		if(edit){var s=scr}else{var s=scrT};
		if(Theme===1){
			s.strokeStyle=SprCol;
			s.strokeRect(this.x+0.5,this.y+0.5,this.w-1,this.h-1);
		}else{
			for(var xx=this.x;xx<this.x+this.w;xx+=32){
				for(var yy=this.y;yy<this.y+this.h;yy+=32){
					if(yy===this.y){
						s.drawImage(sprFloor[0],xx,yy);
					}else{
						s.drawImage(sprBlock[0],xx,yy);
					}
				}
			}
		}
	}
}
const objBreakableBlock=class{
	constructor(x,y){
		objInit(this,block,sprBBlock,x,y,32,32);
	}
	draw_self(){
		scr.drawImage(sprBBlock[0],this.x,this.y);
	}
}
const objSpike=class{
	constructor(x,y,ang){
		objInit(this,spike,sprSpike,x,y);
		this.xp=x;this.yp=y;
		this.sprX=16;this.sprY=16;
		this.pxmask=true;
		this._ang=ang;
		this.img_ang=ang;
	}
	setScale(w,h){
		this.sprX=w/2;this.sprY=h/2;
		this.w=w;this.h=h;this.setWH();
	}
	draw_self(s){
		if(this.x!==this.xp || this.y!==this.yp)this.setWH();
		this.xp=this.x;this.yp=this.y;
		var a=this._ang*PI/180;
		s.save();
		s.rotate(-a);
		if(Math.abs(this.scX)<1 || Math.abs(this.scY)<1){
			s.imageSmoothingEnabled=true;
		}
		s.drawImage(sprSpike[0],this.dX,this.dY,this.w,this.h);
		s.restore();
		s.imageSmoothingEnabled=false;
	}
	get img_ang(){
		return this._ang;
	}
	set img_ang(a){
		this._ang=a;this.setWH();
	}
	setWH(){
		var {x,y,sprX,sprY}=this;
		var a=this._ang*PI/180;
		let _s=Math.sin(a);let _c=Math.cos(a);
		this.dX=x*_c-y*_s+sprX*(_c-_s-1);
		this.dY=y*_c+x*_s+sprY*(_c+_s-1);
		let xx=sprX*_c;let yy=sprY*_s;
		this.cX=this.dX*_c+this.dY*_s+xx+yy;
		this.drawW=Number(Math.max(Math.abs(yy-xx),Math.abs(yy+xx)).toFixed(0));
		xx=sprY*_c;yy=sprX*_s;
		this.cY=this.dY*_c-this.dX*_s+xx-yy;
		this.drawH=Number(Math.max(Math.abs(yy-xx),Math.abs(yy+xx)).toFixed(0));
	}
}
const objPlayer=class{
	constructor(x,y){
		this.x=x;this.y=y;
		this.w=32;this.h=32;
		this.Ox=17;this.Oy=23;
		this.cL=-5;this.cT=-12;
		this.cW=11;this.cH=21;
		this.pxmask=false;
		this.spr=sprPlayerIdle;this.sprName='i'
		this.image_number=4;
		this.ind=0;this.img_spd=0.2;
		this.image_xscale=1;
		this.maxSpeed=3;this.maxVspeed=9;
		this.jumpNum=JumpNum;this.djump=this.jumpNum;
		this.jump1=-8.5;this.jump2=-7;
		this.hspeed=0;this.vspeed=0;
		this.Ghspeed=0;this.friction=0.5;
		this.pfhspeed=0;this.pfvspeed=0;
		this.onPlatform=false;this.shoottime=0;
		this.hspdR=0;this.hspdL=0;this.gravity=0.4;
	}
	place_meeting(xx,yy,obj,a){
		var l=roundFix(xx+this.cL),t=roundFix(yy+this.cT)
		if(obj===warpEnd){
			if(colRect(l,t,this.cW,this.cH,obj.cX-obj.drawW,obj.cY-obj.drawH,obj.drawW<<1,obj.drawH<<1))return colRP(l,t,this.cW,this.cH,obj);
			return false;
		}else{
			return colRO(l,t,this.cW,this.cH,obj,a);
		}
	}
	place_free(xx,yy){
		var l=roundFix(xx+this.cL),t=roundFix(yy+this.cT)
		return !colRO(l,t,this.cW,this.cH,block);
	}
	evStep(){
		this.gravity=0.4;this.maxSpeed=3;
		var o=this.place_meeting(this.x,this.y,field);
		if(o){
			switch(o.f){
				case 0:this.gravity=0.2;break;
				case 1:this.gravity=0.7;break;
				case 2:this.maxSpeed=6;break;
				case 3:this.maxSpeed=1;break;
			}
		}
		//Move
		var L=keyboard_check(vk_left);
		var R=keyboard_check(vk_right);
		var h=R;if(!h){h=-L};
		if(h){
			var l=Math.round(this.x+this.cL-1);
			var t=Math.round(this.y+this.cT-1);
			var o=colRO(l,t,this.cW+2,this.cH+2,Walljump);
			var con=true;
			if(o)con=(h===o.side);
			if(con){
				this.image_xscale=h;
				this.hspeed=this.maxSpeed*h;
				this.spr=sprPlayerRun;this.sprName='r'
				this.img_spd=0.5;
			}
		}else{
			this.hspeed=0;
			this.spr=sprPlayerIdle;this.sprName='i'
			this.img_spd=0.2;
		}
		if(this.vspeed>this.maxVspeed)this.vspeed=this.maxVspeed;
		if(!this.onPlatform&&this.place_free(this.x,this.y+1)){
			if(this.vspeed<0){
				this.spr=sprPlayerJump;this.sprName='j'
			}else{
				this.spr=sprPlayerFall;this.sprName='f'
			}
			this.img_spd=0.2;
		}
		//Water
		this.inWater=false;
		if(this.place_meeting(this.x,this.y,water)){
			this.inWater=true;
			if(this.vspeed>2)this.vspeed=2;
		}
		var o=this.place_meeting(this.x,this.y+1,block);
		//IceBlock
		if(o){
			switch(o.__proto__.constructor.name){
				case 'objIceBlock':
					if(this.hspeed>0){
						this.hspdR=this.hspeed;
						this.hspeed-=Math.round(this.hspdL);
					}else if(this.hspeed<0){
						this.hspdL=-this.hspeed;
						this.hspeed+=Math.round(this.hspdR);
					}else{
						this.hspeed+=Math.round(this.hspdR-this.hspdL);
					}
					if(this.hspdR>0)this.hspdR-=0.1;
					if(this.hspdL>0)this.hspdL-=0.1;
				break;
				default:this.hspdR=0;this.hspdL=0;
			}
		}else{
			this.hspdR=0;this.hspdL=0;
		}
		//GMColObj
		var o=this.place_meeting(this.x,this.y,GMColObj);
		if(o)o.evCol();
		//Conveyor
		var o=this.place_meeting(this.x,this.y+1,conveyor,true);
		if(o){
			var [ch1,ch2,cv1,cv2]=[0,0,0,0];
			o.forEach(o=>{
				if(o.fx===0){
					if(o.spd>0){ch1=Math.max(ch1,o.spd)}else{ch2=Math.min(ch2,o.spd)}
				}else{
					if(o.spd>0){cv1=Math.max(cv1,o.spd)}else{cv2=Math.min(cv2,o.spd)}
				}
			})
			player.hspeed+=ch1+ch2;
			var cv3=cv1+cv2;
			if(cv3>0){
				if(player.vspeed<cv3)player.vspeed+=cv3;
			}else{
				if(player.vspeed>cv3)player.vspeed+=cv3;
			}
		}
		//Shoot&Jump
		if(this.shoottime===0 && keyboard_check_pressed(vk_Z)){
			bullet.push(new objBullet(this.x,this.y));
			sndPlay(sndShoot);
			this.shoottime=5;
		}
		if(this.shoottime)this.shoottime-=1;
		if(keyboard_check_pressed(vk_shift)){
			if(!this.place_free(this.x,this.y+1) || this.onPlatform || this.place_meeting(this.x,this.y+1,platform)){
				this.vspeed=this.jump1;
				sndPlay(sndJump);this.djump=this.jumpNum;
			}else if(this.djump>0 || this.inWater){
				this.vspeed=this.jump2;
				if(this.djump)this.djump-=1;
				sndPlay(sndJump,2);
				if(this.jumpNum>1 && this.djump<this.jumpNum-1){
					for(let i=0;i<3;i++){
						let spd=Math.random()/10+0.1;
						let dir=Math.random()*2*PI;
						let xx=Math.random()*10-5;
						let alspd=Math.random()/100+0.02;
						objEf.push(new objParticle(this.x+xx,this.y+4,sprPStar,spd,dir,alspd))
					}
				}
			}
		}
		if(keyboard_check_released(vk_shift)){
			if(this.vspeed<0)this.vspeed*=0.45;
		}
		//Walljump
		var c=this.place_free(this.x,this.y+1);
		var l=Math.round(this.x+this.cL-1);
		var t=Math.round(this.y+this.cT-1);	
		if(c){
			var o=colRO(l,t,this.cW+2,this.cH+2,Walljump);
			if(o){
				this.vspeed=2;
				this.spr=sprPlayerSlide;this.sprName='s'
				this.img_spd=0.5;
				this.image_xscale=o.side;
				var leave=(keyboard_check_pressed(vk_right) && o.side===1)||(keyboard_check_pressed(vk_left) && o.side==-1)
				if(leave){
					if(keyboard_check(vk_shift)){
						this.spr=sprPlayerJump;this.sprName='j'
						this.img_spd=0.2;this.vspeed=-9;
						this.hspeed=15*o.side;
						sndPlay(sndWallJump);
					}else{
						this.spr=sprPlayerFall;this.sprName='f'
						this.img_spd=0.2;this.hspeed=3*o.side;
					}
				}
			}
		}
	}
	move(){
		this.ind+=this.img_spd;
		this.ind=this.ind % this.spr.length;
		this.vspeed+=this.gravity;
		if(this.Ghspeed>0){
			this.Ghspeed-=this.friction;
			if(this.Ghspeed<0)this.Ghspeed=0;
		}else if(this.Ghspeed<0){
			this.Ghspeed+=this.friction;
			if(this.Ghspeed>0)this.Ghspeed=0;
		}
		this.hspeed+=this.Ghspeed;
		this.x+=this.hspeed;this.y+=this.vspeed;
		//Platform
		if(this.onPlatform){
			var o=this.place_meeting(this.x,this.y+4,platform,true)
			if(!o||!o.includes(this.onPlatform))this.onPlatform=false
		}
		var other=this.place_meeting(this.x,this.y+2,platform,true);
		if(other&&this.place_free(this.x,this.y)){
			for(var i=0;i<other.length;i++){
				this.pfhspeed+=other[i].hspeed;
				this.x+=other[i].hspeed;
			}
		}
		if(this.onPlatform&&this.place_meeting(this.x,this.y,platform)){
			var temprey=this.y;
			var temy=this.onPlatform.y-9;
			var temvspeed=this.vspeed;
			if(this.onPlatform.vspeed>0){
				this.vspeed=this.onPlatform.vspeed;
				this.pfvspeed=temy-this.y;
			}else{
				this.vspeed=0;this.pfvspeed=0;
			}
			this.y=temy
		}
		//Block
		var vmove=false;
		if(!this.place_free(this.x,this.y)){
			var hspd=this.hspeed+this.pfhspeed;
			var vspd=this.vspeed+this.pfvspeed;
			this.x-=hspd;this.y-=vspd;
			if(hspd!==0 && !this.place_free(this.x+hspd,this.y)){
				var mh=sign(hspd);
				while(this.place_free(this.x+mh,this.y)){
					this.x+=mh;
				}
				this.hspeed=0;hspd=0;this.Ghspeed=0;
			}
			if(vspd!==0 && !this.place_free(this.x,this.y+vspd)){
				var mv=sign(vspd),d=0;
				while(this.place_free(this.x,this.y+mv)&&d<Math.abs(vspd)){
					this.y+=mv;d++
				}
				if(vspd>0)this.djump=this.jumpNum;
				this.vspeed=0;vspd=0;vmove=true;
			}
			if((hspd!==0 || vspd!==0) && !this.place_free(this.x+hspd,this.y+vspd)){
				this.hspeed=0;hspd=0;
			}
			this.x+=hspd;this.y+=vspd;
		}
		this.pfhspeed=0;this.pfvspeed=0;
		if(this.onPlatform){
			var o=this.place_meeting(this.x,this.y+4,platform,true);
			if(o && o.includes(this.onPlatform)){
				this.djump=this.jumpNum;
			}else{
				this.onPlatform=false;
				if(!vmove&&temprey!==undefined){
					this.vspeed=temvspeed;this.y=temprey;
				}
			}
		}else{
			var other=this.place_meeting(this.x,this.y,platform,true);
			if(other){
				for(var i=0;i<other.length;i++){
					if(!this.onPlatform&&this.y-this.vspeed/2<=other[i].y&&this.place_free(this.x-this.hspeed,this.y-this.vspeed)){
						this.onPlatform=other[i];this.y=this.onPlatform.y-9;this.vspeed=this.onPlatform.vspeed;break;
					}
				}
			}
		}
		if(this.place_meeting(this.x,this.y,block))killPlayer()
		if(this.place_meeting(this.x,this.y,spike))killPlayer()
		if(this.place_meeting(this.x,this.y,cherry))killPlayer()
		if(this.x<0 || this.x>RoomW || this.y<0 || this.y>RoomH){
			if(OutMode===2){
				if(this.x>RoomW){this.x-=RoomW}
				else if(this.x<0){this.x+=RoomW}
				else if(this.y>RoomH){this.y-=RoomH}
				else if(this.y<0){this.y+=RoomH}
			}else if(OutMode===3){
				if(this.x>RoomW){this.x-=RoomW}
				else if(this.x<0){this.x+=RoomW}
				else{killPlayer()}
			}else if(OutMode===4){
				if(this.y>RoomH){this.y-=RoomH}
				else if(this.y<0){this.y+=RoomH}
				else{killPlayer()}
			}else{killPlayer()}
		}
		if(player){
			if(this.place_meeting(this.x,this.y,warpEnd)){
				GameEnd();
			}else{
				var o=this.place_meeting(this.x,this.y,warp);
				if(o){o.Warp()}
			}
		}
	}
	draw_self(){
		if(this.image_xscale<0){
			scr.save();scr.scale(-1,1);
			scr.drawImage(this.spr[this.ind<<0],-this.x-this.Ox,this.y-this.Oy);
			scr.restore();
		}else{
			scr.drawImage(this.spr[this.ind<<0],this.x-this.Ox,this.y-this.Oy);
		}
	}
}
const objSavePoint=class{
	constructor(x,y){
		objInit(this,GMStepObj,sprSave,x,y);
		this.img_spd=0;this.ind=0;
	}
	evStep(){
		if(player){
			if(keyboard_check_pressed(vk_S)||keyboard_check_pressed(vk_Z)){
				if(colRect(this.x,this.y,this.w,this.h,player.x+player.cL,player.y+player.cT,player.cW,player.cH)){
					saveX=player.x;saveY=player.y;
					JumpNum=player.jumpNum;SAVE=this.id;
					if(RECT!==undefined){
						SRECT=RECT;SRECO={...RECO};
					}
					keyPause();saveEf=[]
					save_ef();room_load(true,true)
				}
			}
		}
		this.ind+=this.img_spd;
		if(this.ind>2){
			this.img_spd=0;this.ind=0;
		}
	}
	draw_self(){
		scr.drawImage(this.spr[this.ind%2<<0],this.x,this.y);
	}
}
const objBullet=class{
	constructor(x,y){
		this.x=x;this.y=y;
		this.spr=sprBullet;centerSpr(this);
		this.hspeed=player.image_xscale*16;
		this.ind=0;this.id=bullet.length;
	}
	evStep(){
		if(this.ind>40)destroy(bullet,this.id);
		var o=colRO(this.x-2,this.y-2,4,4,block);
		if(o){
			if(o.__proto__.constructor.name==='objBreakableBlock'){
				breakEF(o);destroy(block,o.id);
			}
			destroy(bullet,this.id);
		}
		this.x+=this.hspeed;this.ind+=1;
	}
	draw_self(){
		scr.drawImage(this.spr[this.ind%2],this.x-this.Ox,this.y-this.Oy);
	}
}
const objBlood=class{
	constructor(x,y){
		this.x=x;this.y=y;this.al=1;
		this.ind=Math.random()*3<<0;
		this.tosc=Math.random()*25+25;
		this.gravity=Math.random()/2+0.5;
		this.toal=Math.random()/6+0.3;
		this.sc=0;this.spd=Math.random()*5+1.5;
		this.dir=Math.random()*2*PI;
		this.spr=sprBlood;
	}
	draw_self(){
		if(this.sc>20){
			if(this.x+this.sc<0 || this.x-this.sc>RoomW || this.y+this.sc<0 || this.y-this.sc>RoomH)return false;
		}
		if(this.sc<this.tosc)this.sc+=0.5;
		if(this.al>this.toal)this.al-=0.04;
		if(this.gravity>0.2)this.gravity-=0.02;
		if(this.spd>0){
			this.x+=this.spd*Math.cos(this.dir);
			this.y+=this.spd*Math.sin(this.dir);
			this.spd-=0.05;
		}
		this.y+=this.gravity;
		scr.globalAlpha=this.al;
		scr.drawImage(this.spr[this.ind],this.x-this.sc,this.y-this.sc,this.sc*2,this.sc*2);
		scr.globalAlpha=1;
	}
}
const objPlatform=class{
	constructor(x,y){
		objInit(this,platform,sprPlatform,x,y,32,16);
		this.pxmask=false;this.hspeed=0;this.vspeed=0;
	}
	evStep(){
		if(this.hspeed===0&&this.vspeed===0)return false;
		var h=false;var v=false;
		var o=colRO(this.x+this.hspeed,this.y,this.w,this.h,block)
		if(o && o.__proto__.constructor.name!=='objCrate'){
			this.hspeed=-this.hspeed;h=true;
		}
		var o=colRO(this.x,this.y+this.vspeed,this.w,this.h,block);
		if(o && o.__proto__.constructor.name!=='objCrate'){
			this.vspeed=-this.vspeed;v=true;
		}
		if(!h && !v){
			var o=colRO(this.x+this.hspeed,this.y+this.vspeed,this.w,this.h,block);
			if(o && o.__proto__.constructor.name!=='objCrate'){
				this.hspeed=-this.hspeed;
			}
		}
	}
	move(){
		this.x+=this.hspeed;this.y+=this.vspeed;
	}
	draw_self(){
		scr.drawImage(this.spr[0],this.x,this.y);
	}
}
const objWater=class{
	constructor(x,y,w,h){
		objInit(this,water,sprWater,x,y,w,h);
		this.pxmask=false;
	}
	setScale(w,h){
		this.w=Math.max(32,Math.round(w/32)*32);
		this.h=Math.max(32,Math.round(h/32)*32);
	}
	draw_self(){
		scr.drawImage(this.spr[0],this.x,this.y,this.w,this.h);
	}
}
const objCherry=class{
	constructor(x,y,rid,dir){
		objInit(this,cherry,sprCherry,x,y,21,24);
		this.Ox=10;this.Oy=12;
		this.pxmask=true;this.hspeed=0;this.vspeed=0;
		if(rid===undefined){
			this.move=this.move1;
		}else{
			this.rid=rid;this.dir=dir;
			this.move=this.move2;
		}
	}
	place_nfree(xx,yy){
		var l=xx-this.Ox;var t=yy-this.Oy;
		for(var i=0;i<block.length;i++){
			if(block[i].__proto__.constructor.name!=='objCrate'){
				if(colRect(l,t,this.w,this.h,block[i].x,block[i].y,block[i].w,block[i].h))return true;
			}
		}
		return false;
	}
	move1(){
		if(this.hspeed===0&&this.vspeed===0)return false;
		if(this.place_nfree(this.x+this.hspeed,this.y))this.hspeed=-this.hspeed;
		if(this.place_nfree(this.x,this.y+this.vspeed))this.vspeed=-this.vspeed;
		this.x+=this.hspeed;this.y+=this.vspeed;
		if(this.x<=-11||this.y<=-12||this.x>=RoomW+10||this.y>=RoomH+12)destroy(this.obj_ind,this.id)
	}
	move2(){
		this.x=Rcherry[this.rid].x+Rcherry[this.rid].r*Math.cos(DtoR(Rcherry[this.rid].ang)+this.dir);
		this.y=Rcherry[this.rid].y-Rcherry[this.rid].r*Math.sin(DtoR(Rcherry[this.rid].ang)+this.dir);
	}
	draw_self(s){
		this.ind=cherry.ind;
		s.drawImage(this.spr[this.ind<<0],this.x-this.Ox,this.y-this.Oy);
	}
}
const objWallJump=class{
	constructor(x,y,s){
		objInit(this,Walljump,sprWallJump,x,y,14,32);
		this.side=s;this.pxmask=false;
	}
	draw_self(){
		if(edit){
			scr.drawImage(this.spr[0],this.x,this.y);
		}else{
			scrT.drawImage(this.spr[0],this.x,this.y);
		}
	}
}
const objRoundingCherry=class{
	constructor(x,y){
		objInit(this,Rcherry,sprRCherry,x,y,21,24);
		this.Ox=10;this.Oy=12;this.ang=0;
		this.n=6;this.r=64;this.spd=1;
	}
	createR(){
		for(var i=0;i<this.n;i++){
			cherry.push(new objCherry(this.x,this.y,this.id,i/this.n*2*PI));
		}
	}
	evDes(){
		for(var i=0;i<cherry.length;i++){
			if(cherry[i].rid===this.id){
				destroy(cherry,i);i-=1;
			}
		}
	}
	move(){
		this.ang+=this.spd;
		this.ang=this.ang%360;
	}
	draw_self(){
		scr.beginPath();
		scr.strokeStyle='gray';
		scr.arc(this.x,this.y,this.r,0,PI*2);
		scr.stroke();
		scr.globalAlpha=0.5;
		var dir=DtoR(this.ang);var xx,yy;
		for(var i=0;i<this.n;i++){
			xx=this.x+this.r*Math.cos(dir);
			yy=this.y-this.r*Math.sin(dir);
			scr.drawImage(sprCherry[0],xx-this.Ox,yy-this.Oy);
			dir+=PI*2/this.n;
		}
		scr.globalAlpha=1;
		scr.drawImage(this.spr[0],this.x-this.Ox,this.y-this.Oy);
	}
}
const objCrate=class{
	constructor(x,y){
		objInit(this,block,sprCrate,x,y,32,32);
		this.onPlat=false;this.gravity=0.5;
		this.hspeed=0;this.vspeed=0;
	}
	move(){
		this.vspeed+=this.gravity;var on=false;
		var fall=this.vspeed===this.gravity;
		//Player
		if(player){
			if(this.hspeed!==0 || player.hspeed!==0){
				var l=Math.round(player.x+player.cL+player.hspeed);
				var t=Math.round(player.y+player.cT);
				if(colRect(l,t,player.cW,player.cH,this.x+this.hspeed,this.y,this.w,this.h)){
					this.hspeed-=sign(this.hspeed-player.hspeed);
				}
			}
		}
		//Block
		if(this.hspeed!==0){
			if(colRO(this.x+this.hspeed,this.y,this.w,this.h,block,false,this.id)){
				this.hspeed=0;
			}
		}
		var o=colRO(this.x,this.y+this.vspeed,this.w,this.h,block,false,this.id);
		if(o){
			if(this.vspeed>0){
				this.y=o.y-32;this.vspeed=0;on=true;
			}else if(this.vspeed<0){
				this.y=o.y+o.h;this.vspeed=0;
			}
		}
		if(fall && on)this.x+=this.hspeed;
		this.y+=this.vspeed;
		if(this.y>RoomH)destroy(block,this.id);
	}
	draw_self(){
		this.hspeed=0;
		scr.drawImage(this.spr[0],this.x,this.y);
	}
}
const objParticle=class{
	constructor(x,y,spr,spd=0,dir=0,alspd=0.1,xs=1,ys=1){
		this.x=x;this.y=y;this.spr=spr;
		this.spd=spd;this.dir=dir;
		this.xs=xs;this.ys=ys;this.al=1;
		this.alspd=alspd;
		this.O=this.spr[0].width/2;
		this.id=objEf.length;
	}
	move(){
		if(this.vspd!==undefined){
			this.vspd+=this.grav;
			this.x+=this.hspd;this.y+=this.vspd;
		}else{
			this.x+=this.spd*Math.cos(this.dir);
			this.y-=this.spd*Math.sin(this.dir);
		}
		this.al-=this.alspd;
		if(this.al<=0)destroy(objEf,this.id);
	}
	draw_self(){
		scr.save();scr.scale(this.xs,this.ys);
		scr.globalAlpha=this.al;
		scr.drawImage(this.spr[0],this.x/this.xs-this.O,this.y/this.ys-this.O);
		scr.globalAlpha=1;scr.restore();
	}
}
const objJumpRefresher=class{
	constructor(x,y){
		this.x=x;this.y=y;this.spr=sprJR;centerSpr(this);
		this.obj_ind=GMColObj;this.id=GMColObj.length;
	}
	evCol(){
		if(player.djump===0)player.djump=1;
		sndPlay(sndExtraJump);
		destroy(GMColObj,this.id);
	}
	draw_self(){
		if(edit)centerSpr(this);
		scr.drawImage(this.spr[0],this.x-this.Ox,this.y-this.Oy);
	}
}
const objJumpChanger=class{
	constructor(x,y,n){
		var s;
		switch(n){
			case Infinity:s=sprIJump;this.snd=sndGainJump;break;
			case 2:s=sprTJump;this.snd=sndGainJump;break;
			case 1:s=sprDJump;this.snd=sndLoseJump;break;
			case 0:s=sprSJump;this.snd=sndLoseJump;break;
		}
		objInit(this,GMColObj,s,x,y,32,32);
		this.num=n;this.drawEf=0;this.pxmask=true;
	}
	evCol(){
		if(player.jumpNum!==this.num){
			player.djump+=this.num-player.jumpNum;
			player.jumpNum=this.num;
			sndPlay(this.snd);this.drawEf=25;
		}
	}
	draw_self(s){
		s.drawImage(this.spr[0],this.x,this.y);
		if(s===scr && this.drawEf>0){
			var sc=(45-this.drawEf)*0.8;
			scr.globalAlpha=this.drawEf/25;
			scr.drawImage(this.spr[0],this.x+16-sc,this.y+16-sc,sc*2,sc*2);
			scr.globalAlpha=1;this.drawEf-=1;
		}
	}
}
const objIceBlock=class{
	constructor(x,y,w,h){
		objInit(this,block,sprFloor,x,y,w,h);
	}
	setScale(w,h){
		this.w=Math.max(32,Math.round(w/32)*32);
		this.h=Math.max(32,Math.round(h/32)*32);
	}
	draw_self(){
		if(edit){var s=scr}else{var s=scrT};
		if(Theme===1){
			s.strokeStyle=SprCol;
			s.strokeRect(this.x+0.5,this.y+0.5,this.w-1,this.h-1);
			for(var xx=this.x;xx<this.x+this.w;xx+=32){
				s.drawImage(sprIce[0],xx,this.y);
			}
		}else{
			for(var xx=this.x;xx<this.x+this.w;xx+=32){
				for(var yy=this.y;yy<this.y+this.h;yy+=32){
					if(yy===this.y){
						s.drawImage(sprFloor[0],xx,yy);
						s.drawImage(sprIce[0],xx,yy);
					}else{
						s.drawImage(sprBlock[0],xx,yy);
					}
				}
			}
		}
	}
}
const objSpikeGear=class{
	constructor(x,y){
		objInit(this,Rcherry,sprSG,x,y,24,24);
		this.Ox=12;this.Oy=12;
		this.ang=0;this.n=4;
		this.r=32;this.l=32;this.spd=1;
	}
	createR(){
		if(this.n<3){
			this.l=this.r/2;
		}else{
			this.l=16/Math.tan(PI/this.n)+this.r/2;
		}
		for(var i=0;i<this.n;i++){
			let l=spike.push(new objMovingSpike(this.x,this.y,i/this.n*360-90+this.ang));
			spike[l-1].rid=this.id;
			spike[l-1].dir=i/this.n*2*PI+DtoR(this.ang);
			spike[l-1].setScale(32,this.r);
		}
	}
	evDes(){
		for(var i=0;i<spike.length;i++){
			if(spike[i].rid===this.id){
				destroy(spike,i);i-=1;
			}
		}
	}
	move(){
		this.ang+=this.spd;this.ang=this.ang%360;
	}
	draw_self(){
		scr.globalAlpha=0.6;
		var dir=DtoR(this.ang);var xx,yy;
		this.l=16/Math.tan(PI/this.n)+this.r/2;
		for(var i=0;i<this.n;i++){
			xx=this.x+this.l*Math.cos(dir);
			yy=this.y-this.l*Math.sin(dir);
			draw_sprite_angle(scr,sprSpike[0],xx,yy,dir/PI*180-90,16,this.r/2,1,this.r/32)
			dir+=PI*2/this.n;
		}
		scr.globalAlpha=1;
		scr.drawImage(this.spr[0],this.x-this.Ox,this.y-this.Oy);
	}
}
const objMovingSpike=class{
	constructor(x,y,ang){
		objInit(this,spike,sprSpike,x,y,32,32);
		this.sprX=16;this.sprY=16;
		this.pxmask=true;this._ang=ang;this.img_ang=ang;
	}
	setScale(w,h){
		this.sprX=w/2;this.sprY=h/2;
		this.w=w;this.h=h;this.setWH();
	}
	move(){
		if(this.rid!==undefined){
			let a=DtoR(Rcherry[this.rid].ang)+this.dir;
			this.x=Rcherry[this.rid].x+Rcherry[this.rid].l*Math.cos(a);
			this.y=Rcherry[this.rid].y-Rcherry[this.rid].l*Math.sin(a);
			this.img_ang=a/PI*180-90;
		}
	}
	draw_self(s){
		draw_sprite_angle(s,this.spr[0],this.x,this.y,this._ang,this.sprX,this.sprY,this.w/32,this.h/32);
	}
	get img_ang(){return this._ang}
	set img_ang(a){this._ang=a;this.setWH()}
	setWH(){
		var {x,y,sprX,sprY}=this;
		var a=this._ang*PI/180;
		let _s=Math.sin(a);
		let _c=Math.cos(a);
		this.cX=x;this.cY=y;
		let xx=sprX*_c;let yy=sprY*_s;
		this.drawW=Number(Math.max(Math.abs(yy-xx),Math.abs(yy+xx)).toFixed(0));
		xx=sprY*_c;yy=sprX*_s;
		this.drawH=Number(Math.max(Math.abs(yy-xx),Math.abs(yy+xx)).toFixed(0));
	}
}
const objBoard=class{
	constructor(x,y,txt=''){
		objInit(this,board,sprBoard,x,y);
		this.txt=txt;this.pxmask=false;this.op=false;
	}
	on(){
		Bd.innerHTML=`<pre><xmp>${this.txt}</xmp></pre>`;
		Bd.style.visibility='visible';
		GC.style.visibility='hidden';
		GO.style.visibility='hidden';
		GUI.style.transition="opacity 0.5s";
		GUI.style.visibility='visible';
		GUI.style.opacity='1';this.op=true;
	}
	off(){
		Bd.style.visibility='hidden';
		GUI.style.visibility='hidden';
		GUI.style.opacity='0';this.op=false;
		GUI.style.transition="none";
	}
	move(){
		if(player){
			if(player.x>this.x&&player.x<this.x+32&&player.y>this.y&&player.y<this.y+32){
				if(keyboard_check_pressed(vk_up))this.on();
				if(keyboard_check_pressed(vk_down))this.off();
			}else{
				if(this.op)this.off();
			}
		}
	}
	draw_self(){
		if(edit){
			scr.drawImage(this.spr[0],this.x,this.y);
		}else{
			scrT.drawImage(this.spr[0],this.x,this.y);
		}
	}
}
Conveyor=1;
const objConveyor=class{
	constructor(x,y){
		objInit(this,conveyor,sprConveyor,x,y);
		this.spd=1;this.pxmask=false;
		this.fx=0;this.sx=-32;this.CW=32;
	}
	get CW(){return this._w}
	set CW(a){this._w=a;if(this.fx===0){this.w=a}else{this.h=a}}
	setScale(w,h){
		if(this.fx===0){this.CW=Math.max(32,w)}else{this.CW=Math.max(32,h)}
	}
	move(){
		this.sx+=Math.abs(this.spd);if(this.sx>0){this.sx-=32}
	}
	draw_self(){
		scr.fillStyle=fillCoveyor;
		scr.save();
		if(this.fx===1){scr.rotate(PI/2);let xx=this.x+32;this.x=this.y;this.y=-xx}
		var tx=0;
		if(this.spd<0){scr.scale(-1,1);tx=this.x*2+this._w}
		scr.translate(this.x+this.sx-tx,this.y);
		scr.fillRect(-this.sx,0,this._w,32);
		if(this.fx===1){let yy=this.y+32;this.y=this.x;this.x=-yy}
		scr.restore();
	}
}
Spring=10;
const objSpring=class{
	constructor(x,y){
		objInit(this,GMColObj,sprSpring,x,y);
		centerSpr(this,true);this.ang=0;this.ind=0;
		this.spd=Spring;this.pxmask=true;
	}
	evCol(){
		if(this.ind===0){
			this.ind=1;sndPlay(sndSpring);
			player.vspeed=-this.spd*Math.cos(DtoR(this.ang));
			player.Ghspeed=-this.spd*Math.sin(DtoR(this.ang));
		}
	}
	draw_self(s){
		if(edit)centerSpr(this,true);
		draw_sprite_angle(s,this.spr[this.ind],this.x,this.y,this.ang,this.sprX,this.sprY,1,1);
	}
}
Trampoline=8;
const objTrampoline=class{
	constructor(x,y){
		objInit(this,GMColObj,sprTp,x,y);
		centerSpr(this,true);this.ang=0;this.ind=0;
		this.spd=Trampoline;this.pxmask=true;
	}
	evCol(){
		player.vspeed=-this.spd*Math.cos(DtoR(this.ang));
		player.Ghspeed=-this.spd*Math.sin(DtoR(this.ang));
	}
	draw_self(s){
		if(edit)centerSpr(this,true);
		draw_sprite_angle(s,this.spr[this.ind],this.x,this.y,this.ang,this.sprX,this.sprY,1,1);
	}
}
const objField=class {
	constructor(x,y,w,h,f){
		var s;this.f=f;
		switch(f){
			case 0:s=sprLGrav;this.fs=fillLGrav;break;
			case 1:s=sprHGrav;this.fs=fillHGrav;break;
			case 2:s=sprFSpd;this.fs=fillFSpd;break;
			case 3:s=sprSSpd;this.fs=fillSSpd;break;
		}
		objInit(this,field,s,x,y,w,h);
		this.pxmask=false;this.sx=0;
	}
	setScale(w,h){
		this.w=Math.max(32,Math.round(w/32)*32);
		this.h=Math.max(32,Math.round(h/32)*32);
	}
	move(){
		switch(this.f){
			case 0:this.sx-=1.2;if(this.sx<-32){this.sx+=32};break;
			case 1:case 2:this.sx+=1.2;if(this.sx>0){this.sx-=32};break;
			case 3:this.sx-=0.8;if(this.sx<-32){this.sx+=32};break;
		}
	}
	draw_self(){
		if(this.f>1){
			scr.save();
			scr.translate(this.x+this.sx,this.y);
			scr.fillStyle=this.fs;
			scr.fillRect(-this.sx,0,this.w,this.h);
			scr.restore();
		}else{
			scr.save();
			scr.translate(this.x,this.y+this.sx);
			scr.fillStyle=this.fs;
			scr.fillRect(0,-this.sx,this.w,this.h);
			scr.restore();
		}
	}
}
const objSaveEf=class{
	constructor(x,y){
		this.x=x;this.y=y;
		this.l=x;this.t=y;
		var spd=(1+Math.random())*2;
		var dir=Math.random()*PI*2;
		this.hspeed=spd*Math.cos(dir);
		this.vspeed=spd*Math.sin(dir);
		this.grav=(Math.random()+2)/10;
		this.al=1;
	}
	draw_self(){
		this.al-=0.05;if(this.al<=0)return false;
		this.vspeed+=this.grav;
		this.x+=this.hspeed;
		this.y+=this.vspeed;
		scr.globalAlpha=this.al
		scr.drawImage(scref,this.l,this.t,200,152,ViewX+this.x,ViewY+this.y,200,152);
		scr.globalAlpha=1
	}
}
const objBKEF=class{
	constructor(x,y,k,s){
		this.id=BKEF.length;this.k=k;this.s=s;
		this.x=x;this.y=y;this.h=0;this.v=0;
		this.a=Math.random()*Math.PI;this.aspd=0;
	}
	move(){
		this.y+=this.v;this.x+=this.h;
		if(this.k===0||this.k===2||this.k===6)this.a+=this.aspd;
		if(this.x+this.s<0 || this.x-this.s>800 || this.y+this.s<0 || this.y-this.s>608){
			destroy(BKEF,this.id);
		}
	}
	draw0(){
		let x=scr
		x.save();x.translate(this.x+ViewX,this.y+ViewY);
		if(this.a!==0)x.rotate(this.a);
		let s=-this.s>>1;
		x.rect(s,s,this.s,this.s);
		x.restore();
	}
	draw1(){
		let x=scr
		let dx=this.x+ViewX;var dy=this.y+ViewX;
		let r=this.s>>1;
		x.moveTo(dx+r,dy);
		x.arc(dx,dy,r,0,2*Math.PI);
	}
	draw2(){
		let x=scr
		x.save();x.translate(this.x+ViewX,this.y+ViewY);
		if(this.a!==0)x.rotate(this.a);
		let s=-this.s>>1;
		x.fillRect(s,s,this.s,this.s);
		x.restore();
	}
	draw3(){
		let x=scr
		x.save();x.translate(this.x+ViewX,this.y+ViewY);
		if(this.a!==0)x.rotate(this.a);let s=-this.s>>1;
		x.drawImage(sprPt[this.k-4],s,s,this.s,this.s);
		x.restore();
	}
}
const objFake=class{
	constructor(x,y,ind){
		switch(ind){
			case 0:
				objInit(this,GMColObj,sprFloor,x,y,32,32);
				this.img_ind=0;
			break;
			case 1:
				objInit(this,GMColObj,sprBlock,x,y,32,32);
				this.img_ind=0;
			break;
			case 2:
				objInit(this,GMColObj,sprPlatform,x,y,32,16);
				this.img_ind=1;
			break;
		}
		this.pxmask=false;this.ind=ind;
	}
	evCol(){
		breakEF(this);
		destroy(this.obj_ind,this.id);
	}
	draw_self(){
		scr.drawImage(this.spr[0],this.x,this.y)
		if(edit)scr.drawImage(sprFake[this.img_ind],this.x,this.y)
	}
}
const objShooter=class{
	constructor(x,y,m=0){
		objInit(this,shooter,sprShooter,x,y,24,24);
		this.Ox=12;this.Oy=12;
		this.ang=0;this.n=m===3?15:12;
		this.t=0;this.spd=4;this.mode=m
	}
	move(){
		if(this.t--===0){
			let dir=this.ang*Math.PI/180
			switch(this.mode){
				case 0:for(let i=this.n;i--;){
					let o=new objCherry(this.x,this.y)
					o.hspeed=this.spd*Math.cos(dir)
					o.vspeed=-this.spd*Math.sin(dir)
					o.inView=true
					cherry.push(o);dir+=Math.PI*2/this.n
				};break
				default:
				let e=this.mode+2,ag=2*Math.PI/e,a=dir+Math.PI-ag/4*this.mode,
				d=2*this.spd*Math.sin(Math.PI/e)*e/this.n,h,v,dh,dv,n=0
				for(let i=0;i<this.n;i++){
					if(i>=n){
						h=this.spd*Math.cos(dir);v=-this.spd*Math.sin(dir)
						dh=d*Math.cos(a);dv=-d*Math.sin(a)
						dir+=ag;a+=ag;n+=this.n/e
					}
					let o=new objCherry(this.x,this.y)
					o.hspeed=h;o.vspeed=v
					o.inView=true;cherry.push(o)
					h+=dh;v+=dv
				}
			}
			destroy(this.obj_ind,this.id)
		}
	}
	draw_self(){
		scr.drawImage(this.spr[this.mode],this.x-this.Ox,this.y-this.Oy);
	}
}
