const socket=io.connect('https://server.vicklleall.com/race')
socket.on('open',()=>{socket.emit('init',{u:nowUser,room:GAME_ID})})
socket.on('users',e=>{for(let k in e)USERS[k]=['i',0,-32,-32,1]})
socket.on('leave',e=>{delete USERS[e]})
socket.on('kid',e=>{USERS[e[0]]=e[1].split(',')})
socket.on('msg',addMsg)
function pushData(){
	if(player){
		socket.emit('kid',[nowUser,`${player.sprName},${player.ind<<0},${player.x<<0},${player.y<<0},${player.image_xscale}`])
	}
}
function addMsg(m){
	let p=document.createElement('p');p.innerText=m
	$`msg`.appendChild(p)
	setTimeout(e=>$`msg`.removeChild(e),20000,p)
}
document.addEventListener('keydown',e=>{
	if(document.activeElement.type==='textarea' || document.activeElement.type==='input')return true;
	if(e.keyCode===32){
		let m=(prompt('请输入要发送的消息：')||'').slice(0,30)
		if(m.trim()){
			m=nowUser+': '+m;
			if(parent){
				parent.addMsg(m)
				parent.ws.emit('msg',m)
			}else{
				addMsg(m)
				socket.emit('msg',m)
			}
		}
	}
})
console.log('%c温馨提示：不要作弊哦:)','color:#39F')