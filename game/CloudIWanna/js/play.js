if(currentUser){
	$('Player').innerText+=currentUser.getUsername();
}
const GAME_ID=window.location.search.slice(1);
ROOM={};
let USERS={},nowUser=currentUser.get('username')
if(GAME_ID){
var query=new AV.Query('Game');
query.include(['A','Room']);
query.select(['Name','C','RP','R','DP','D','P','I','A.username','Room']);
query.get(GAME_ID).then(function(o){
		GAME=o._serverData;
		if(GAME.RP===0){
			GAME.R=0;GAME.Ra='---';
		}else{
			GAME.R=GAME.R/GAME.RP;
			GAME.Ra=GAME.R.toFixed(1);
		}
		if(GAME.DP===0){
			GAME.D='??';
		}else{
			GAME.D=(GAME.D/GAME.DP).toFixed(1);
		}
		GAME.A=GAME.A.username;
		GAME.T=o.createdAt;
		GAME.Room=undefined;
		ROOM=o.get('Room')._serverData;
		ROOM.CT=ROOM.CT||Infinity;
		if(ROOM.CT>7200)ROOM.CT=Infinity;
		ROOM.CD=ROOM.CD||Infinity;
		if(ROOM.CD>1000)ROOM.CD=Infinity;
		Object.freeze(GAME);
		Object.freeze(ROOM);
		GameInit();
	},function(e){console.log(e)}
)
}
function gameC(c){
	switch(c){
		case 0:return 'Needle';break;
		case 1:return 'Trap';break;
		case 2:return 'Gimmick';break;
	}
}
function gameD(d){
	return `${d.getFullYear()}-${d.getMonth()+1}-${d.getDate()} ${cov(d.getHours())}:${cov(d.getMinutes())}:${cov(d.getSeconds())}`
}
const GameInit=function(){
	document.title+=` -- ${GAME.Name}`;
	$('GameName').innerText=GAME.Name;
	$('Rt').innerText=GAME.Ra;
	$('RC').style.width=`${(GAME.R<<0)*24+GAME.R%1*19+(GAME.R%1*20>1)*3}px`
	$('Author').innerText=GAME.A;
	$('In').innerText=GAME.I;
	$('Dif').innerText=GAME.D;
	$('Hot').innerText=GAME.P;
	$('Class').innerText=gameC(GAME.C);
	$('Date').innerText=gameD(GAME.T);
	if(ROOM.CT!==Infinity || ROOM.CD!==Infinity){
		var add=`<span>🏅挑战:</span>`;
		if(ROOM.CT!==Infinity){
			var s=ROOM.CT%60;
			var t=cov((ROOM.CT-s)/60)+':'+cov((s).toFixed(0));
			add+=`<span>⏱️Time:</span><span>${t}</span>`
		}
		if(ROOM.CD!==Infinity){
			add+=`<span>💀Deaths:</span><span>${ROOM.CD}</span>`
		}
		var o=$('challenge');
		o.innerHTML=add;
		o.style.display='inline-block';
	}
	$('warp').style.opacity=1;loadGame()
}
const NtS=function(n){
	return String.fromCharCode(n+100);
}
const StN=function(s){
	return s.charCodeAt()-100;
}
const cNum=function(s,n){
	if(n)return (s.charCodeAt()-200)/Math.pow(10,n);
	return s.charCodeAt()-200;
}
const loadGame=function(){
	LOAD();
	var str=ROOM['R'];
	RoomW=cNum(str.charAt(0));RoomH=cNum(str.charAt(1));
	tiles.width=RoomW;tiles.height=RoomH;
	ViewX=0;ViewY=0;toVX=0;toVY=0;PViewX=ViewX;PViewY=ViewY;RViewX=0;RViewY=0;
	BGhspd=0;BGvspd=0;OutMode=0;ViewMode=0;
	var th=ROOM['T'];
	if(th){
		Theme=StN(th.charAt(0));
		if(Theme===0){
			SprCol=th.slice(1,8);SprCol2=th.slice(8,15);
			setSprCol(SprCol,SprCol2,0);
		}else if(Theme===1){
			SprCol=th.slice(1,8);SprCol2=SprCol;
			setSprCol(SprCol,SprCol2,1);
		}else{
			loadSkin(AllTheme[Theme]);
		}
	}
	var col=ROOM['BG'];
	if(col){
		BackGround=$('back');sfbg=BackGround.getContext('2d')
		BGMode=StN(col.charAt(0));
		col=col.slice(1);
		if(BGMode===3){
			let m=col.match(/h-*\d+\.*\d*v-*\d+\.*\d*/);
			var h=0;var v=0;
			if(m){
				h=m[0].match(/h-*\d+\.*\d*/)[0].slice(1);
				v=m[0].match(/v-*\d+\.*\d*/)[0].slice(1);
			}
			var s=col.match(/url.*\)/);
			if(s){
				BackGround.style.background=s[0];
			}else{
				BackGround.style.background=`url('img/spr/${col}/BG.png')`;
			}
			var ls=col.match(/\/ \d+px/);
			if(ls){
				$('C21').click();
				let s1=BackGround.style.background.match(/url.*\)/);
				if(s1){
					BackGround.style.background=`${s1[0]} 0 0 /${RoomW}px ${RoomH}px`;
				}
				BGhspd=0;BGvspd=0;
			}else{
				var jz=col.match(/center/);
				if(jz){
					let s2=BackGround.style.background.match(/url.*\)/);
					if(s2){
						BackGround.style.background=s2[0]+'center no-repeat';
					}
					BGhspd=0;BGvspd=0;
				}else{
					let s0=BackGround.style.background.match(/url.*\)/);
					if(s0){
						BackGround.style.background=s0[0];
					}
					BGhspd=Number(h);
					BGvspd=Number(v);
				}
			}
		}else{
			BackGround.style.background=col;
		}
	};
	try{room_load(false,false,true)}catch(e){console.log(e)}
	BGMid={value:ROOM['BGM']}
	if(ROOM['E'] && ROOM['E']!==''){
		EF[0]=Number(ROOM['E'].charAt(0));EF[7]=Number(ROOM['E'].charAt(7));
		for(let i=1;i<7;i++){
			var step;
			switch(i){
				case 1:case 2:case 6:step=1;break;
				default:step=0.1;
			}
			EF[i]=StN(ROOM['E'].charAt(i))*step;
		}
		EF[8]=ROOM['E'].slice(8);
	}else{
		EF=[];
	}
}
const loadRoom=function(){
	var str=ROOM['R'];
	RoomW=cNum(str.charAt(0));RoomH=cNum(str.charAt(1));
	startX=cNum(str.charAt(2));
	startY=cNum(str.charAt(3));
	warpEnd=new objWarp(cNum(str.charAt(4)),cNum(str.charAt(5)),true);
	OutMode=cNum(str.charAt(6));
	ViewMode=cNum(str.charAt(7));
	ViewSX=cNum(str.charAt(8));ViewSY=cNum(str.charAt(9));
	readRoom(ROOM);
}
document.addEventListener('keydown',e=>{if(e.keyCode===116)e.preventDefault()})
Loading.ontouchstart=()=>Loading.touch=true
Loading.ontouchend=()=>Loading.touch=false
function loadCheck(){
	if(Time.start){
		if(keyboard_check_pressed(vk_shift)||Loading.touch){
			var Views=AV.Object.createWithoutData('Game',GAME_ID);
			Views.increment('P',1);Views.save();
			if(oBGM.paused){
				oBGM.src=`https://music.163.com/song/media/outer/url?id=${ROOM['BGM']}.mp3`;BGMplay.click()
			}else{oBGM.currentTime=0}
			key_clear(vk_right);key_clear(vk_left);
			key_clear(vk_shift);
			key_clear(vk_Z);key_clear(vk_S);
			clearTimeout(ICheck);
			Loading.style.display='none';
			Loading.firstChild.innerText='Game Paused';
			Loading.firstChild.style.fontSize='48px';
			oTime=new objTime();REC=true;
			setInterval(GameRun,20);
		}
	}else if(loadCount===sprNumber){
		Loading.style.backgroundColor='rgba(0,0,0,0.5)';
		Loading.innerHTML=`<pre style='color:#FFF;text-shadow:0 0 5px #000,0 0 10px #000'>Press Shift Key To Start<pre>`;
		Time.time=0;pause=false;room_load();
		key_clear(vk_right);key_clear(vk_left);
		key_clear(vk_shift);key_clear(vk_Z);
		key_clear(vk_S);GameRun();room_load(true,true);
		Time.innerText='00:00.00';
		Death.innerText='0';
		Time.start=true;if(EF[0]>3)EFSprinit();
	}
}
ICheck=setInterval(loadCheck,50);
UPRK=false;OnUp=false;
const GameEnd=function(){
	END=true;
	GO.style.visibility='hidden';
	GC.style.visibility='visible';
	GUI.style.transition="opacity 1s 0.4s";
	GUI.style.visibility='visible';
	GUI.style.opacity='1';
	GCobj[0].style.right='420px';
	GCobj[1].style.left='420px';
	if(!REP){
		if(currentUser.get("emailVerified")){
			OnUp=true;UPR();UPRK=Date.now();
		}
		if(ROOM.CT!==Infinity || ROOM.CD!==Infinity){
			if(Time.time<=ROOM.CT && oTime.getDeaths()<=ROOM.CD){
				setTimeout("$('ach').style.top='277px'",2000);
			}
		}
		if(parent&&parent.GameEnd)parent.GameEnd(Time.time)
	}
}
GCB=$('gcb');
$('next').onclick=function(){
	GCB.style.transform='translateX(-50%) translateY(-50%) scale(1) rotate(360deg)';
	GCB.children[0].innerText="⏱️ Time: "+Time.innerText;
	GCB.children[1].innerText="💀 Deaths: "+Death.innerText;
}
pRB=$('pRB');
pRC=$('pRC');
pRT=$('pRT');
pRB.onmousemove=function(e){
	var r=Math.ceil(((e.offsetX-16)/48));
	pRC.style.width=`${r*48}px`;
	pRT.innerText=r.toFixed(1);
	if(!uRD.d && DF.innerText!=='??'){
		uRD.on=true;
		uRD.style.color='';
		uRD.style.backgroundColor='';
	}
}
DF=$('DF');
DFd=$('DFd');
DFd.on=false;
DFd.style.transition='none';
DFd.onmousedown=function(e){
	this.on=true;
	this.dX=e.clientX-this.offsetLeft;
	this.dY=e.clientY-this.offsetTop;
}
window.onmousemove=function(e){
	if(DFd.on){
		var a=Math.min(Math.max(46,e.clientX-DFd.dX),415)
		DFd.style.left=a+'px';
		DF.d=(a-46)/3.69;
		if(DF.d<20){
			s=' Very Easy'
		}else if(DF.d<40){
			s=' Easy'
		}else if(DF.d<60){
			s=' Medium'
		}else if(DF.d<80){
			s=' Hard'
		}else{
			s=' Very Hard'
		}
		DF.innerHTML=`${DF.d.toFixed(0)}<span>${s}</span>`;
		if(!uRD.d && pRT.innerText!=='---'){
			uRD.on=true;
			uRD.style.color='';
			uRD.style.backgroundColor='';
		}
	}
}
window.onmouseup=function(){
	if(DFd.on)DFd.on=false;
}
uRD=$('uRD');uRD.on=false;uRD.d=false;
uRD.style.color='#BBB';
uRD.style.backgroundColor='#EEE';
uRD.onclick=function(){
	if(!currentUser.get('emailVerified')){
		alert('您的账号尚未验证，无法给游戏评分');return false;
	}
	if(currentUser.getUsername()===GAME.A){
		alert('不能给自己的作品评分！');return false;
	}
	if(upRank&&upRank.get('C')!==undefined){
		alert('您已经给此游戏评过分了！');return false;
	}
	if(this.on && !this.d){
		var o=AV.Object.createWithoutData('Game',GAME_ID);
		o.increment('R',Number(pRT.innerText));
		o.increment('D',Math.round(DF.d));
		o.increment('RP',1);
		o.increment('DP',1);
		if(upRank){upRank.set('C',Number(pRT.innerText));upRank.save()}
		o.set('Index',GAME.T.getTime()+(GAME.R+Number(pRT.innerText))/(GAME.RP+1)*1e8)
		o.save().then(function(){
			alert('提交成功！感谢评价。')
		},function(e){
			uRD.d=false;
			uRD.style.color='';
			uRD.style.backgroundColor='';
			alert('提交失败！')
		});
		this.d=true;
		this.style.color='#BBB';
		this.style.backgroundColor='#EEE';
	}
}
function objRank(s){
	var o={};
	o.T=s.T;o.D=s.D;
	o.P=s.P.get('username');
	o.R=s.R;
	return o;
}
var RANK,Rind,upRank;
function UPR(){
	var query=new AV.Query('GRank');
	var g=AV.Object.createWithoutData('Game',GAME_ID);
	query.equalTo('G',g);
	query.include(['P.username','R']);
	query.find().then(function(r){
		var done=false;RANK=[];var nr=false;
		r.forEach((o,ind)=>{
			RANK.push(objRank(o._serverData));
			if(o.get('P').id===currentUser.id){
				Rind=ind;upRank=o
				if(o.get('T')>Time.time){
					var a=upRank;a.set('T',Time.time);
					a.set('D',oTime.getDeaths());
					a.set('R',RECO);
					if(ROOM.CT!==Infinity || ROOM.CD!==Infinity){
					if(o.get('T')>ROOM.CT || o.get('D')>ROOM.CD){
						if(Time.time<=ROOM.CT && oTime.getDeaths()<=ROOM.CD){
							currentUser.increment('Exp',1);
							currentUser.save();
						}
					}
					}
					a.save().then(function(a){
						RANK[Rind].T=a._serverData.T;
						RANK[Rind].D=a._serverData.D;
						loadLB();OnUp=false;
						setTimeout("GCobj[0].style.top='-50px';GCobj[1].style.top='-50px';GCobj[2].style.top='60px'",Math.max(1200-Date.now()+UPRK,0));
					},function(e){console.log(e)});
				}else{
					nr=true;OnUp=false;
					setTimeout("GCobj[0].style.top='-50px';GCobj[1].style.top='-50px';GCobj[2].style.top='60px'",Math.max(1200-Date.now()+UPRK,0));
				}
				done=true;
			}
		})
		if(done){
			if(nr)loadLB();
		}else{
			upRank=new AV.Object('GRank');
			currentUser.increment('Exp',1);
			if(ROOM.CT!==Infinity || ROOM.CD!==Infinity){
			if(Time.time<=ROOM.CT && oTime.getDeaths()<=ROOM.CD){
				currentUser.increment('Exp',1);
			}
			}
			var a=upRank;a.set('G',g);
			a.set('P',currentUser);
			a.set('T',Time.time);
			a.set('D',oTime.getDeaths());
			a.set('R',RECO);
			a.save().then(function(a){
				RANK.push(objRank(a._serverData));
				loadLB();OnUp=false;
				setTimeout("GCobj[0].style.top='-50px';GCobj[1].style.top='-50px';GCobj[2].style.top='60px'",Math.max(1200-Date.now()+UPRK,0));
			},function(e){console.log(e)});
		}
	},function(e){console.log(e)})
}
function stLB(){
	RANK.sort((a,b)=>a.T-b.T);
	RANK.forEach((o,ind)=>{
		if(o.P===currentUser.get('username'))Rind=ind;
		return true;
	});
}
lB=$('lB');
lB.onclick=loadLB;
LB=$('LB');
function loadLB(){
	lB.style.display='none';
	if(typeof RANK==='object'){
		drawLB();
	}else{
		var query=new AV.Query('GRank');
		var g=AV.Object.createWithoutData('Game',GAME_ID);
		query.equalTo('G',g);
		query.include(['P.username','R']);
		query.addAscending('Time');
		query.addAscending('Death');
		query.find().then(function(r){
			RANK=[];
			r.forEach((o,ind)=>{
				RANK.push(objRank(o._serverData));
				if(o.get('P').id===currentUser.id)Rind=ind;
			})
			drawLB();
		},function(e){console.log(e)});
	}
}
function drawLB(){
	stLB();
	$('RK').innerText=Rind+1;
	var HTML=`<table><tr><th style='width:120px'>Player</th><th>Time</th><th>Deaths</th><th>Replay</th></tr>`;
	RANK.forEach((o,ind)=>{
		var b='';
		if(ind===Rind){
			b=` style='background:hsl(205,100%,95%)'`;
		}else if(!(ind & 1)){
			b=` style='background:#F0F2F5'`;
		}
		var r='';
		if(ind<3)r=` style='background:hsl(205,100%,60%)'`;
		HTML+=`<tr${b}><td><div class='R' ${r}>${ind+1}</div>${o.P}</td><td>${covT(o.T)}</td><td>${o.D}</td><td><span class='rep'>▶️</span></td></tr>`;
	})
	LB.innerHTML=HTML+'</table>';
	LB.style.display='block';
	rep=LB.getElementsByClassName('rep');
	for(var i=rep.length;i--;){
		rep[i].ind=i;
		rep[i].onclick=function(){
			if(Loading.style.display==='none'){
				if(this.innerText==='🔲'){
					this.innerText='▶️';END=false;
					REP=false;REC=true;keyPause();
					if(UPRK){
						room_load();
						oTime=new objTime();
					}else{
						oTime.resume();
					}
					if(player){
						toVX=Math.max(400,Math.min(player.x,RoomW-400))-400;
						toVY=Math.max(304,Math.min(player.y,RoomH-304))-304;
						RViewX=toVX;RViewY=toVY;
						ViewX=Math.round(RViewX);
						ViewY=Math.round(RViewY);
						activate();
					}
				}else if(!REP){
					if(REPIND)rep[REPIND].innerText='▶️';
					this.innerText='🔲';
					oTime.strep(this);
				}
			}
		}
	}
}
function covT(t){
	var s=t%60;
	return cov((t-s)/60)+':'+cov(((s*100<<0)/100).toFixed(2));
}
revt=$('revt');
emoji=$('emoji');
emoji.onclick=function(){
	if(emojiShow.style.display==='block'){
		emojiShow.style.display='none';
	}else{
		emojiShow.style.display='block';
	}
}
emojiShow=$('emojiShow');
emojiShow.innerHTML='<span>（￣▽￣）</span><span>(⌒▽⌒)</span><span>(･∀･)</span><span>(=・ω・=)</span><span>(〜￣△￣)〜</span><span>←◡←</span><span>("▔□▔)/</span><span>（￣へ￣）</span><span>(╯°口°)╯</span><span>_(:3」∠)_</span><span>☺️</span><span>😂</span><span>😔</span><span>😧</span><span>😶</span><span>😒</span><span>😠</span><span>👍</span><span>👎</span><span>❤️</span><span>☕</span>';
em=emojiShow.children;
for(var i=em.length;i--;){
	em[i].onclick=function(){
		revt.value+=this.innerText;revt.oninput();
	}
}
revt.oninput=function(){
	if(this.value.length>0){
		rev.style.background='';
		rev.style.borderColor='';
		rev.style.color='';
		this.value=this.value.slice(0,200)
	}else{
		rev.style.background='#EAEAEA';
		rev.style.borderColor='#EAEAEA';
		rev.style.color='#AAA';
	}
}
rev=$('rev');
revt.oninput();
rev.onclick=function(){
	if(revt.value.length>0 && this.style.background===''){
		var r=new AV.Object('GReview');
		var g=AV.Object.createWithoutData('Game',GAME_ID);
		r.set('G',g);
		r.set('U',currentUser);
		r.set('W',revt.value);
		r.save().then(function(){revt.value='';revt.oninput();loadRV()},function(e){});
	}
}
SRV=$('SRV');
lR=$('lR');
lR.onclick=loadRV;
function loadRV(){
	lR.style.display='none';
	var query=new AV.Query('GReview');
	var g=AV.Object.createWithoutData('Game',GAME_ID);
	query.equalTo('G',g);
	query.include('U.username');
	query.descending('createdAt');
	query.find().then(function(r){
		SRV.innerHTML='';
		r.forEach(o=>{
			var a=o._serverData;
			var dt=`${o.createdAt.getFullYear()}-${o.createdAt.getMonth()+1}-${o.createdAt.getDate()} ${cov(o.createdAt.getHours())}:${cov(o.createdAt.getMinutes())}`;
			SRV.innerHTML+=`<h3>${a.U.get('username')}<span>${dt}<span></h3><pre><xmp>${a.W}</xmp></pre>`;
		})
	},function(e){console.log(e)});
}