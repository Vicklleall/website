function mouse(e){
	if(e.pageX || e.pageY)return {x:e.pageX,y:e.pageY};
	return{x:e.clientX+document.body.scrollLeft-document.body.clientLeft,y:e.clientY+document.body.scrollTop-document.body.clientTop};
}
window.onmousemove=function(e){
	e=e||window.event;
	mouse_x=mouse(e).x;mouse_y=mouse(e).y;
	if(Cchs.Dw!==undefined){
		cGC.sc=Math.max(64,Math.min(dUp.height-Number(Cchs.style.top.slice(0,-2)),dUp.width-Number(Cchs.style.left.slice(0,-2)),mouse_x-Cchs.Dw,mouse_y-Cchs.Dh));
		Cchs.style.width=`${cGC.sc}px`;Cchs.style.height=`${cGC.sc}px`
	}else if(Cchs.Dx!==undefined){
		var x=mouse_x-Cchs.Dx,y=mouse_y-Cchs.Dy
		x=Math.min(dUp.width-Number(Cchs.style.width.slice(0,-2)),Math.max(0,x))
		y=Math.min(dUp.height-Number(Cchs.style.height.slice(0,-2)),Math.max(0,y))
		Cchs.style.left=`${x}px`;Cchs.style.top=`${y}px`
	}
	if(RBar.on){
		if(RBar.on===RBC[0]){
			var a=Math.min(Math.max(0,e.clientX-RBar.on.dX),RBC[1].offsetLeft);
			RG.S=(Math.round(a/180*RBar.w)*RBar.step).toFixed(1/RBar.step>>3);
		}else{
			var a=Math.min(Math.max(RBC[0].offsetLeft,e.clientX-RBar.on.dX),180);
			RG.E=(Math.round(a/180*RBar.w)*RBar.step).toFixed(1/RBar.step>>3);
		}
		RG.innerText=`${RG.S}~${RG.E}`
		RBar.on.style.left=a+'px'
	}
}
window.onmouseup=function(){
	if(Cchs.Dw!==undefined || Cchs.Dx!==undefined){
		cGCc.clearRect(0,0,64,64);
		cGCc.drawImage(dUp,Number(Cchs.style.left.slice(0,-2)),Number(Cchs.style.top.slice(0,-2)),cGC.sc,cGC.sc,0,0,64,64);
		Cchs.Dw=undefined;Cchs.Dx=undefined
		document.body.style.cursor='default'
	}
	if(RBar.on)RBar.on=null
}
ipGC=document.createElement('input')
ipGC.type='file';ipGC.accept='image/*'
ipGC.style.display='none';
cGC=$('cGC');cGCc=cGC.getContext('2d')
dUp=$('dUp');dUpc=dUp.getContext('2d')
Cchs=$('Cchs');
Cchs.onmousemove=function(){
	if(this.Dw===undefined && this.Dx===undefined){
		var p=this.getBoundingClientRect();
		if(mouse_x>p.right-20 && mouse_y>p.bottom-20){
			document.body.style.cursor='se-resize'
		}else{
			document.body.style.cursor='move'
		}
	}
}
Cchs.onmouseout=function(){
	if(this.Dw===undefined && this.Dx===undefined){
		document.body.style.cursor='default'
	}
}
Cchs.onmousedown=function(){
	var p=this.getBoundingClientRect();
	if(mouse_x>p.right-20 && mouse_y>p.bottom-20){
		this.Dw=mouse_x-cGC.sc;this.Dh=mouse_y-cGC.sc;
		document.body.style.cursor='se-resize';
	}else{
		this.Dx=mouse_x-Number(this.style.left.slice(0,-2));
		this.Dy=mouse_y-Number(this.style.top.slice(0,-2));
		document.body.style.cursor='move';
	}
}
ipGC.onchange=function(){
	var m=this.files[0],r=new FileReader();r.readAsDataURL(m)
	r.onload=function(){try{
		var o=new Image();o.style.imageRendering='auto'
		o.onload=function(){
			if(this.width<64 || this.height<64){
				alert('图片宽高不能小于64像素！');
				return false;
			}
			while(this.width>1024 || this.height>1024){
				this.width=this.width>>1;
				this.height=this.height>>1;
			}
			dUp.width=this.width;dUp.height=this.height
			dUpc.drawImage(this,0,0,dUp.width,dUp.height)
			cGC.sc=Math.min(dUp.width,dUp.height)
			Cchs.style.width=`${cGC.sc}px`;Cchs.style.height=`${cGC.sc}px`
			var l=(dUp.width-cGC.sc)/2,t=(dUp.height-cGC.sc)/2;
			Cchs.style.left=`${l}px`;Cchs.style.top=`${t}px`
			cGCc.imageSmoothingEnabled=true;cGCc.imageSmoothingQuality='high'
			cGCc.clearRect(0,0,64,64);cGCc.drawImage(dUp,l,t,cGC.sc,cGC.sc,0,0,64,64);
			var b=$('AUp').getElementsByTagName('div');
			b[0].onclick=function(){
				var url=cGC.toDataURL('image/webp',1);
				if(url.length>5000)url=cGC.toDataURL('image/webp',0.92)
				currentUser.set('Avatar',url)
				currentUser.save().then(function(){
					UserPro.style.backgroundImage=`url(${currentUser.get('Avatar')})`
					UserPro.firstChild.style.display='none'
					$('AUp').style.display='none';$('GUI').style.display='none'
				},function(){alert('图片上传失败！')})
			}
			b[1].onclick=function(){
				$('AUp').style.display='none';$('GUI').style.display='none'
			}
			$('GUI').style.display='block';$('AUp').style.display='flex'
		}
		o.src=this.result;
	}catch(e){
		console.log(e);alert('文件读取错误！')
	}}
}
document.body.appendChild(ipGC);