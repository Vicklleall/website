let monoRoom=null,frame=$('frame'),main=$('main'),rand=$('rand')
const wsRoom=location.search.slice(1)
if(!wsRoom)window.location.href='./'
var ws=io.connect('https://server.vicklleall.com/mono')
ws.on('connect',()=>{
	ws.emit('init',{room:wsRoom,u:currentUser.getUsername()})
}).on('crt',e=>{
	let u=currentUser.getUsername(),r={id:e,u:{},c:{},m:[],now:u}
	r.u[u]={p:0,n:0};monoRoom=r
	for(let i=10;i--;)r.m.push(0)
	for(let i=30;i--;)r.m.push(1)
	for(let i=20;i--;)r.m.push(2)
	for(let i=10;i--;)r.m.push(3)
	for(let i=30;i--;)r.m.push(4)
	for(let i=1;i<100;i++){
		let t=r.m[i],j=Math.random()*100<<0;if(j===0)continue;r.m[i]=r.m[j];r.m[j]=t
	}
	createMap()
	ws.emit('crt',[r,u])
}).on('room',e=>{
	monoRoom=e
	createMap()
	checkUser()
}).on('join',e=>{
	monoRoom.u[e[0]]={id:e[1],p:0,n:0}
	showUser(e[0])
	addMsg(e[0]+' 进入房间')
}).on('move',e=>{
	let u=$(e.u)
	monoRoom.u[e.u].p=e.p
	u.innerText=e.u+' '+e.p
	u.style.left=(block[e.n].offsetLeft+33)+'px'
	u.style.top=block[e.n].offsetTop+'px'
}).on('count',e=>{
	if(e.next!==monoRoom.now){
		$('cls').onclick()
		monoRoom.u[e.u].p=e.p
		updateUser(e.u)
		monoRoom.now=e.next
		rand.on=false
		if(monoRoom.now===currentUser.getUsername())rand.onclick()
	}
	checkUser()
}).on('msg',addMsg).on('start',e=>{
	start(e.id,e.t)
}).on('leave',e=>{
	$(e.u).remove()
	delete monoRoom.u[e.u]
	if(e.next!==monoRoom.now){
		$('cls').onclick()
		monoRoom.now=e.next
		rand.on=false
	}
	checkUser()
})

function checkUser(){
	for(let k in monoRoom.u){
		let u=$(k)
		u.style.opacity=(k===monoRoom.now)?1:0.5
		u.style.zIndex=(k===monoRoom.now)?11:10
	}
}

function start(id,t,m){
	EST=t;frame.src='./Play.html?'+id
	frame.style.pointerEvents='auto'
	frame.onload=()=>{
		$('timebar').style.animation=`bar linear ${t}s forwards`
		let w=frame.contentWindow;if(!w.$)return;
		w.$('box').style.display='none'
		w.$('room').style.width='802px'
		w.$('room').style.borderRight='1px solid #AAA'
		frame.clock=setTimeout(()=>$('cls').onclick(),t*1000)
	}
	if(m){
		ws.emit('start',{id:id,t:t})
	}
}

const color=['#DDD','#8E8','#EE8','#E88','#8DF','#AAA','#5B5','#BB5','#B55','#5AC','#999','#4A4','#AA4','#A44','#49B']

function createMap() {
	for(let i=0;i<100;i++){
		block[i].style.background=color[monoRoom.m[i]]
		block[i].style.borderColor=color[monoRoom.m[i]+5]
		block[i].style.color=color[monoRoom.m[i]+10]
	}
	for(let k in monoRoom.u)showUser(k)
}

function showUser(k){
	if($(k))return;
	let u=document.createElement('div'),n=monoRoom.u[k].n
	u.className='user';u.innerText=k+' '+monoRoom.u[k].p;u.id=k
	u.style.left=(block[n].offsetLeft+33)+'px'
	u.style.top=block[n].offsetTop+'px'
	main.append(u)
}

function updateUser(k){
	$(k).innerText=k+' '+monoRoom.u[k].p
}

function wait (t) {
	return new Promise((r, f) => setTimeout(t => r(t), t, t))
}

rand.i=6
rand.src='img/6.gif'
rand.onload=function(){
	if(this.i>1){this.src=`img/${--this.i}.gif`}else{this.style.display='block'}
}
rand.onclick=function(){
	if(this.on||monoRoom.now!==currentUser.getUsername())return;
	let n=1+Math.random()*6<<0
	this.src=`img/${n}.gif`
	this.on=true
	$('timebar').style.animation=''
	setTimeout(async()=>{
		let b=document.getElementsByClassName('block'),u=currentUser.getUsername(),e=$(u)
		monoRoom.u[u].n+=n
		if(monoRoom.u[u].n>99)monoRoom.u[u].n-=100
		if(nowItem.times)monoRoom.u[u].p+=n*10
		updateUser(u)
		e.style.left=(b[monoRoom.u[u].n].offsetLeft+33)+'px'
		e.style.top=b[monoRoom.u[u].n].offsetTop+'px'
		if(Object.keys(monoRoom.u).length>1)ws.emit('move',{u:u,n:monoRoom.u[u].n,p:monoRoom.u[u].p})
		await wait(1000)
		if(nowItem[0]&&nowItem[0].pre){
			let r=nowItem[0].pre();if(r!==0)removeItem();if(r)return;
		}
		switch(monoRoom.m[monoRoom.u[u].n]){
			case 0:rand.on=false;if(Object.keys(monoRoom.u).length>1){
				monoRoom.now='';ws.emit('count',{u:u,p:monoRoom.u[u].p})
			};break
			case 1:start(GAME[0][Math.random()*GAME[0].length<<0].id,60*nowItem.time,1);break
			case 2:start(GAME[1][Math.random()*GAME[1].length<<0].id,120*nowItem.time,1);break
			case 3:start(GAME[2][Math.random()*GAME[2].length<<0].id,240*nowItem.time,1);break
			case 4:rand.on=false;let o=ITEM[Math.random()*ITEM.length<<0]
				ws.emit('msg',`${u} 获得 ${o.name}`);addMsg(`${u} 获得 ${o.name}`)
				if(o.now){if(o.now())return}else{pushItem(o)}
				if(Object.keys(monoRoom.u).length>1){
					monoRoom.now='';ws.emit('count',{u:u,p:monoRoom.u[u].p})
				}
				alert(o.name+': '+o.use)
			break
		}
		nowItem.time=1
	},2000)
}
ITEM=[
{name:'三倍分数',use:'下一关卡得到的分数翻三倍',after:()=>nowItem.times=3},
{name:'过路费',use:'下一关通关前不会得到分数',pre:()=>nowItem.times=0,after:()=>true},
{name:'自动完成',use:'下一关卡自动完成并获得全部分数',pre:()=>{
	let u=currentUser.getUsername(),n=monoRoom.m[monoRoom.u[u].n]
	switch(n){
		case 1:case 2:case 3:
			ws.emit('msg',u+' 使用 自动完成')
			monoRoom.u[u].p+=n*60+(n===3?60:0)
			if(Object.keys(monoRoom.u).length>1){
				monoRoom.now='';ws.emit('count',{u:u,p:monoRoom.u[u].p})
			}
			updateUser(u);rand.on=false
		return true;
	}
}},{name:'双倍时限',use:'下一关卡时限延长到双倍',pre:()=>{
	let u=currentUser.getUsername(),n=monoRoom.m[monoRoom.u[u].n]
	if(n===1||n===2||n===3){
		ws.emit('msg',u+' 使用 双倍时限');nowItem.time=2
	}
}},
{name:'再来一次',use:'额外一次掷骰子机会',now:()=>{rand.onclick();return true}},
{name:'奖励100',use:'获得100分',now:()=>{if(!nowItem.times)return;let u=currentUser.getUsername();monoRoom.u[u].p+=100;updateUser(u)}},
{name:'奖励500',use:'获得500分',now:()=>{if(!nowItem.times)return;let u=currentUser.getUsername();monoRoom.u[u].p+=500;updateUser(u)}}
]
nowItem=[];nowItem.times=1;nowItem.time=1
ITEM.forEach(o=>{
	p=document.createElement('p');p.innerText=` - ${o.name}: ${o.use}`;$('info').append(p)
})

function pushItem(o){
	let e=document.createElement('div');e.innerText=o.name;$('item').append(e)
	o.e=e;nowItem.push(o)
}
function removeItem(){
	let o=nowItem.shift();if(o)o.e.remove()
}

function GameEnd(t){
	if(monoRoom.now===currentUser.getUsername()&&t<EST){
		if(nowItem[0]&&nowItem[0].after){
			nowItem[0].after();removeItem()
		}
		monoRoom.u[monoRoom.now].p+=(t<=EST/2?EST:(EST-t<<0))*nowItem.times
		updateUser(monoRoom.now)
		if(Object.keys(monoRoom.u).length>1){
			let u=monoRoom.now;monoRoom.now=''
			ws.emit('count',{u:u,p:monoRoom.u[u].p})
		}
		rand.on=false;nowItem.times=1
	}
}

let GAME=[[],[],[]],EST=60
function fetchGame(){
	new AV.Query('Game').select(['RP','R','DP','D']).limit(1000).find().then(a=>{a.forEach(r=>{
		if(r.get('DP')){
			let d=r.get('D')/r.get('DP')
			GAME[d<25?0:d<50?1:2].push({id:r.id,r:r.get('R')/r.get('RP'),d:d})
		}
	})})
}
fetchGame()

let w=20,h=11
for(let i=0;i<100;i++){
	let b=document.createElement('div')
	b.className='block'
	b.innerText=i+1
	main.append(b)
	if(i<w){
		b.style.left=i*66+'px'
	}else if(i<w+h) {
		b.style.top=(i-w+1)*66+'px'
		b.style.left=(w-1)*66+'px'
	}else if(i<w+h*2) {
		b.style.top=(w+h*2-i)*66+'px'
		b.style.left=(w-2)*66+'px'
	}else if(i<w+h*3) {
		b.style.top=(i-w-h*2+1)*66+'px'
		b.style.left=(w-3)*66+'px'
	}else if(i<w*2+h*3-6){
		b.style.top=h*66+'px'
		b.style.left=(w*2+h*3-4-i)*66+'px'
	}else if(i<w*2+h*4-6){
		b.style.top=(w*2+h*4-6-i)*66+'px'
		b.style.left='132px'
	}else if(i<w*2+h*5-6){
		b.style.top=(i-w*2-h*3-4)*66+'px'
		b.style.left='66px'
	}else if(i<w*2+h*6-6){
		b.style.top=(w*2+h*6-6-i)*66+'px'
	}
}

block=document.getElementsByClassName('block')

function addMsg(m){
	let p=document.createElement('p');p.innerText=m
	$`msg`.appendChild(p);setTimeout(e=>e.remove(),20000,p)
}
document.addEventListener('keydown',e=>{
	if(e.keyCode===32){
		let m=(prompt('请输入要发送的消息：')||'').slice(0,30)
		if(m.trim()){
			m=currentUser.getUsername()+': '+m;addMsg(m)
			ws.emit('msg',m)
		}
	}
})

$('cls').onclick=()=>{
	if(monoRoom.now===currentUser.getUsername()){
		if(Object.keys(monoRoom.u).length>1){
			let u=monoRoom.now;monoRoom.now=''
			ws.emit('count',{u:u,p:monoRoom.u[u].p})
		}
		rand.on=false
	}
	$('timebar').style.animation='none'
	frame.src='';clearTimeout(frame.clock)
	frame.style.pointerEvents=''
}