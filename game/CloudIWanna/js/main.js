ObjT[2].click();
ViewX=0;ViewY=0;toVX=0;toVY=0;RViewX=0;RViewY=0;OutMode=0;ViewMode=0;
SAVE=undefined;RECT=undefined;SRECT=undefined;
function room_load(f,r,rl){
	roomInit();
	if(f){
		loadRoom(rl);
	}else{
		RoomW=800;RoomH=608;startX=401;startY=567;
		warpEnd=new objWarp(384,448,true);
		block.push(new objBlock(0,576,800,32));
	}
	if(!r){saveX=startX;saveY=startY;JumpNum=1};
	blood=null;
	scrT.clearRect(0,0,tiles.width,tiles.height);
	if(edit){player=null}else{drawInit()};
	resetPro();resetGUI();
	GCobj[3].style.top='600px';
	if(player){
		toVX=Math.max(400,Math.min(player.x,RoomW-400))-400;
		toVY=Math.max(304,Math.min(player.y,RoomH-304))-304;
		RViewX=toVX;RViewY=toVY;
		ViewX=Math.round(RViewX);
		ViewY=Math.round(RViewY);
		activate();
	}
	if(shooter.length>9)oBGM.currentTime=0
}
function loadMap(){
	if(window.localStorage[NowGame]){
		saveData=JSON.parse(window.localStorage[NowGame]);
		room_load(saveData['V'],false,true);
	}
}
END=false;
BGx=0;BGy=0;
function evDraw(){
	drawBegin();
	if(!edit)drawBGEF();
	draw_obj(field);
	if(edit){
		draw_obj(spike,scr);
		block.forEach(o=>{
			if(o.__proto__.constructor.name!=='objCrate'&&o.__proto__.constructor.name!=='objBreakableBlock')o.draw_self();
		})
		draw_obj(Walljump);
		draw_obj(board);
	}else{
		spike.forEach(o=>{
			if(o.__proto__.constructor.name!=='objSpike')o.draw_self(scr);
		})
	}
	block.forEach(o=>{
		if(o.__proto__.constructor.name==='objCrate'||o.__proto__.constructor.name==='objBreakableBlock')o.draw_self();
	})
	draw_obj(conveyor);
	draw_obj(GMStepObj);
	draw_obj(GMColObj,scr);
	draw_obj(platform);
	draw_obj(cherry,scr);
	draw_obj(bullet,0,true);
	if(edit){
		Rcherry.forEach(o=>o.draw_self());
		shooter.forEach(o=>o.draw_self());
		scr.drawImage(sprPlayerStart[0],startX-17,startY-23);
	}
	draw_obj(objEf,0,true);
	if(player){
		player.draw_self();
		px.innerText='x:'+player.x;
		py.innerText='y:'+player.y.toFixed(2);
		pa.innerText='align:'+player.x%3;
	}
	if(blood){
		if(blood.length<50){
			for(var i=5;i--;){
				blood.push(new objBlood(bloodX,bloodY));
			}
		}
		blood.forEach(o=>o.draw_self());
	}
	warpEnd.draw_self(scr);
	if(edit)scr.strokeStyle='#FFD400';
	draw_obj(warp,scr);
	draw_obj(water);
	if(edit){
	if(canPut){
		var spr=getObjSpr();
		if(spr.ang===undefined)spr.ang=0;
		if(spr.Ox===undefined)spr.Ox=0;
		if(spr.Oy===undefined)spr.Oy=0;
		if(spr.sc===undefined)spr.sc=1;
		if(spr){
			scr.globalAlpha=0.5;
			if(spr.sc<1)scr.imageSmoothingEnabled=true;
			draw_sprite_angle(scr,spr.spr[spr.ind||0],mouseX,mouseY,spr.ang,spr.Ox,spr.Oy,spr.sc,spr.sc);
			scr.imageSmoothingEnabled=false;
			scr.globalAlpha=1;
		}
	}
	}else{
		drawEf();
	}
	if(!edit && !END){
		Time.time=(Date.now()-Time.cTime)/1000;
		var s=Time.time%60;
		Time.innerText=cov((Time.time-s)/60)+':'+cov(s<<0);
	}
	HBar.style.width=800/RoomW*800+'px';
	HBar.style.left=`${9+ViewX/RoomW*800}px`;
	VBar.style.height=608/RoomH*608+'px';
	VBar.style.top=`${39+ViewY/RoomH*608}px`;
	Gshow.style.backgroundPosition=`${-ViewX}px ${-ViewY}px`
}
function ProCheck(){
	if(tab[2].style.left=='0px'){
		if(nowObj){
			if(pObjPro){
				for(var i=pObjPro.length;i--;){
					if(pObjPro[i]!=document.activeElement)pObjPro[i].value=nowObj[pObjPro[i].id];
				}
			}
			scr.strokeStyle='#FFD400';
			if(nowObj.drawW){
				var l=nowObj.cX-nowObj.drawW;
				var t=nowObj.cY-nowObj.drawH;
				var [w,h]=[nowObj.drawW*2,nowObj.drawH*2];
			}else{
				var l=nowObj.x;var t=nowObj.y;
				var [w,h]=[nowObj.w,nowObj.h];
			}
			if(nowObj.Ox)l-=nowObj.Ox;
			if(nowObj.Oy)t-=nowObj.Oy;
			scr.strokeRect(l,t,w,h);
			scr.beginPath();
			scr.arc(nowObj.x,nowObj.y,1,0,PI*2);
			scr.arc(nowObj.x,nowObj.y,2,0,PI*2);
			scr.stroke();
		}
	}
}
UDLRObj=[]
function UDLR(){
	if(CreateObj){
		var d=null;
		if(keyboard_check_pressed(vk_up)){
			d='Up';
		}else if(keyboard_check_pressed(vk_down)){
			d='Down';
		}else if(keyboard_check_pressed(vk_left)){
			d='Left';
		}else if(keyboard_check_pressed(vk_right)){
			d='Right';
		}
		if(d){
			switch(CreateObj){
			case 'Spike Up':case 'Spike Down':case 'Spike Left':case 'Spike Right':
			case 'Mini Spike Up':case 'Mini Spike Down':case 'Mini Spike Left':case 'Mini Spike Right':
			case 'Conveyor Up':case 'Conveyor Down':case 'Conveyor Left':case 'Conveyor Right':
				CreateObj=CreateObj.replace(/Up|Down|Left|Right/g,d);
			break;
			}
		}
	}
}
function GameRun(){
if(loadCount>sprNumber+1){
	if(!edit){
		if(keyDown[82]){
			if(END){
				Time.time=0;Death.death=0;
				Death.innerText='0';
				Time.cTime=Date.now();
				END=false;
			}
			keyPause();
			room_load(true,true);
			keyDown[82]=false;
		}
	}
	key_check(vk_right);key_check(vk_left);
	key_check(vk_up);key_check(vk_down);
	key_check(vk_shift);key_check(vk_ctrl);
	key_check(vk_Z);key_check(vk_S);
	if(!edit && !END){evStep();BGEF();evMove()};
	evDraw();
	if(edit){
		ProCheck();UDLR();
	}
	key_clear(vk_right);key_clear(vk_left);
	key_clear(vk_up);key_clear(vk_down);
	key_clear(vk_shift);key_clear(vk_ctrl);
	key_clear(vk_Z);key_clear(vk_S);
}else if(loadCount===sprNumber){
	loadCount=sprNumber+1;OpenGame();
}
if(loadCount===sprNumber+82){
	resetSPR();loadCount=100;
}
}
function OpenGame(){
	Loading.style.display='none';
	ShowGUI.style.display='block';
	$('ShowBox').style.display='block';
	ShowTitle.innerText='打开项目';
	var html='';
	for(var i=0;i<5;i++){
		if(localStorage['Game '+i]){
			var o=JSON.parse(localStorage['Game '+i]);
			if(o.N===''){var str='Untitled'}else{var str=o.N};
			if(o.V<0.4){
				localStorage.removeItem('Game '+i);
				html+=`<div class='OpGame'><div>➕ Create A New Game</div></div>`;
			}else{
				html+=`<div class='OpGame'><span>⛔</span><div>🔵 ${str}</div></div>`;
			}
		}else{
			html+=`<div class='OpGame'><div>➕ Create A New Game</div></div>`;
		}
	}
	BoxCt.innerHTML=html;
	var o=BoxCt.children;
	for(var i=0;i<o.length;i++){
		var cl=o[i].firstChild;
		cl.ind=i;
		cl.onclick=function(){
			if(confirm('⚠️确定要删除此游戏？')){
				window.localStorage.removeItem('Game '+this.ind);OpenGame();
			}
		}
		cl=o[i].lastChild;cl.ind=i;
		cl.onclick=function(){
			NowGame='Game '+this.ind;
			if(!localStorage[NowGame]){
				room_load();ObjT[2].click();RoomOut[0].click();GameName.value='';
				BGMid.value='22776365';BGMid.onchange();
			}else{loadMap()}
			loadCount=100;CloseShow();
		}
	}
}
$('ShowBox').style.display='none';
ShowGUI.style.display='block';
setInterval(GameRun,20);