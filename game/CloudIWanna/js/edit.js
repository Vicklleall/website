edit=true;
const REC=false;const REP=false;
function GameEnd(){
	END=true;
	GO.style.visibility='hidden';
	GC.style.visibility='visible';
	GUI.style.transition="opacity 1s 0.4s";
	GUI.style.visibility='visible';
	GUI.style.opacity='1';
	GCobj[0].style.right='420px';
	GCobj[1].style.left='420px';
	GCobj[2].innerText="⏱️ Time: "+Time.innerText;
	GCobj[3].innerText="💀 Deaths: "+Death.innerText;
	setTimeout("GCobj[0].style.top='-60px';GCobj[1].style.top='-60px';GCobj[2].style.top='80px';GCobj[3].style.top='160px'",1200);
}
LOAD();
make=$('make');run=$('run');mode=$('mode');
protect=document.getElementsByClassName('pt');
PViewX=0;PViewY=0;
mode.onclick=function(){
	if(!NowGame)return false;
	edit=!edit;
	if(edit){
		room_load(true,true);
		grid.on=true;showGrid(grid.on);
		this.innerHTML="<span>▶️运行</span>";
		run.style.display='none';
		make.style.display='inline-block';
		for(var i=protect.length;i--;){
			protect[i].style.display='none';
		}
		ViewX=PViewX;ViewY=PViewY;
		toVX=ViewX;toVY=ViewY;
		RViewX=ViewX;RViewY=ViewY;
		sfbg.clearRect(0,0,800,608)
	}else{
		for(var i=protect.length;i--;)protect[i].style.display='block'
		saveRoom();
		tiles.width=RoomW;tiles.height=RoomH;
		room_load(true,true);END=false;
		grid.on=false;showGrid(grid.on);
		this.innerHTML="<span>📝编辑</span>";
		make.style.display='none';
		run.style.display='inline-block';
		Time.time=0;Death.death=0;
		Time.cTime=Date.now();
		PViewX=ViewX;PViewY=ViewY;
		ViewX=ViewSX;ViewY=ViewSY;
		RViewX=ViewX;RViewY=ViewY;
		toVX=ViewX;toVY=ViewY;activate();
	}
	BGx=0;BGy=0;BKEF=[];saveEf=[]
}
grid=$('grid');grid.on=true;
GS=$('GS');Gs=grid.children;Gs.value=32;
Gshow=$('Gshow');
function showGrid(a){
	if(a){
		GS.innerText='网格(On)';
		grid.style.color='#222';
		grid.style.borderColor='#666';
		Gshow.style.display='block';
	}else{
		GS.innerText='网格(Off)';
		grid.style.color='#AAA';
		grid.style.borderColor='#CCC';
		Gshow.style.display='none';
	}
}
showGrid(true);
GS.onclick=function(){
	grid.on=!grid.on;showGrid(grid.on);
}
for(var i=1;i<Gs.length;i++){
	Gs[i].onclick=function(){
		Gs.value=Number(this.innerText);setGrid();
	}
}
function setGrid(){
	Gshow.style.backgroundSize=`100% ${Gs.value}px,${Gs.value}px 100%,100% 608px,800px,100%`;
}
px=$('px');py=$('py');pa=$('pa');
CreateObj='';canPut=false;
OC=document.getElementsByClassName('OC');
for(var i=0;i<OC.length;i++){
	if(OC[i].title){
		OC[i].ins=OC[i].title;OC[i].title='';
	}
	OC[i].onclick=function(){
		CreateObj=this.id;
		for(var i=0;i<OC.length;i++){
			OC[i].style.borderColor='#CCC';
		}
		this.style.borderColor='hsl(50,80%,60%)';
	}
	OC[i].onmouseover=function(){
		objMouse.style.display='block';
		if(this.ins){
			objMouse.innerText=`${this.id} （请勿逐个摆放）`;
		}else{
			objMouse.innerText=this.id;
		}
	}
	OC[i].onmouseout=function(){
		objMouse.style.display='none';
	}
}
mouseX=0;mouseY=0;mouseTX=0;mouseTY=0;
objMouse=$('objMouse');
window.onmousemove=function(e){
	e=e||window.event;
	mouse_x=mouse(e).x;mouse_y=mouse(e).y;
	if(ShowGUI.style.display==='block'){
		if(Cchs.Dw!==undefined){
			cGC.sc=Math.max(96,Math.min(dUp.height-Number(Cchs.style.top.slice(0,-2)),dUp.width-Number(Cchs.style.left.slice(0,-2)),mouse_x-Cchs.Dw,mouse_y-Cchs.Dh));
			Cchs.style.width=`${cGC.sc}px`;
			Cchs.style.height=`${cGC.sc}px`;
		}else if(Cchs.Dx!==undefined){
			var x=mouse_x-Cchs.Dx;
			var y=mouse_y-Cchs.Dy;
			x=Math.min(dUp.width-Number(Cchs.style.width.slice(0,-2)),Math.max(0,x));
			y=Math.min(dUp.height-Number(Cchs.style.height.slice(0,-2)),Math.max(0,y));
			Cchs.style.left=`${x}px`;
			Cchs.style.top=`${y}px`;
		}
		return false;
	}
	var p=scrn.getBoundingClientRect();
	mouseTX=(mouse_x-p.left-1+ViewX);
	mouseTY=(mouse_y-p.top-1+ViewY);
	if(mouse_x-1>p.left && mouse_x+1<p.right && mouse_y-1>p.top && mouse_y+1<p.bottom){
		mouseX=Math.floor(mouseTX/Gs.value)*Gs.value;
		mouseY=Math.floor(mouseTY/Gs.value)*Gs.value;
		objMouse.style.display='block';
		objMouse.innerText='('+mouseX+','+mouseY+')';
		if(edit){
			if(tab[2].style.left==='0px'  || (tab[1].style.left=='0px' && keyboard_check(vk_ctrl))){
				if(mouseCheck){
					if(ev_mouse.button===0){
						if(DragObj){DragObj.x=mouseX+DragX;DragObj.y=mouseY+DragY}
					}else if(keyboard_check(vk_shift)){
						mouseObj(ev_mouse);
					}
				}
			}else if(tab[1].style.left==='0px'){
				canPut=true;
				if(mouseCheck){
					var c=true;
					if(ev_mouse.button===0){
						if(DragObj){DragObj.setScale(mouseX-DragObj.x+32,mouseY-DragObj.y+32);c=false}
					}
					if(c && keyboard_check(vk_shift))mouseObj(ev_mouse);
				}
			}
		}
	}else{
		if(tab[3].style.left==='0px'){
			if(mouseCheck){
				if(slider.dragi>=0){
					oSlider[slider.dragi].style.left=Math.min(100,Math.max(0,(mouse_x-slider.pos.left-6)/1.72))+'%';
					setGra();
				}
			}
		}
		if(objMouse.innerText.includes('('))objMouse.style.display='none';
		canPut=false;
	}
	if(objMouse.style.display==='block'){
		objMouse.style.left=`${mouse_x+4}px`;
		objMouse.style.top=`${mouse_y-24}px`;
	}
	if(edit){
		if(HBar.on){
			ViewX=Math.max(0,Math.min(RoomW-800,(mouse_x-HBar.DragX)/800*RoomW+HBar.VX));
		}else if(VBar.on){
			ViewY=Math.max(0,Math.min(RoomH-608,(mouse_y-VBar.DragY)/608*RoomH+VBar.VY));
		}
	}
}
function mouse(e){
	if(e.pageX || e.pageY)return{x:e.pageX,y:e.pageY};
	return{x:e.clientX+document.body.scrollLeft-document.body.clientLeft,y:e.clientY+document.body.scrollTop-document.body.clientTop};
}
function selectObj(x,y,p,s){
	return pos_obj(x,y,water)||pos_obj(x,y,warp)||pos_obj(x,y,trigger)||pos_obj(x,y,Rcherry)||pos_obj(x,y,shooter)||pos_obj(x,y,cherry)||pos_obj(x,y,platform)||pos_obj(x,y,GMColObj)||pos_obj(x,y,GMStepObj)||pos_obj(x,y,conveyor)||pos_obj(x,y,board)||pos_obj(x,y,Walljump)||pos_obj(x,y,block)||pos_obj(x,y,spike)||pos_obj(x,y,field)
}
function destroy_place(x,y,o,f){
	for(var i=o.length;i--;){
		if(o[i].__proto__.constructor===f){
			var ax=0;var ay=0;
			switch(o[i].__proto__.constructor.name){
				case 'objSpring':ax=16;ay=16;break;
				case 'objTrampoline':ax=16;ay=32-(sprTp[0].height>>1);break;
			}
			if(o[i].x===x+ax && o[i].y===y+ay){
				destroy(o[i].obj_ind,o[i].id);
			}
		}
	}
}
function mouseObj(e){
if(e.button===2){
	var o=selectObj(mouseTX,mouseTY);
	if(o)destroy(o.obj_ind,o.id);
}else if(canPut){
	if(e.button===0 && CreateObj){
		if(CreateObj=='Player Start'){
			startX=mouseX+17;startY=mouseY+23;saveX=startX;saveY=startY;
		}else if(CreateObj=='Warp End'){
			warpEnd=new objWarp(mouseX,mouseY,true);
		}else if(CreateObj==='Rounding Cherry'){
			Rcherry.push(new objRoundingCherry(mouseX,mouseY));
			ico[2].click();setPro(Rcherry[Rcherry.length-1]);
		}else if(CreateObj==='Spike Gear'){
			Rcherry.push(new objSpikeGear(mouseX,mouseY));
			ico[2].click();setPro(Rcherry[Rcherry.length-1]);
		}else if(CreateObj.includes('Split')){
			let c=CreateObj[0]
			shooter.push(new objShooter(mouseX,mouseY,c=='T'?1:c=='R'?2:c=='P'?3:c=='H'?4:0))
			ico[2].click();setPro(shooter[shooter.length-1])
		}else if(CreateObj.includes('Spike')){
			var sc=32-CreateObj.includes('Mini')*16;
			var ang=CreateObj.includes('Down')*180+CreateObj.includes('Left')*90+CreateObj.includes('Right')*270;
			for(var i=spike.length;i--;){
				if(spike[i].x===mouseX && spike[i].y===mouseY && spike[i].img_ang===ang && spike[i].w===sc && spike[i].h===sc){
					destroy(spike,spike[i].id);
				}
			}
			spike.push(new objSpike(mouseX,mouseY,ang));
			if(sc!==32)spike[spike.length-1].setScale(sc,sc);
		}else if(CreateObj.includes('Wall Jump')){
			var xx=CreateObj.includes('Left')*18;
			for(var i=Walljump.length;i--;){
				if(Walljump[i].x===mouseX+xx && Walljump[i].y===mouseY){
						destroy(Walljump,Walljump[i].id);
				}
			}
			Walljump.push(new objWallJump(mouseX+xx,mouseY,(xx-9)/9));
		}else if(CreateObj.includes('Conveyor')){
			var fx=Number(CreateObj.includes('Down')||CreateObj.includes('Up'));
			var a=1-(CreateObj.includes('Left')||CreateObj.includes('Up'))*2;
			destroy_place(mouseX,mouseY,conveyor,objConveyor);
			var i=conveyor.push(new objConveyor(mouseX,mouseY));
			DragObj=conveyor[i-1];DragObj.fx=fx;DragObj.spd=Conveyor*a;
		}else if(CreateObj.includes('Fake')){
			destroy_place(mouseX,mouseY,GMColObj,objFake);
			var ind;
			switch(CreateObj){
				case 'Fake Floor':ind=0;break;
				case 'Fake Block':ind=1;break;
				case 'Fake Platform':ind=2;break;
			}
			GMColObj.push(new objFake(mouseX,mouseY,ind));
		}else{
			var Cobj=NameToObj(CreateObj);
			destroy_place(mouseX,mouseY,Cobj.obj,Cobj.f);
			switch(CreateObj){
			case 'Warp':case 'Board':
				Cobj.obj.push(new Cobj.f(mouseX,mouseY));
				ico[2].click();setPro(Cobj.obj[Cobj.obj.length-1]);
			break;
			case 'Block':case 'Water':case 'Ice Block':case 'Trigger':
				Cobj.obj.push(new Cobj.f(mouseX,mouseY,32,32));
				DragObj=Cobj.obj[Cobj.obj.length-1];
			break;
			case 'Triple Jump':Cobj.obj.push(new Cobj.f(mouseX,mouseY,2));break;
			case 'Double Jump':Cobj.obj.push(new Cobj.f(mouseX,mouseY,1));break;
			case 'Single Jump':Cobj.obj.push(new Cobj.f(mouseX,mouseY,0));break;
			case 'Infinite Jump':Cobj.obj.push(new Cobj.f(mouseX,mouseY,Infinity));break;
			case 'Low Gravity':
				Cobj.obj.push(new Cobj.f(mouseX,mouseY,32,32,0));
				DragObj=Cobj.obj[Cobj.obj.length-1];
			break;
			case 'High Gravity':
				Cobj.obj.push(new Cobj.f(mouseX,mouseY,32,32,1));
				DragObj=Cobj.obj[Cobj.obj.length-1];
			break;
			case 'Fast Speed':
				Cobj.obj.push(new Cobj.f(mouseX,mouseY,32,32,2));
				DragObj=Cobj.obj[Cobj.obj.length-1];
			break;
			case 'Slow Speed':
				Cobj.obj.push(new Cobj.f(mouseX,mouseY,32,32,3));
				DragObj=Cobj.obj[Cobj.obj.length-1];
			break;
			case 'Spring':Cobj.obj.push(new Cobj.f(mouseX+16,mouseY+16,0));break;
			case 'Trampoline':Cobj.obj.push(new Cobj.f(mouseX+16,mouseY+32-(sprTp[0].height>>1),0));break;
			default:Cobj.obj.push(new Cobj.f(mouseX,mouseY));
			}
		}
	}
}
}
mouseCheck=false;
window.onmousedown=function(e){
	e=e||window.event;ev_mouse=e;
	if(e.button===0 || e.button===2){
		mouseCheck=true;mouseDownX=mouseX;mouseDownY=mouseY;
	}
	if(edit){
	if(tab[2].style.left=='0px' || (tab[1].style.left=='0px' && keyboard_check(vk_ctrl))){
		if(e.button===0){
			var xx=mouseTX-ViewX;var yy=mouseTY-ViewY;
			if(xx>0 && yy>0 && xx<800 && yy<608){
				var o=selectObj(mouseTX,mouseTY);
				if(o){
					setPro(o);DragObj=o;
					DragX=o.x-mouseDownX;DragY=o.y-mouseDownY;
				}else{resetPro()};
			}
		}else if(e.button===2){
			var o=selectObj(mouseTX,mouseTY);
			if(o)destroy(o.obj_ind,o.id);
		}
	}else{mouseObj(e)}
	}
}
window.onmouseup=function(){
	mouseCheck=false;slider.dragi=-1;DragObj=undefined;
	HBar.on=false;VBar.on=false;
	if(Cchs.Dw!==undefined || Cchs.Dx!==undefined){
		cGCc.clearRect(0,0,96,96);
		cGCc.drawImage(dUp,Number(Cchs.style.left.slice(0,-2)),Number(Cchs.style.top.slice(0,-2)),cGC.sc,cGC.sc,0,0,96,96);
		Cchs.Dw=undefined;Cchs.Dx=undefined;
		document.body.style.cursor='default';
	}
}
window.oncontextmenu=function(e){
	e=e||window.event;e.preventDefault();
};
function NameToObj(n){
	switch(n){
	case'Block':return{obj:block,f:objBlock};break;
	case'Ice Block':return{obj:block,f:objIceBlock};break;
	case'Breakable Block':return{obj:block,f:objBreakableBlock};break;
	case'Delicious Fruit':return{obj:cherry,f:objCherry};break;
	case'Platform':return{obj:platform,f:objPlatform};break;
	case'Water':return{obj:water,f:objWater};break;
	case'Warp':return{obj:warp,f:objWarp};break;
	case'Save Point':return{obj:GMStepObj,f:objSavePoint};break;
	case'Crate':return{obj:block,f:objCrate};break;
	case'Board':return{obj:board,f:objBoard};break;
	case'Jump Refresher':return{obj:GMColObj,f:objJumpRefresher};break;
	case'Spring':return{obj:GMColObj,f:objSpring};break;
	case'Trampoline':return{obj:GMColObj,f:objTrampoline};break;
	case'Triple Jump':case'Double Jump':case'Single Jump':case'Infinite Jump':return{obj:GMColObj,f:objJumpChanger};break;
	case'Low Gravity':case'High Gravity':case'Fast Speed':case'Slow Speed':return{obj:field,f:objField};break;
	case'Trigger':return{obj:trigger,f:objTrigger};break;
	}
}
function getObjSpr(){
	switch(CreateObj){
	case'Block':return{spr:sprFloor};break;
	case'Ice Block':return{spr:sprFloor};break;
	case'Breakable Block':return{spr:sprBBlock};break;
	case'Spike Up':return{spr:sprSpike};break;
	case'Spike Down':return{spr:sprSpike,ang:180,Ox:32,Oy:32};break;
	case'Spike Left':return{spr:sprSpike,ang:90,Ox:32};break;
	case'Spike Right':return{spr:sprSpike,ang:270,Oy:32};break;
	case'Mini Spike Up':return{spr:sprSpike,sc:0.5};break;
	case'Mini Spike Down':return{spr:sprSpike,ang:180,sc:0.5,Ox:16,Oy:16};break;
	case'Mini Spike Left':return{spr:sprSpike,ang:90,sc:0.5,Ox:16};break;
	case'Mini Spike Right':return{spr:sprSpike,ang:270,sc:0.5,Oy:16};break;
	case'Delicious Fruit':return{spr:sprCherry,Ox:10,Oy:12};break;
	case'Platform':case'Water':case'Crate':case'Board':case'Trigger':return{spr:window['spr'+CreateObj]};break;
	case'Wall Jump Right':return{spr:sprWallJump};break;
	case'Wall Jump Left':return{spr:sprWallJump,Ox:-18};break;
	case'Save Point':return{spr:sprSave};break;
	case'Player Start':return{spr:sprPlayerStart};break;
	case'Warp End':case'Warp':return{spr:sprWarp};break;
	case'Rounding Cherry':return{spr:sprRCherry,Ox:10,Oy:12};break;
	case'Spike Gear':return{spr:sprSG,Ox:12,Oy:12};break;
	case'Jump Refresher':return{spr:sprJR,Ox:sprJR[0].width>>1,Oy:sprJR[0].height>>1};break;
	case'Spring':return{spr:sprSpring,Ox:sprSpring[0].width>>1-16,Oy:sprSpring[0].height>>1-16};break;
	case'Trampoline':return{spr:sprTp,Ox:0,Oy:sprTp[0].height-32};break;
	case'Conveyor Up':return{spr:sprConveyor,ang:90,Ox:32};break;
	case'Conveyor Down':return{spr:sprConveyor,ang:270,Oy:32};break;
	case'Conveyor Left':return{spr:sprConveyor,ang:180,Ox:32,Oy:32};break;
	case'Conveyor Right':return{spr:sprConveyor};break;
	case'Triple Jump':return{spr:sprTJump};break;
	case'Double Jump':return{spr:sprDJump};break;
	case'Single Jump':return{spr:sprSJump};break;
	case'Infinite Jump':return{spr:sprIJump};break;
	case'Low Gravity':return{spr:sprLGrav};break;
	case'High Gravity':return{spr:sprHGrav};break;
	case'Fast Speed':return{spr:sprFSpd};break;
	case'Slow Speed':return{spr:sprSSpd};break;
	case'Fake Floor':return{spr:sprFloor};break;
	case'Fake Block':return{spr:sprBlock};break;
	case'Fake Platform':return{spr:sprPlatform};break;
	case'Circular Split':return{spr:sprShooter,Ox:12,Oy:12};break;
	case'Triangle Split':return{spr:sprShooter,Ox:12,Oy:12,ind:1};break;
	case'Rectangle Split':return{spr:sprShooter,Ox:12,Oy:12,ind:2};break;
	case'Pentagon Split':return{spr:sprShooter,Ox:12,Oy:12,ind:3};break;
	case'Hexagon Split':return{spr:sprShooter,Ox:12,Oy:12,ind:4};break;
	}
	return false;
}
//Property
pObj=$('pObj');pObjSpr=$('pObjSpr');
pObjName=$('pObjName');pObjId=$('pObjId');
pObjPro=null;nowObj=null;
function setPro(o){
	var obj=o.__proto__.constructor.name;
	nowObj=o;var spr=obj==='objShooter'?o.spr[o.mode]:o.spr[0];
	pObjSpr.style.setProperty('background-image',"url('"+spr.src+"')");
	pObjSpr.style.setProperty('background-position','center');
	pObjName.innerText=obj;
	pObjId.innerText='id:'+o.id;
	var tempHTML="<span>位置: </span><span>x:</span><input id='x' type='number'/><span>y:</span><input id='y' type='number'/>"
	switch(obj){
	case'objWarp':
		tempHTML+="<br/><span>传送点: </span><span>x:</span><input id='warpX' type='number'/><span>y:</span><input id='warpY' type='number'/><span>注：Kid中心坐标为(17,23)</span><br/><span>过渡效果:</span><input id='k' type='number' min='0' max='3'/><span>( 0=淡入淡出 1=传送 2=旋转矩形 3=无 )"
	break;
	case'objBlock':case'objIceBlock':case'objWater':case'objField':case'objTrigger':
		tempHTML+="<br/><span>大小: </span><span>w:</span><input class='sc' id='w' type='number' min='32' max='800'/><span>h:</span><input class='sc' id='h' type='number' min='32' max='800'/>"
	break;
	case'objSpike':
		tempHTML+="<span>角度: </span><input id='img_ang' type='number' min='0' name='360'/><br/><span>大小: </span><span>w:</span><input class='sc' id='w' type='number' min='16'/><span>h:</span><input class='sc' id='h' type='number' min='16'/>"
	break;
	case'objCherry':case'objPlatform':
		tempHTML+="<br/><span>速度: </span><span>hspeed:</span><input id='hspeed' type='number' step='1'/><span>vspeed:</span><input id='vspeed' type='number' step='1'/>"
	break;
	case'objWallJump':
		if(o.side===1){
			pObjSpr.style.setProperty('background-position','22px 4px');tempHTML+='<span>方向：Left</span>';
		}else{
			pObjSpr.style.setProperty('background-position','4px 4px');tempHTML+='<span>方向：Right</span>';
		}
	break;
	case'objRoundingCherry':
		tempHTML+="<span>数量: </span><input id='n' type='number' min='1'/><br/><span>半径：</span><input id='r' type='number' min='0'/><span>转速：</span><input id='spd' type='number' step='1'/><span>初始角度：</span><input id='ang' type='number' min='0' name='360'/>"
	break;
	case'objSpikeGear':
		tempHTML+="<span>数量: </span><input id='n' type='number' min='1'/><br/><span>大小：</span><input id='r' type='number' min='16'/><span>转速：</span><input id='spd' type='number' step='1'/><span>初始角度：</span><input id='ang' type='number' min='0' name='360'/>"
	break;
	case'objShooter':
		tempHTML+="<span>数量: </span><input id='n' type='number' min='1'/><br/><span>速度：</span><input id='spd' type='number' min='0.1' step='0.1'/><span>时间点：</span><input id='t' type='number' step='1'/><span>初始角度：</span><input id='ang' type='number' min='0' name='360'/>"
	break;
	case'objSpring':case'objTrampoline':
		tempHTML+="<span>角度: </span><input id='ang' type='number' min='0' name='360'/><span>弹力: </span><input id='spd' type='number' min='0'/>"
	break;
	case'objBoard':
		tempHTML+="<br/><span>内容: </span><textarea id='txt' maxlength='200'/>"
	break;
	case'objConveyor':
		tempHTML+="<span>速度: </span><input id='spd' type='number' step='1'/><span>长度: </span><input id='CW' type='number' min='32'/>"
	break;
	}
	pObj.innerHTML=tempHTML;
	var p=pObj.getElementsByTagName('input');pObjPro=[];
	for(var i=0;i<p.length;i++){
		if(p[i].type=='number'){
		p[i].mod=Number(p[i].name);
		p[i].num=Number(p[i].step);
		p[i].step='0.00000000000000001';
		p[i].onchange=function(){
			if(this.min!=='')this.value=Math.max(this.value,this.min);
			if(this.max!=='')this.value=Math.min(this.value,this.max);
			if(this.mod>0)this.value=this.value%this.mod;
			if(this.id==='n'&&o.evDes)o.evDes();
			if(this.value!==NaN)o[this.id]=Number(Number(this.value).toFixed(this.num));
			if(this.id==='spd'){
				switch(o.__proto__.constructor.name){
				case'objSpring':Spring=o[this.id];break;
				case'objTrampoline':Trampoline=o[this.id];break;
				case'objConveyor':Conveyor=Math.abs(o[this.id]);break;
				}
			}else if(this.id==='k'){Trans=o[this.id]}
			if(this.className==='sc')o.setScale(o.w,o.h);
		}
		}
		pObjPro.push(p[i]);
	}
	p=pObj.getElementsByTagName('textarea');
	if(p.length>0){
		p[0].onchange=function(){o[this.id]=this.value}
		pObjPro.push(p[0]);
	}
}
function resetPro(){
	pObjSpr.style.setProperty('background-image','none');
	pObjName.innerText='未选择物体';
	pObjId.innerText='';pObj.innerHTML='';nowObj=undefined;
}
//ShowBox
ShowGUI=$('ShowBack');ShowTitle=$('ShowTitle');BoxCt=$('BoxCt');
function removeH(o){
	if(o.className.charAt(o.className.length-1)==='H')o.className=o.className.slice(0,o.className.length-2);
}
//Room Options
oRoomW=$('RoomW');oRoomH=$('RoomH');
RoomW=800;RoomH=608;
oRoomW.onchange=function(){
	try{
		this.value=Math.max(800,Math.min(4000,this.value)).toFixed(0);
	}catch(e){this.value=800}
	RoomW=Number(this.value);
	if(ViewX>RoomW-800)ViewX=0;
}
oRoomH.onchange=function(){
	try{
		this.value=Math.max(608,Math.min(3040,this.value)).toFixed(0);
	}catch(e){this.value=608}
	RoomH=Number(this.value);
	if(ViewY>RoomH-608)ViewY=0;
}
RoomOut=$('OutMode').getElementsByClassName('chsBox');
for(let i=RoomOut.length;i--;){
	RoomOut[i].ind=i;
	RoomOut[i].onclick=function(){
		for(let i=RoomOut.length;i--;)removeH(RoomOut[i]);
		this.className+=' H';OutMode=this.ind;
	}
}
oView=$('ViewMode').getElementsByClassName('chsBox');
for(let i=oView.length;i--;){
	oView[i].ind=i;
	oView[i].onclick=function(){
		for(let i=oView.length;i--;)removeH(oView[i]);
		this.className+=' H';ViewMode=this.ind;
	}
}
oViewX=$('ViewX');oViewY=$('ViewY');
ViewSX=0;ViewSY=0;
oViewX.onchange=function(){
	try{
		this.value=Number(this.value).toFixed(0);
	}catch(e){this.value=0}
	ViewSX=Number(this.value);
}
oViewY.onchange=function(){
	try{
		this.value=Number(this.value).toFixed(0);
	}catch(e){this.value=0}
	ViewSY=Number(this.value);
}
HBar=$('HBar');VBar=$('VBar');
HBar.onmousedown=function(){this.on=true;this.DragX=mouse_x;this.VX=ViewX}
VBar.onmousedown=function(){this.on=true;this.DragY=mouse_y;this.VY=ViewY}
//BGM
BGMid=$('BGMid');BGMid.value='22776365';
BGMid.onchange=function(){
	oBGM.src=`https://music.163.com/song/media/outer/url?id=${this.value}.mp3`;
	BGMplay.click();
}
DefBGM=document.getElementsByClassName('bgmn');
for(var i=DefBGM.length;i--;){
	DefBGM[i].onclick=function(){
		BGMid.value=this.innerText;
		BGMid.onchange();
	}
}
//Theme
SPR=$('spr');
(function(){
	var tHTML='';
	for(let i=0;i<32;i+=4){
		tHTML+=`<div class='vl'><div class='spr'></div><div class='spr'></div><div class='spr'></div><div class='spr'></div></div>`
	}
	SPR.innerHTML=tHTML;
})();
oSPR=SPR.getElementsByClassName('spr');
function setSPR(o,s,s2){
	o.innerHTML='';
	var n=s[0].cloneNode();
	if(s[0].tagName==='CANVAS'){
		let d=n.getContext('2d');d.drawImage(s[0],0,0);
	}
	if(s===sprJR || s===sprCherry){n.style.top=`${16-(n.height>>1)}px`}
	else if(s===sprTp){n.style.top=`${32-n.height}px`}
	o.appendChild(n);
	if(s2){
		var n2=s2[0].cloneNode();
		if(s2[0].tagName==='CANVAS'){
			let d=n2.getContext('2d');d.drawImage(s2[0],0,0);
		}
		if(s2===sprWallJump){n2.style.left='24px'};
		o.appendChild(n2);
	}
}
function resetSPR(){
setSPR(oSPR[0],sprSpike)
setSPR(oSPR[4],sprPlayerIdle);
setSPR(oSPR[8],sprSave);
setSPR(oSPR[1],sprFloor);
setSPR(oSPR[5],sprPlatform);
setSPR(oSPR[6],sprCherry);
setSPR(oSPR[2],sprBlock,sprWallJump);
setSPR(oSPR[3],sprBlock);
setSPR(oSPR[9],sprWater);
setSPR(oSPR[10],sprWarp);
//setSPR(oSPR[7],sprButton);
//setSPR(oSPR[11],sprSwitch);
setSPR(oSPR[12],sprBoard);
setSPR(oSPR[13],sprFloor,sprIce);
setSPR(oSPR[14],sprJR);
//setSPR(oSPR[15],sprTJump);
setSPR(oSPR[16],sprCrate);
setSPR(oSPR[17],sprFloor,sprConveyor);
//setSPR(oSPR[19],sprDJump);
setSPR(oSPR[20],sprTp);
//setSPR(oSPR[23],sprSJump);
setSPR(oSPR[24],sprSpring);
//setSPR(oSPR[27],sprIJump);
}
Theme=2;theme=$('theme');ObjT=theme.children;
function setTheme(t){
	for(let i=ObjT.length;i--;)removeH(ObjT[i]);
	ObjT[t].className+=' H';
	Theme=t;loadSkin(AllTheme[t]);
}
ObjT[2].onclick=function(){
	loadGra('linear-gradient(180deg,rgb(248,248,248) 0%,rgb(255,255,255) 100%)');
	setTheme(2);
}
ObjT[3].onclick=function(){
	loadGra('linear-gradient(180deg,rgb(47,47,47) 0%,rgb(106,106,106) 100%)');
	setTheme(3);
};
ObjT[4].onclick=function(){setTheme(4);setBKCol('#80FFFF')}
ObjT[5].onclick=function(){setTheme(5);setBKImg('Guy02')}
ObjT[6].onclick=function(){setTheme(6);setBKImg('K3S5',-1,-1)}
ObjT[7].onclick=function(){
	loadGra('linear-gradient(180deg,rgb(180,20,20) 0%,rgb(120,0,0) 100%)');
	setTheme(7);
};
ObjT[8].onclick=function(){setTheme(8);setBKImg('Darejar')}
ObjT[9].onclick=function(){setTheme(9);setBKImg('Adventure')}
ObjT[10].onclick=function(){setTheme(10);setBKImg('Hanamogeta')}
ObjT[11].onclick=function(){setTheme(11);setBKImg('Shadowlink57')}
ObjT[12].onclick=function(){setTheme(12);setBKImg('Hibikusuku')}
ObjT[13].onclick=function(){setTheme(13);setBKImg('Kamirin')}
ObjT[14].onclick=function(){setTheme(14);setBKImg('MELTDOWN')}
ObjT[15].onclick=function(){setTheme(15);setBKImg('NANGS8')}
ObjT[16].onclick=function(){setTheme(16);setBKImg('uhuhu3')}
ObjT[17].onclick=function(){setTheme(17);setBKImg('Midnight')}
ObjT[0].onclick=function(){
	ShowGUI.style.display='block';
	ShowTitle.innerText='选择主题色';
	BoxCt.innerHTML=`<div class='chsSpr'></div><div class='chsSpr'></div><div class='chsSpr'></div><div class='chsSpr'></div><div class='chsSpr'></div><div class='chsSpr'></div><div class='chsSpr'></div><div class='chsSpr'></div>`;
	var c=BoxCt.children;
	c[0].col='#000000';c[0].col2='#FFFFFF';c[0].col3='#CCCCCC';
	c[1].col='#FFFFFF';c[1].col2='#000000';c[1].col3='#666666';
	c[2].col='#DDDDDD';c[2].col2='#AAAAAA';c[2].col3='#CCCCCC';
	c[3].col='#888888';c[3].col2='#555555';c[3].col3='#707070';
	c[4].col='#F4D3E9';c[4].col2='#FFACD6';c[4].col3='#FF88C4';
	c[5].col='#DAFFFF';c[5].col2='#95E4FF';c[5].col3='#62D8FF';
	c[6].col='#FFFFC8';c[6].col2='#F5E869';c[6].col3='#F4DD0B';
	c[7].col='#CCFFDE';c[7].col2='#80FFB3';c[7].col3='#00FF93';
	for(var i=c.length;i--;){
		c[i].style.backgroundColor=c[i].col;
		c[i].style.backgroundImage=`linear-gradient(63.435deg,${c[i].col2} 50%,transparent 50%),linear-gradient(-63.435deg,${c[i].col2} 50%,transparent 50%)`;
		c[i].style.backgroundRepeat='no-repeat';
		c[i].style.backgroundPosition='22px 6px,6px 6px';
		c[i].style.backgroundSize='16px 32px';
		c[i].onclick=function(){
			setBKCol(this.col);
			for(let i=ObjT.length;i--;)removeH(ObjT[i]);
			ObjT[0].className+=' H';Theme=0;
			SprCol=this.col2;SprCol2=this.col3;
			setSprCol(SprCol,SprCol2,0);
		}
	}
}
ObjT[1].onclick=function(){
	ShowGUI.style.display='block';
	ShowTitle.innerText='选择主题色';
	BoxCt.innerHTML=`<div class='chsSpr'></div><div class='chsSpr'></div><div class='chsSpr'></div><div class='chsSpr'></div><div class='chsSpr'></div><div class='chsSpr'></div>`;
	var c=BoxCt.children;
	c[0].col='#000000';c[0].col2='#FFFFFF';
	c[1].col='#FFFFFF';c[1].col2='#000000';
	c[2].col='#FFFFFF';c[2].col2='#808080';
	c[3].col='#000000';c[3].col2='#88A0B0';
	c[4].col='#FFFFFF';c[4].col2='#E7C858';
	c[5].col='#1A001A';c[5].col2='#B622BA';
	for(var i=c.length;i--;){
		c[i].style.backgroundColor=c[i].col;
		c[i].style.backgroundImage=`linear-gradient(180deg,${c[i].col2} 1px,transparent 1px),linear-gradient(180deg,${c[i].col2} 1px,transparent 1px),linear-gradient(90deg,${c[i].col2} 1px,transparent 1px),linear-gradient(90deg,${c[i].col2} 1px,transparent 1px)`;
		c[i].style.backgroundRepeat='no-repeat';
		c[i].style.backgroundPosition='6px 6px,6px 37px,6px 6px,37px 6px';
		c[i].style.backgroundSize='32px 32px';
		c[i].onclick=function(){
			setBKCol(this.col);
			for(let i=ObjT.length;i--;)removeH(ObjT[i]);
			ObjT[1].className+=' H';Theme=1;
			SprCol=this.col2;SprCol2=this.col2;
			setSprCol(SprCol,SprCol2,1);
		}
	}
}
//Sprite
SetSpr=$('spr');
ObjSpr=SetSpr.getElementsByClassName('spr');
for(var i=ObjSpr.length;i--;){
	ObjSpr[i].onclick=function(){
		ShowGUI.style.display='block';
		ShowTitle.innerText='设置贴图';
		BoxCt.innerHTML='';
	}
}
function CloseShow(){ShowGUI.style.display='none'}
//Background
BackGround=$('back');sfbg=BackGround.getContext('2d');BGMode=1;setBK=$('setBK');
chsBox=setBK.getElementsByClassName('chsBox');
for(var i=0;i<chsBox.length;i++){
	chsBox[i].tab=[];chsBox[i].chs=[];
	var c=chsBox[i].parentElement.children;
	var a=0;
	for(var ii=0;ii<c.length;ii++){
		if(c[ii].className.includes('chsBox')){
			chsBox[i].chs.push(c[ii]);c[ii].ind=a;a+=1;
		}else if(c[ii].className.includes('chsTab')){
			chsBox[i].tab.push(c[ii]);
		}
	}
	chsBox[i].onclick=function(){
		this.chs.forEach(o=>removeH(o));
		this.className+=' H';
		for(var i=0;i<this.tab.length;i++){
			if(i===this.ind){
				this.tab[this.ind].style.display='block';
			}else{
				this.tab[i].style.display='none';
			}
		}
		if(this.id){
			switch(this.id){
			case 'C0':BGMode=0;setBKCol(bkCol.style.background);break;
			case 'C1':$('C1'+GraMode).click();break;
			case 'C10':BGMode=1;GraMode=0;setGra();break;
			case 'C11':BGMode=2;GraMode=1;setGra();break;
			case 'C20':
				let s0=BackGround.style.background.match(/url.*\)/);
				if(s0)BackGround.style.background=s0[0];
				BGhspd=Number(oBGhspd.value);
				BGvspd=Number(oBGvspd.value);
			break;
			case 'C21':
				let s1=BackGround.style.background.match(/url.*\)/);
				if(s1)BackGround.style.background=`${s1[0]} 0 0 /${RoomW}px ${RoomH}px`;
				BGhspd=0;BGvspd=0;
			break;
			case 'C22':
				let s2=BackGround.style.background.match(/url.*\)/);
				if(s2)BackGround.style.background=s2[0]+'center no-repeat';
				BGhspd=0;BGvspd=0;
			break;
			}
		}
	}
}
bkCol=$('bkCol');
bkCol.style.background='#FFFFFF';
function setBKCol(v){
	chsBox[0].click();$('C0').click();
	bkCol.style.background=v;
	BackGround.style.background=v;
}
bkCol.onclick=function(){
	ObjCol.onchange=function(){setBKCol(this.value)};
	ObjCol.value=covRGB(this.style.background);
	ObjCol.click();
}
bkedit=$('bkedit');GraMode=0;
function setGra(){
	var arr=Array.from(oSlider);
	arr.sort((a,b)=>parseFloat(a.style.left)-parseFloat(b.style.left))
	var str='';
	for(var i=0;i<arr.length;i++){
		str+=','+arr[i].style.backgroundColor+' '+arr[i].style.left;
	}
	bkedit.style.background='linear-gradient(90deg'+str+')';
	if(GraMode){
		BackGround.style.background='radial-gradient(ellipse'+str+')';
	}else{
		BackGround.style.background='linear-gradient('+slider.deg+'deg'+str+')';
	}
}
function loadGra(c){
	chsBox[0].click();$('C1').click();
	var deg=c.match(/\d+deg/);
	if(deg===null){
		if(BGMode===2)GraMode=1;
	}else{
		GraMode=0;slider.deg=parseInt(deg[0]);
		if(slider.deg===NaN)slider.deg=0;
		BGang.value=slider.deg;
	}
	var coll=c.match(/rgb.*?\)/g);
	var left=c.match(/\d+(\.\d+)?%/g);
	var str=c.match(/rgb.*\)/);
	slider.innerHTML='';
	for(var i=0;i<coll.length;i++){
		slider.innerHTML+="<div><div class='tri'></div></div>";
		oSlider=slider.children;
		oSlider[i].style.backgroundColor=coll[i];
		oSlider[i].style.left=left[i];
	}
	slider.set();
	bkedit.style.background='linear-gradient(90deg,'+str;
	BackGround.style.background=c;
}
slider=$('slider');oSlider=slider.children;
slider.dragi=-1;slider.deg=0;ObjCol=$('ObjCol');
slider.set=function(){
for(var i=0;i<oSlider.length;i++){
	oSlider[i].ind=i;
	oSlider[i].onmousedown=function(e){
		e=e||window.event;
		if(e.button===0){
			slider.pos=slider.getBoundingClientRect();
			slider.dragi=this.ind;
		}else if(e.button===2 && oSlider.length>2){
			slider.removeChild(this);
			slider.set();slider.dragi=-1;setGra();
		}
	}
	oSlider[i].ondblclick=function(){
		ObjCol.ind=this.ind;
		ObjCol.onchange=function(){
			oSlider[this.ind].style.backgroundColor=ObjCol.value;
			setGra();
		}
		ObjCol.value=covRGB(oSlider[this.ind].style.backgroundColor);
		ObjCol.click();
	}
}
}
slider.set();setGra();
slider.onmousedown=function(e){
	if(slider.dragi==-1 && e.button===0){
		slider.pos=slider.getBoundingClientRect();
		var l=Math.min(100,Math.max(0,(mouse_x-slider.pos.left-6)/1.72));
		slider.innerHTML+="<div style='left:"+l+"%;background-color:#FFF'><div class='tri'></div></div>"
		oSlider=slider.children;
		slider.set();slider.dragi=oSlider.length-1;setGra();
	}
}
BGang=$('BGang');
BGang.onchange=function(){
	slider.deg=Number(BGang.value);
	if(slider.deg===NaN)slider.deg=0;
	setGra();
}
oBGhspd=$('BGhspd');oBGvspd=$('BGvspd');
oBGhspd.onchange=function(){
	BGhspd=Number(this.value);
}
oBGvspd.onchange=function(){
	BGvspd=Number(this.value);
}
BGhspd=0;BGvspd=0;
oBGhspd.value=0;oBGvspd.value=0;
function setBKImg(t,hspd=0,vspd=0){
	var s=t.match(/url.*\)/);
	if(s){
		BackGround.style.background=s[0];
	}else{
		BackGround.style.background=`url('img/spr/${t}/BG.png')`;
	}
	var ls=t.match(/\/ \d+px/);
	if(ls){
		$('C21').click();
	}else{
		var jz=t.match(/center/);
		if(jz){
			$('C22').click();
		}else{
			oBGhspd.value=hspd;oBGvspd.value=vspd;
			$('C20').click();
		}
	}
	BGMode=3;chsBox[1].click();
}
function covRGB(s){
	var r=s.match(/\d+/g);
	if(r){
		var a=((Number(r[0])<<16)|(Number(r[1])<<8)|Number(r[2])).toString(16);
		return '#'+a.padStart(6,'00000');
	}
	return s;
}
//Effects
efcol=$('efcol');
efcol.style.background='#FFFFFF';
efcol.onclick=function(){
	ObjCol.onchange=function(){efcol.style.background=this.value};
	ObjCol.value=covRGB(this.style.background);
	ObjCol.click();
}
effects=$('effects').getElementsByTagName('input');
haveEf=$('haveEf');
for(var i=effects.length;i--;){
	effects[i].ind=i;
	effects[i].onchange=function(){
	if(Number(this.value)===NaN){
		this.value='';
	}
	if(this.ind===0 && this.value===''){
		haveEf.style.color='';
		haveEf.style.pointerEvents='';
		return true;
	}
	if(this.min)this.value=Math.max(this.min,this.value);
	if(this.max)this.value=Math.min(this.max,this.value);
	if(this.step==1){
		this.value=Number(this.value).toFixed(0);
	}else{
		this.value=Number(this.value).toFixed(1);
	}
	if(this.ind===0){
		haveEf.style.color='#000';
		haveEf.style.pointerEvents='auto';
	}
	}
}
//SaveRoom
function NtS(n){return String.fromCharCode(n+100)}
function cStr(num,n){
	if(n)num*=Math.pow(10,n);
	if(num<0){
		return NtS(1)+NtS(Math.abs(num)+100);
	}else{return NtS(num+100)}
}
function setStr(array,s,o,p){
	var ii=array.findIndex(a=>a.indexOf(s)==1);
	if(ii<0){
		if(p){array.push(s+cStr(o.x)+cStr(o.y))}
		else{array.push(NtS(0)+s+cStr(o.x)+cStr(o.y))}
	}else{array[ii]+=cStr(o.x)+cStr(o.y)}
}
function concatArr(a1,a2,id){
	if(a2.length>0){
		a1.push(NtS(id));a2[0]=a2[0].slice(1);
		a1=a1.concat(a2);a2.splice(0);
	}
	return a1;
}
function saveStr(name,arr,s){
	var str='';arr.forEach(o=>str+=o);
	arr.splice(0);s[name]=str;
}
function strWH(o){
	return cStr(o.w+(o.h>>5))
}
GameName=$('GameName');
function checkSize(){
	var n=0;block.forEach(o=>n+=1);
	if(n>400){alert('Block数量过多，请修改');return false}
	n=0;spike.forEach(o=>n+=1);
	Rcherry.forEach(o=>{if(o.__proto__.constructor.name=='objSpikeGear')n+=o.n})
	if(n>800){alert('Spike数量过多，请修改');return false}
	n=0;platform.forEach(o=>n+=1);
	if(n>200){alert('Platform数量过多，请修改');return false}
	n=0;cherry.forEach(o=>n+=1);
	Rcherry.forEach(o=>{if(o.__proto__.constructor.name=='objRoundingCherry')n+=o.n})
	if(n>500){alert('Cherry数量过多，请修改');return false}
	n=0;conveyor.forEach(o=>n+=1);
	if(n>100){alert('Conveyor数量过多，请修改');return false}
	n=0;warp.forEach(o=>n+=1);
	if(n>100){alert('Warp数量过多，请修改');return false}
	return true
}
function saveRoom(mes){
	saveData={};saveData['V']='0.500';
	saveData['N']=GameName.value;
	saveData['R']=cStr(RoomW)+cStr(RoomH)+cStr(startX)+cStr(startY)+cStr(warpEnd.x)+cStr(warpEnd.y)+cStr(OutMode)+cStr(ViewMode)+cStr(ViewSX)+cStr(ViewSY);
	saveData['T']=NtS(Theme);
	if(Theme===0){
		saveData['T']+=SprCol+SprCol2;
	}else if(Theme===1){
		saveData['T']+=SprCol;
	}
	switch(BGMode){
		case 0:saveData['BG']=NtS(BGMode)+BackGround.style.background;break;
		case 1:case 2:saveData['BG']=NtS(BGMode)+BackGround.style.backgroundImage;break;
		case 3:
			saveData['BG']=NtS(BGMode)+BackGround.style.background;
			if(BGhspd!==0 || BGvspd!==0)saveData['BG']+=` h${BGhspd}v${BGvspd}`;
		break;
	}
	saveData['BGM']=BGMid.value;
	saveData['E']=effects[0].value;
	if(saveData['E']!==''){
		for(let i=1;i<7;i++){
			saveData['E']+=NtS(Number(effects[i].value)/Number(effects[i].step||0.1));
		}
		saveData['E']+=effects[7].value+covRGB(efcol.style.background);
	}
	//Block
	var arr=[];var arr2=[];var arr3=[];
	block.forEach(o=>{
		switch(o.__proto__.constructor.name){
			case 'objBlock':setStr(arr,strWH(o),o);break;
			case 'objIceBlock':setStr(arr2,strWH(o),o);break;
		}
	});
	arr=concatArr(arr,arr2,11);
	water.forEach(o=>setStr(arr2,strWH(o),o));
	arr=concatArr(arr,arr2,10);
	field.forEach(o=>{
		switch(o.f){
			case 0:setStr(arr2,strWH(o),o);break;
			case 1:setStr(arr3,strWH(o),o);break;
		}
	});
	arr=concatArr(arr,arr2,12);
	arr=concatArr(arr,arr3,13);
	field.forEach(o=>{
		switch(o.f){
			case 2:setStr(arr2,strWH(o),o);break;
			case 3:setStr(arr3,strWH(o),o);break;
		}
	});
	arr=concatArr(arr,arr2,14);
	arr=concatArr(arr,arr3,15);
	//trigger.forEach(o=>setStr(arr2,cStr(o.w)+cStr(o.h),o,false,true));
	//arr=concatArr(arr,arr2,16);
	saveStr('B',arr,saveData);
	//Spike
	spike.forEach(o=>{
		if(o.__proto__.constructor.name==='objSpike'){
			setStr(arr,cStr(o.w)+cStr(o.h)+cStr(o.img_ang),o);
		}
	});
	saveStr('S',arr,saveData);
	//Obj
	GMStepObj.forEach(o=>{
		switch(o.__proto__.constructor.name){
			case 'objSavePoint':setStr(arr,NtS(10),o,true);break;
		}
	});
	Walljump.forEach(o=>setStr(arr,NtS(12+o.side),o,true));
	block.forEach(o=>{
		switch(o.__proto__.constructor.name){
			case 'objCrate':setStr(arr,NtS(12),o,true);break;
			case 'objBreakableBlock':setStr(arr,NtS(22),o,true);break;
		}
	});
	GMColObj.forEach(o=>{
		switch(o.__proto__.constructor.name){
			case 'objJumpRefresher':setStr(arr,NtS(14),o,true);break;
			case 'objJumpChanger':setStr(arr,NtS(15+Math.min(o.num,3)),o,true);break;
			case 'objFake':setStr(arr,NtS(19+o.ind),o,true);break;
		}
	});
	saveStr('O',arr,saveData);
	//Obj2
	platform.forEach(o=>setStr(arr,cStr(o.hspeed,1)+cStr(o.vspeed,1),o));
	cherry.forEach(o=>{
		if(o.rid===undefined)setStr(arr2,cStr(o.hspeed,1)+cStr(o.vspeed,1),o);
	});
	arr=concatArr(arr,arr2,10);
	GMColObj.forEach(o=>{
		switch(o.__proto__.constructor.name){
			case 'objSpring':setStr(arr2,cStr(o.ang)+cStr(o.spd),o);break;
			case 'objTrampoline':setStr(arr3,cStr(o.ang)+cStr(o.spd),o);break;
		}
	});
	arr=concatArr(arr,arr2,11);
	arr=concatArr(arr,arr3,12);
	conveyor.forEach(o=>{
		if(o.fx===0){setStr(arr2,cStr(o._w)+cStr(o.spd,1),o)}
		else{setStr(arr3,cStr(o._w)+cStr(o.spd,1),o)}
	});
	arr=concatArr(arr,arr2,13);
	arr=concatArr(arr,arr3,14);
	saveStr('O2',arr,saveData);
	//Obj3
	warp.forEach(o=>setStr(arr,cStr(o.warpX)+cStr(o.warpY)+cStr(o.k),o));
	saveStr('O3',arr,saveData);
	//Obj4
	Rcherry.forEach(o=>{
		setStr(arr,NtS(10+(o.__proto__.constructor.name==='objSpikeGear'))+cStr(o.n)+cStr(o.r)+cStr(o.spd,1)+cStr(o.ang),o,true);
	});
	shooter.forEach(o=>{
		setStr(arr,NtS(12+o.mode)+cStr(o.n)+cStr(o.spd,1)+cStr(o.t)+cStr(o.ang),o,true);
	})
	saveStr('O4',arr,saveData);
	//Board
	board.forEach(o=>{
		arr.push(NtS(-100)+cStr(o.x)+cStr(o.y)+o.txt)
	});
	saveStr('Bd',arr,saveData);
	if(mes){
		localStorage.setItem(NowGame,JSON.stringify(saveData));
		alert("保存成功！");
	}
}
function cNum(s,n){
	if(VER<0.4){
		if(n)return (s.charCodeAt()-100)/Math.pow(10,n);
		return s.charCodeAt()-100;
	}else{
		if(n)return (s.charCodeAt()-200)/Math.pow(10,n);
		return s.charCodeAt()-200;
	}
}
//loadRoom
function StN(s){
	if(VER<0.4){return s.charCodeAt()}
	else{return s.charCodeAt()-100}
}
function loadRoom(reload){
	VER=Number(saveData['V']);
	if(reload){
	GameName.value=saveData['N'] || '';
	var th=saveData['T'];
	if(th){
		Theme=StN(th.charAt(0));
		for(let i=ObjT.length;i--;){removeH(ObjT[i])};
		ObjT[Theme].className+=' H';
		if(Theme===0){
			SprCol=th.slice(1,8);SprCol2=th.slice(8,15);
			setSprCol(SprCol,SprCol2,0);
		}else if(Theme===1){
			SprCol=th.slice(1,8);SprCol2=SprCol;
			setSprCol(SprCol,SprCol2,1);
		}else{
			ObjT[Theme].click();
		}
	}
	resetSPR();BGMid.value=saveData['BGM']||'29482950';BGMid.onchange();
	}
	var str=saveData['R'];
	if(str.length<10){
		str=cStr(800)+cStr(608)+str;
		while(str.length<10)str+=cStr(0);
	}
	RoomW=cNum(str.charAt(0));RoomH=cNum(str.charAt(1));
	oRoomW.value=RoomW;oRoomH.value=RoomH;
	startX=cNum(str.charAt(2));
	startY=cNum(str.charAt(3));
	warpEnd=new objWarp(cNum(str.charAt(4)),cNum(str.charAt(5)),true);
	RoomOut[cNum(str.charAt(6))].click();
	oView[cNum(str.charAt(7))].click();
	ViewSX=cNum(str.charAt(8));ViewSY=cNum(str.charAt(9));
	oViewX.value=ViewSX;oViewY.value=ViewSY;
	var col=saveData['BG'];
	if(col){
		BGMode=StN(col.charAt(0));
		col=col.slice(1);
		for(let i=0;i<chsBox.length;i++){removeH(chsBox[i])};
		switch(BGMode){
			case 0:setBKCol(col);break;
			case 1:case 2:loadGra(col);break;
			case 3:
				let m=col.match(/h-*\d+\.*\d*v-*\d+\.*\d*/);
				var h=0;var v=0;
				if(m){
					h=m[0].match(/h-*\d+\.*\d*/)[0].slice(1);
					v=m[0].match(/v-*\d+\.*\d*/)[0].slice(1);
				}
				setBKImg(col,h,v);
			break;
		}
	};
	if(saveData['E'] && saveData['E']!==''){
		effects[0].value=saveData['E'].charAt(0);effects[0].onchange();
		effects[7].value=saveData['E'].charAt(7);effects[7].onchange();
		EF[0]=Number(effects[0].value);EF[7]=Number(effects[7].value);
		for(let i=1;i<7;i++){
			EF[i]=StN(saveData['E'].charAt(i))*(effects[i].step||0.1);
			effects[i].value=EF[i];effects[i].onchange();
		}
		EF[8]=saveData['E'].slice(8);
		efcol.style.background=EF[8];
		if(EF[0]>3)EFSprinit();
	}else{
		EF=[];effects[0].value='';effects[0].onchange();
	}
	readRoom(saveData);
}
//Download
function download(o,f){
	var a=document.createElement('a');
	a.download=f;a.style.display='none';
	var blob=new Blob([JSON.stringify(o)]);
	a.href=URL.createObjectURL(blob);
	document.body.appendChild(a);
	a.click();document.body.removeChild(a);
}
function DLGame(){
	if(saveData!==undefined){
		saveRoom();var n=saveData.N;
		if(n==='')n='Untitled'
		download(saveData,n+'.iw');
	}
}
var ipGame=document.createElement('input');
ipGame.type='file';ipGame.accept='.iw';
ipGame.style.display='none';
ipGame.onchange=function(){
	var m=this.files[0];
	var r=new FileReader();
	r.readAsText(m);
	r.onload=function(){
		try{
			var s=JSON.parse(this.result);
			saveData=s;room_load(true,false,true);
		}catch(e){
			console.log(e);alert('文件读取错误！');
		}
	}
}
document.body.appendChild(ipGame);
function LDGame(){ipGame.value='';ipGame.click()}
ipGC=document.createElement('input');
ipGC.type='file';ipGC.accept='image/*';
ipGC.style.display='none';
cGC=$('cGC');cGCc=cGC.getContext('2d');
dUp=$('dUp');dUpc=dUp.getContext('2d');
Cchs=$('Cchs');
Cchs.onmousemove=function(){
	if(this.Dw===undefined && this.Dx===undefined){
		var p=this.getBoundingClientRect();
		if(mouse_x>p.right-20 && mouse_y>p.bottom-20){
			document.body.style.cursor='se-resize';
		}else{
			document.body.style.cursor='move';
		}
	}
}
Cchs.onmouseout=function(){
	if(this.Dw===undefined && this.Dx===undefined){
		document.body.style.cursor='default';
	}
}
Cchs.onmousedown=function(){
	var p=this.getBoundingClientRect();
	if(mouse_x>p.right-20 && mouse_y>p.bottom-20){
		this.Dw=mouse_x-cGC.sc;this.Dh=mouse_y-cGC.sc;
		document.body.style.cursor='se-resize';
	}else{
		this.Dx=mouse_x-Number(this.style.left.slice(0,-2));
		this.Dy=mouse_y-Number(this.style.top.slice(0,-2));
		document.body.style.cursor='move';
	}
}
ipGC.onchange=function(){
	var m=this.files[0];
	var r=new FileReader();
	r.readAsDataURL(m);
	r.onload=function(){
		try{
		var o=new Image();
		o.style.imageRendering='auto';
		o.onload=function(){
			if(this.width<96 || this.height<96){
				alert('图片宽高不能小于96像素！');return false;
			}
			while(this.width>1024 || this.height>1024){
				this.width=this.width>>1;
				this.height=this.height>>1;
			}
			dUp.width=this.width;
			dUp.height=this.height;
			dUpc.drawImage(this,0,0,dUp.width,dUp.height);
			cGC.sc=Math.min(dUp.width,dUp.height);
			Cchs.style.width=`${cGC.sc}px`;
			Cchs.style.height=`${cGC.sc}px`;
			var l=(dUp.width-cGC.sc)/2;
			var t=(dUp.height-cGC.sc)/2;
			Cchs.style.left=`${l}px`;
			Cchs.style.top=`${t}px`;
			cGCc.clearRect(0,0,96,96);
			cGCc.drawImage(dUp,l,t,cGC.sc,cGC.sc,0,0,96,96);
			var b=$('CUp').getElementsByTagName('div');
			b[0].onclick=function(){
				var url=cGC.toDataURL('image/webp',1);
				if(url.length>9000){
					$('CImg').src=cGC.toDataURL('image/webp',0.92);
				}else{$('CImg').src=url}
				$('CBK').style.display='none';
			}
			b[1].onclick=function(){$('CBK').style.display='none'}
			$('CBK').style.display='block';
		}
		o.src=this.result;
		}catch(e){
			console.log(e);alert('文件读取错误！');
		}
	}
}
document.body.appendChild(ipGC);
function ULGame(){
if(!currentUser.get("emailVerified")){
	alert('您的账号尚未验证，无法发布游戏');return false
}
if(!currentUser.get('Exp')||currentUser.get('Exp')<20){
	alert('用户等级达到3级才能发布游戏哦！');return false
}
saveRoom();
if(saveData.N===''){ico[0].click();alert('请填写游戏标题！');return false}
alert('注意事项：\n1.请勿发布单刺阵图\n2.游戏质量不能太低（不合格会被删除）\n3.合理设置挑战成就，确保游戏能够通关\n4.警告：房间内物体过多会卡顿！！（必要时会采取删除措施）')
ShowGUI.style.display='block';
ShowTitle.innerText='发布游戏';
BoxCt.innerHTML=`<h2>游戏名<span style='color:#000;font-weight:bold;margin-left:8px'>${saveData.N}</span></h2>
<div style='position:absolute;left:112px'>
	<h2>游戏简介</h2>
	<textarea id='GIn'></textarea>
</div>
<h2>游戏封面</h2>
<div id='GCover'><img id='CImg' alt=''/>➕</div>
<h2 style='display:inline-block'>游戏分类</h2>
<div class='chsBox'>Needle</div><div class='chsBox'>Trap</div><div class='chsBox'>Gimmick</div><br/>
<h2 style='display:inline-block'>挑战成就</h2>
<span>时间:</span><input type='number' id='at'/><span style='margin-right:10px'>s</span><span>死亡:</span><input type='number' id='ad'/>
<div id='UP'>发布</div>`;
$('at').onchange=function(){
	try{
		var num=Math.abs(Math.round(Number(this.value)));
		if(num!==NaN)this.value=num;
	}catch(e){this.value=''}
}
$('ad').onchange=$('at').onchange;
var GI=$('GIn');GI.maxLength=200;
var GCover=$('GCover');
GCover.onclick=function(){ipGC.value='';ipGC.click()}
var GameUP=$('UP');
GameUP.onclick=function(){
	if(CImg.src===''){alert('请设置游戏封面！');return false}
	if(GameC===null){alert('请选择游戏分类！');return false}
	if(GI.value===''){alert('请填写游戏简介！');return false}
	UPGAME()
}
var GameC=null;
var gameC=BoxCt.getElementsByClassName('chsBox');
for(let i=gameC.length;i--;){
	gameC[i].ind=i;
	gameC[i].onclick=function(){
		for(let i=gameC.length;i--;){removeH(gameC[i])}
		this.className+=' H';GameC=this.ind;
	}
}
function UPGAME(){
if(!checkSize())return false;
var nG=new AV.Object('Game');
var GR=new AV.Object('GRoom');
var r=saveData;
GR.set('V',r.V);GR.set('R',r.R);
GR.set('T',r.T);GR.set('BG',r.BG);
GR.set('B',r.B);GR.set('S',r.S);
GR.set('O',r.O);GR.set('O2',r.O2);
GR.set('O3',r.O3);GR.set('O4',r.O4);
GR.set('Bd',r.Bd);GR.set('BGM',r.BGM);
GR.set('E',r.E);
if($('at').value!=='')GR.set('CT',Number($('at').value));
if($('ad').value!=='')GR.set('CD',Number($('ad').value));
nG.set('Name',r.N);nG.set('A',AV.User.current());
nG.set('I',GI.value);nG.set('C',GameC);
nG.set('Room',GR);nG.set('Cover',CImg.src);nG.set('Index',Date.now())
nG.save().then(function(){alert(`✅ 发布成功！`);CloseShow();},function(e){alert('发布失败');});
}
}