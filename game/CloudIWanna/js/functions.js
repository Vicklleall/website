function cov(n){
	if(n<10)return '0'+n;
	return n;
}
const PI=Math.PI,roundFix=n=>{
	//let a=Number(n.toFixed(1))
	//return a%2===0.5?a<<0:Math.round(a)
	return Math.round(n.toFixed(1))
}
vk_shift=16;vk_ctrl=17;LRshift=1;
vk_left=37;vk_right=39;
vk_up=38;vk_down=40;
vk_Z=90;vk_S=83;vk_P=80;
keyDown=[];keyUp=[];key=[];
function initKey(k){
	keyDown[k]=false;keyUp[k]=false;key[k]=false;
}
initKey(vk_ctrl);initKey(vk_shift);
initKey(vk_right);initKey(vk_left);
initKey(vk_up);initKey(vk_down);
initKey(vk_Z);initKey(vk_S);
document.onkeydown=function(e){
	if(e.keyCode===9)e.preventDefault()
	if(document.activeElement.type==='textarea' || document.activeElement.type==='input')return true;
	var e=e||window.event;
	if(e){
		if(!e.code)return true;
		if(e.keyCode===vk_shift && e.location!==LRshift){
			if(e.code!=='ShiftLeft'&&e.code!=='ShiftRight')return true;
			LRshift=e.location;keyDown[e.keyCode]=true;
		}
		if(!key[e.keyCode])keyDown[e.keyCode]=true;
		if(REC&&!e.repeat&&e.code.startsWith("Numpad")){
			RECO[e.code.slice(-1)]+=NtS(RECT);
		}
	}
}
document.onkeyup=function(e){
	var e=e||window.event;
	if(e){
		if(!e.code)return true;
		e.preventDefault();keyUp[e.keyCode]=true;
	}
}
function sign(x){
	return (x>0)-(x<0);
}
function draw_obj(obj,arg,a=false){
	obj.forEach(o=>{
		if(edit||a||o.inView)o.draw_self(arg);
	});
}
function move(obj){
	obj.forEach(o=>{
		if(o.move&&o.inView)o.move();
	});
}
function activate(){
	act_obj(field);act_obj(platform)
	act_obj(block);act_obj(Rcherry);act_obj(shooter)
	act_obj(spike);act_obj(conveyor)
	act_obj(GMStepObj);act_obj(GMColObj)
	act_obj(cherry);act_obj(bullet)
	act_obj(warp);act_obj(water)
	tiles.style.transform=`translate(-${ViewX}px,-${ViewY}px)`
}
function act_obj(obj){
	obj.forEach(o=>{
		if(o.drawW){
			var l=o.cX-o.drawW;var t=o.cY-o.drawH;
			var ww=o.drawW*2;var hh=o.drawH*2;
		}else if(o.r){
			var l=o.x-o.r;var t=o.y-o.r;
			var ww=o.r*2;var hh=ww;
		}else{
			var l=o.x;var t=o.y;var ww=o.w;var hh=o.h;
		}
		if(o.Ox)l-=o.Ox;if(o.Oy)t-=o.Oy;
		o.inView=colRect(ViewX,ViewY,800,608,l,t,ww,hh);
	})
}
function key_check(k){
	if(REP && RECO[KD[k]]!==undefined){
		var D=RECO[KD[k]].charCodeAt(KDR[k])-100;
		if(D===RECT){keyDown[k]=true;KDR[k]+=1}else{keyDown[k]=false};
		var U=RECO[KU[k]].charCodeAt(KUR[k])-100;
		if(U===RECT){keyUp[k]=true;KUR[k]+=1}else{keyUp[k]=false};
	}
	if(keyDown[k]){
		if(!key[k])key[k]=true;
	}else if(keyUp[k]){
		if(key[k])key[k]=false;
	}
	if(REC && KD[k]!==undefined){
		if(keyDown[k])RECO[KD[k]]+=NtS(RECT);
		if(keyUp[k])RECO[KU[k]]+=NtS(RECT);
	}
}
function key_clear(k){
	if(keyUp[k])if(key[k])key[k]=false;
	keyDown[k]=false;keyUp[k]=false;
}
function key_pause(k){
	if(key[k]){
		key[k]=false;keyUp[k]=true;
	}
	keyDown[k]=false;
}
function keyboard_check(k){
	return key[k];
}
function keyboard_check_pressed(k){
	return keyDown[k];
}
function keyboard_check_released(k){
	return keyUp[k];
}
function draw_sprite_angle(s,spr,x,y,ang,ox,oy,scX=1,scY=1){
	var a=ang*PI/180;s.save();s.translate(x,y);s.rotate(-a);
	if(scX!==1 || scY!==1){
		s.scale(scX,scY);ox/=scX;oy/=scY;
		if(Math.abs(scX)<1 || Math.abs(scY)<1){
			s.imageSmoothingEnabled=true;
		};
	}
	s.drawImage(spr,-ox,-oy);
	s.restore();s.imageSmoothingEnabled=false;
}
function colRect(x1,y1,w1,h1,x2,y2,w2,h2){
	return !(x1+w1<=x2||x1>=x2+w2||y1+h1<=y2||y1>=y2+h2)
	//return !(roundFix(x1+w1)<=roundFix(x2)||roundFix(x1)>=roundFix(x2+w2)||roundFix(y1+h1)<=roundFix(y2)||roundFix(y1)>=roundFix(y2+h2))
}
function colRP(x,y,w,h,obj,l,t,ww,hh){
	colsf.clearRect(0,0,colSF.width,colSF.height);
	colsf.save();
	colsf.translate(16-ViewX,16-ViewY);
	obj.draw_self(colsf);
	colsf.restore()
	if(l!==undefined){
		let r=roundFix(Math.min(x+w,l+ww)),b=roundFix(Math.min(y+h,t+hh))
		x=roundFix(x<l?l:x);y=roundFix(y<t?t:y);if(r-x<=0||b-y<=0)return false
		var img=colsf.getImageData(x+16-ViewX,y+16-ViewY,r-x,b-y).data;
	}else{
		var img=colsf.getImageData(x+16-ViewX,y+16-ViewY,w,h).data;
	}
	for(var i=3;i<img.length;i+=4)if(img[i]>0)return true
	return false
}
function colRO(x,y,w,h,obj,a,id){
	if(a)var list=[];
	for(var i=0;i<obj.length;i++){
		if(obj[i].id===id)continue;
		if(obj[i].drawW){
			var l=obj[i].cX-obj[i].drawW;
			var t=obj[i].cY-obj[i].drawH;
			var ww=obj[i].drawW*2;
			var hh=obj[i].drawH*2;
		}else{
			var l=obj[i].x;var t=obj[i].y;
			var ww=obj[i].w;var hh=obj[i].h;
		}
		if(obj[i].Ox)l-=obj[i].Ox;
		if(obj[i].Oy)t-=obj[i].Oy;
		if(colRect(x,y,w,h,l,t,ww,hh)){
			if(obj[i].pxmask){
				if(colRP(x,y,w,h,obj[i],l,t,ww,hh)){
					if(a){list.push(obj[i])}else{return obj[i]};
				}
			}else{
				if(a){list.push(obj[i])}else{return obj[i]};
			}
		}
	}
	if(a){
		if(list.length>0){return list};
	}
	return false;
}
function pos_obj(x,y,o){
	for(var i=o.length;i--;){
		if(o[i].drawW){
			var l=o[i].cX-o[i].drawW;
			var t=o[i].cY-o[i].drawH;
			var ww=o[i].drawW*2;
			var hh=o[i].drawH*2;
		}else{
			var l=o[i].x-(o[i].Ox||0);
			var t=o[i].y-(o[i].Oy||0);
			var ww=o[i].w;var hh=o[i].h;
		}
		if(l<=x && t<=y && l+ww>x && t+hh>y){
			return o[i];
		}
	}
	return false;
}
function instance_place(x,y,o){
	var list=[];
	for(var i=o.length-1;i>-1;i--){
		if(o[i].x===x && o[i].y===y)list.push(o[i]);
	}
	if(list.length>0)return list;
	return false;
}
oTime=null;
function killPlayer(){
	sndPlay(sndDeath);blood=[];
	bloodX=player.x;bloodY=player.y;
	for(let i=0;i<10;i++){
		blood.push(new objBlood(bloodX,bloodY));
	}
	if(!END && !REP){
		if(oTime){
			oTime.addDeaths();Death.innerText=oTime.getDeaths();
		}else{
			Death.death+=1;Death.innerText=Death.death;
		}
		GO.style.visibility='visible';
		GC.style.visibility='hidden';
		GUI.style.transition="opacity 1s 0.4s";
		GUI.style.visibility='visible';
		GUI.style.opacity='1';
	}
	player=null;
}
function destroy(obj,id){
	if(nowObj===obj[id]){nowObj=null;resetPro()};
	if(obj.evDes)obj[id].evDes();
	obj.splice(id,1);
	for(var i=0;i<obj.length;i++){
		if(obj[i].id>id)obj[i].id-=1;
	}
}
function DtoR(a){
	return a/180*PI;
}
icoarea=$('icoarea');nowTab=0;
ico=icoarea.getElementsByClassName('ico');
tab=document.getElementsByClassName('tab');
function setTab(t){
	nowTab=t.ind
	for(var i=0;i<tab.length;i++){
		tab[i].style.left=(i-t.ind)*500+'px';
	}
}
for(var i=0;i<ico.length;i++){
	ico[i].ind=i;
	ico[i].onclick=function(){
		canPut=false;setTab(this);
		for(var i=0;i<ico.length;i++){
			ico[i].style.backgroundColor='#E6E6E6';
		}
		this.style.backgroundColor='#CCC';
	}
}
Time=$('Time');
Death=$('Deaths');
Time.time=0;Death.death=0;
function save_ef(){
	sfscr.clearRect(0,0,800,608)
	sfscr.drawImage(tiles,-ViewX,-ViewY)
	sfscr.drawImage(scrn,0,0)
	for(var xx=0;xx<800;xx+=200){
		for(var yy=0;yy<608;yy+=152){
			saveEf.push(new objSaveEf(xx,yy))
		}
	}
}
function keyPause(){
	key_pause(vk_right);key_pause(vk_left);
	key_pause(vk_shift);key_pause(vk_Z);key_pause(vk_S);
}
function breakEF(o){
	sndPlay(sndBreak);
	let hspd=Math.random()*4-2;
	let vspd=Math.random()-2;
	let i=objEf.push(new objParticle(o.x+16,o.y+16,o.spr,0,0,0.05))
	let a=objEf[i-1];a.grav=0.2;a.hspd=hspd;a.vspd=vspd;
}