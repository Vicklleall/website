function FuncLogIn(){currentUser.fetch().then(function(){
	if(!sessionStorage.getItem('email')){
		if(!currentUser.getEmail()){
			$('GUI').style.display='block';$('GUIA').style.display='block';$('Email').style.display='block'
			alert('在完成账号验证之前，您的账号将受到以下限制：\n-无法发布游戏\n-无法对游戏评分\n-通关记录不会上传至排行榜\n请尽快绑定邮箱以完成账号验证')
		}else if(!currentUser.get("emailVerified")){
			if(confirm('您的账号尚未验证，是否现在进行邮箱验证？')){
				AV.User.requestEmailVerify(currentUser.getEmail())
				alert('已发送验证邮件到您的邮箱，请尽快完成邮箱验证')
			}
		}
		sessionStorage.setItem('email',1)
	}
	var avatar=currentUser.get('Avatar')
	if(avatar){
		UserPro.firstChild.style.display='none'
		UserPro.style.backgroundImage=`url(${avatar})`
	}
	$('username').innerText=currentUser.getUsername()
	var Exp=currentUser.get('Exp')||0
	var l=getLv(Exp),p=Exp-l.p,n=l.n-l.p,per=p/n*100
	$('Exp').innerText=`${p}/${n}`;$('LvS').innerText=`Lv ${l.l}`
	$('Bar').style.background=`linear-gradient(90deg,hsl(205,100%,75%) ${per}%,transparent ${per}%)`
	var message=currentUser.get('M')
	if(message&&message.length>0){
		var str='';message.forEach(o=>{str+=o+'\n'})
		alert(str);currentUser.set('M',[]);currentUser.save()
	}
	var q=new AV.Query('GRank')
	q.select('G');q.equalTo('P',currentUser);q.limit(960)
	q.find().then(r=>{r.forEach(o=>played.push(o.get('G').id));if(GAME.length)setPlayed()})
})}
if(currentUser){
	currentUser.isAuthenticated().then(function(a){
		if(a){FuncLogIn()}else{window.location.href='./Login'}
	})
}
UserPro=$('player')
UserPro.onclick=function(){
	var o=$('userp')
	if(o.style.opacity==0){
		o.style.opacity=1;o.style.pointerEvents='auto'
	}else{
		o.style.opacity=0;o.style.pointerEvents='none'
	}
}
userb=$('userp').children
userb[0].onclick=function(){
	if(currentUser)AV.User.logOut();window.location.href='./Login'
}
userb[1].onclick=function(){ipGC.click()}
userb[2].onclick=function(){
	$('GUI').style.display='block';$('GUIA').style.display='block';$('CUN').style.display='block'
}
userb[3].onclick=function(){
	var e=currentUser.getEmail();
	if(e){
		AV.User.requestPasswordReset(e).then(function(){alert('请求已发送，请检查您的邮箱！')},
		function(e){console.log(e);alert('请求发送失败！')})
	}else{
		alert('请先绑定邮箱！');
		$('GUI').style.display='block';$('GUIA').style.display='block';$('Email').style.display='block'
	}
}
op=document.getElementsByClassName('op')
op[0].onclick=function(){
	this.style.backgroundColor='hsl(206,100%,85%)'
	op[1].style.backgroundColor=''
	var o=$('ga');o.style.opacity=1;o.style.pointerEvents='auto'
	o=$('mk');o.style.opacity=0;o.style.pointerEvents='none'
}
op[1].onclick=function(){
	this.style.backgroundColor='hsl(206,100%,85%)'
	op[0].style.backgroundColor=''
	var o=$('ga');o.style.opacity=0;o.style.pointerEvents='none'
	o=$('mk');o.style.opacity=1;o.style.pointerEvents='auto'
}
const Lv=[5,10,20,30,40,50,64,80,100,125,150,175,200,225,250,275,300,330,360,400,440,480,520,560,600,640,680,720,760,800,850,900,950,1000,1060,1120,1200,1280,1360,1440,1500]
function getLv(e){
	for(i=0;i<Lv.length;i++){
		if(e<Lv[i]){return {l:i,p:Lv[i-1]||0,n:Lv[i]}}
	}
}
function setPlayed(){
	played.forEach(i=>{var e=$('c'+i);if(e){e.parentNode.parentNode.played=true;e.style.display='block'}});Filter()
}
GAME=[];Author=[];MyGame=[];lastDate=0;played=[]
function fetchGame(){
if(lastDate===1)return;
var q1=new AV.Query('Game'),q2=new AV.Query('Game')
if(lastDate){
	q1.lessThan('Index',lastDate);lastDate=1
	q2.notEqualTo('A',currentUser)
	var query=AV.Query.and(q1,q2)
}else{
	lastDate=Date.now()-6e8
	q1.greaterThan('Index',lastDate)
	q2.equalTo('A',currentUser)
	var query=AV.Query.or(q1,q2)
}
query.descending('createdAt');query.include('A');query.limit(960)
query.select(['Name','C','RP','R','DP','D','P','I','Cover','A.username','Room','hide','Index']);
query.find().then(function(r){
	if(r.length>0){
		r.forEach(o=>{
			var g={};g.id=o.id;g.Hide=o.get('hide')
			g.N=o.get('Name');g.C=o.get('C')
			g.A=o.get('A').get('username')
			var RP=o.get('RP')
			if(RP===0){g.R=0;g.Ra='---'}
			else{
				g.R=o.get('R')/RP;g.Ra=g.R.toFixed(1)
				var ind=Author.findIndex(a=>a.A===g.A)
				if(ind<0){
					Author.push({A:g.A,R:g.R,N:1,T:o.get('A').createdAt})
				}else{Author[ind].R+=g.R;Author[ind].N+=1}
			}
			var DP=o.get('DP')
			if(DP===0){g.D='??'}else{g.D=(o.get('D')/DP).toFixed(1)}
			g.P=o.get('P');g.I=o.get('I');g.T=o.createdAt;g.Index=g.R*2e9+g.T.getTime()
			g.Cover=o.get('Cover')
			g.html=document.createElement('div')
			g.html.className='game';g.html.id=g.id
			g.html.onclick=function(){playGame(this.id)}
			g.html.innerHTML=`<img width='96' height='96' src='${g.Cover}'/>
<div class='a'>
	<h1>${g.N}</h1><div class='clear' id='c${g.id}'></div>
	<span${gameC(g,0)}>Needle</span><span${gameC(g,1)}>Trap</span><span${gameC(g,2)}>Gimmick</span><div class='GI'><div></div></div>
	<div class='rating'>${g.Ra}</div>
	<div class='raB'></div>
	<div class='raC' style='width:${(g.R<<0)*24+g.R%1*19+(g.R%1*20>1)*3}px'></div>
	<div class='pa'>
		<p>作者：${g.A}</p>
		<p class='in'>难度：${g.D}</p><p class='in r'>人气：${g.P}</p>
		<p>发布时间：${gameD(g.T)}</p>
	</div>
</div>`
			let ogi=g.html.getElementsByClassName('GI')[0]
			ogi.firstChild.innerText=g.I
			ogi.onmouseover=function(){this.lastChild.style.opacity=1}
			ogi.onmouseout=function(){this.lastChild.style.opacity=0}
			if(!g.Hide)GAME.push(g);
			if(o.get('A').id===currentUser.id){
				let mg=g.html.cloneNode(true);mg.id=g.id
				mg.innerHTML+=`<div class='delg'>删除游戏</div><div class='delg' style='left:50%'>下载游戏</div>`
				let c=mg.getElementsByClassName('delg')
				c[0].id=g.id;c[1].id=o.get('Room').id;c[1].name=g.N
				c[0].onclick=function(){deleteGame(this.id)}
				c[1].onclick=function(){
					let q=new AV.Query('GRoom')
					q.get(this.id).then(r=>{
						let o=r._serverData;o.N=this.name;delete o.CT;delete o.CD
						let a=document.createElement('a');a.download=this.name+'.iw'
						a.href=URL.createObjectURL(new Blob([JSON.stringify(o)]));a.click()
					},e=>{console.log(e);alert('下载失败！')})
				}
				MyGame.push(mg)
			}
		})
		loadGAME();Author.sort((a,b)=>a.T-b.T);Author.sort((a,b)=>b.R/b.N-a.R/a.N)
		board[2].innerHTML=''
		for(let i=0;i<15;i++){
			if(!Author[i])break
			board[2].innerHTML+=`<div>${i+1}</div><ul><span>${Author[i].A}</span><span style='position:absolute;left:210px'>${(Author[i].R/Author[i].N).toFixed(1)}</span></ul><br/>`
		}
		if(MyGame.length>0){
			$('mk').firstChild.style.marginTop='-120px'
			$('mk').children[1].style.display='block'
			var my=$('my');my.style.display='block'
			MyGame.forEach(o=>{my.appendChild(o)})
		}
		$('warp').style.opacity=1
		if(played.length)setPlayed()
	}},console.log)
}
function gameC(o,n){
	if(o.C===n)return ` class='l'`;return '';
}
function cov(n){
	if(n<10)return '0'+n;return n;
}
function gameD(d){
	return `${d.getFullYear()}-${cov(d.getMonth()+1)}-${cov(d.getDate())}`;
}
ga=$('ga');gp=$('gp');
function loadGAME(){
	gp.innerHTML='';
	for(var i=0;i<GAME.length;i++)gp.appendChild(GAME[i].html)
	//Filter()
	if(lastDate!==1){
		var more=document.createElement('div')
		more.id='more';more.innerText='点击加载更多'
		more.onclick=function(){
			if(lastDate===1)return;this.innerText='加载中...';fetchGame()
		}
		gp.appendChild(more)
	}
}
fetchGame()
function playGame(id){window.open('./Play?'+id)}
board=document.getElementsByClassName('bdarea');
var query=new AV.Query('_User');
query.descending('Exp');
query.addAscending('createdAt');
query.limit(10);query.select(['username','Exp']);
query.find().then(function(r){
	r.forEach((o,ind)=>{
		board[1].innerHTML+=`<div>${ind+1}</div><ul><span>${o.get('username')}</span><span style='position:absolute;left:210px'>Lv.${getLv(o.get('Exp')).l}</span></ul><br/>`
	})
},function(e){console.log(e)});
ps=$('ps').getElementsByClassName('opt');
ps[0].onclick=function(){GAME.sort((a,b)=>b.T-a.T);loadGAME()}
ps[1].onclick=function(){GAME.sort((a,b)=>b.R===a.R?b.P-a.P:b.R-a.R);loadGAME()}
ps[2].onclick=function(){GAME.sort((a,b)=>b.D-a.D);loadGAME()}
ps[3].onclick=function(){GAME.sort((a,b)=>b.P-a.P);loadGAME()}
ps[4].onclick=function(){GAME.sort((a,b)=>b.Index-a.Index);loadGAME()}
RBar=$('RBar');RG=$('Range');RG.S=0;RG.E=100;RBC=$('RBar').children;
RBC[0].onmousedown=function(e){
	RBar.on=this;this.dX=e.clientX-this.offsetLeft;this.dY=e.clientY-this.offsetTop
}
RBC[1].onmousedown=RBC[0].onmousedown
ps[5].S=0;ps[5].E=100;ps[5].txt='难度'
ps[5].w=100;ps[5].step=1
ps[6].S=0;ps[6].E=5;ps[6].txt='评分'
ps[6].w=50;ps[6].step=0.1
ps[5].onclick=function(){
	$('GUI').style.display='block'
	$('GUIA').style.display='block'
	RBar.style.display='block'
	RBar.w=this.w;RBar.step=this.step;RG.o=this
	RG.S=this.S.toFixed(1/RBar.step>>3)
	RG.E=this.E.toFixed(1/RBar.step>>3)
	RG.innerText=`${RG.S}~${RG.E}`;
	RBC[0].style.left=`${RG.S/RBar.w/RBar.step*180}px`
	RBC[1].style.left=`${RG.E/RBar.w/RBar.step*180}px`
}
ps[6].onclick=ps[5].onclick
RGO=document.getElementsByClassName('RGO')
RGO[0].onclick=function(){
	RG.o.S=Number(RG.S);RG.o.E=Number(RG.E)
	RG.o.innerText=`${RG.o.txt} ${RG.S}~${RG.E}`
	$('GUI').style.display='none'
	$('GUIA').style.display='none'
	RBar.style.display='none'
	Filter()
}
Tag=$('Tag').firstChild.children;
for(let i=Tag.length;i--;){
	Tag[i].onclick=function(){
		if(this.style.color){this.style.color=''}
		else{this.style.color='#AAA'}
	}
}
ps[7].onclick=function(){
	$('GUI').style.display='block'
	$('GUIA').style.display='block'
	$('Tag').style.display='block'
}
RGO[1].onclick=function(){
	$('GUI').style.display='none'
	$('GUIA').style.display='none'
	$('Tag').style.display='none'
	if(!Tag[0].style.color&&!Tag[1].style.color&&!Tag[2].style.color){
		ps[7].innerText='类型 All'
	}else{
		ps[7].innerText='类型 ';var n=false
		for(let i=0;i<Tag.length;i++){
			if(!Tag[i].style.color){
				if(n)ps[7].innerText+=' &'
				ps[7].innerText+=` ${Tag[i].innerText} `;n=true
			}
		}
	}
	Filter()
}
var filterClear=0
ps[8].onclick=()=>{filterClear=(filterClear===1)?0:1;Filter()}
ps[9].onclick=()=>{filterClear=(filterClear===2)?0:2;Filter()}
function Filter(){
	GAME.forEach(o=>{
		if(o.D==='??'){var d=0}else{var d=o.D}
		if(d>=ps[5].S && d<=ps[5].E && o.R>=ps[6].S && o.R<=ps[6].E && !Tag[o.C].style.color){
			if(sch.value!==''){
				if(o.N.toLowerCase().includes(sch.value.toLowerCase())||o.A.toLowerCase().includes(sch.value.toLowerCase())){
					o.html.style.display='inline-block'
				}else{o.html.style.display='none'}
			}else{o.html.style.display='inline-block'}
			if(filterClear===1){
				if(o.html.played)o.html.style.display='none'
			}else if(filterClear===2){
				if(!o.html.played)o.html.style.display='none'
			}
		}else{o.html.style.display='none'}
	})
}
sch=$('search');sch.oninput=Filter
EM=$('Email').children
EM[1].oninput=function(){
	EM[2].innerText='注：邮箱仅用于账号验证和重置密码'
	EM[2].style.color='';this.style.borderColor=''
}
RGO[2].onclick=function(){
	if(EM[1].value===''){
		EM[2].innerText='请输入邮箱地址'
		EM[2].style.color='#FF4444'
		EM[1].style.borderColor='#FF4444'
		return false;
	}else{
		currentUser.setEmail(EM[1].value)
		currentUser.save().then(function(e){
			alert('已发送验证邮件到您的邮箱，请尽快完成邮箱验证')
			$('GUI').style.display='none'
			$('GUIA').style.display='none'
			$('Email').style.display='none'
		},function(e){
			if(e.code==125){
				EM[2].innerText='邮箱地址无效'
				EM[2].style.color='#FF4444'
				EM[1].style.borderColor='#FF4444'
			}else if(e.code==203){
				EM[2].innerText='电子邮箱已被使用'
				EM[2].style.color='#FF4444'
				EM[1].style.borderColor='#FF4444'
			}else{
				EM[2].innerText='邮箱绑定失败'
				EM[2].style.color='#FF4444'
			}
		})
	}
}
RGO[3].onclick=function(){
	$('GUI').style.display='none';$('GUIA').style.display='none';$('Email').style.display='none'
}
RGO[5].onclick=function(){
	$('GUI').style.display='none';$('GUIA').style.display='none';$('CUN').style.display='none'
}
CUN=$('CUN').children
CUN[1].maxLength=20
CUN[1].oninput=function(){
	CUN[2].innerText='请输入用户名'
	CUN[2].style.color='';this.style.borderColor=''
}
RGO[4].onclick=function(){
	if(CUN[1].value===''){
		CUN[2].innerText='请输入用户名'
		CUN[2].style.color='#FF4444'
		CUN[1].style.borderColor='#FF4444'
		return false;
	}else if(CUN[1].value.length<2){
		CUN[2].innerText='2～20字符'
		CUN[2].style.color='#FF4444'
		CUN[1].style.borderColor='#FF4444'
		return false;
	}else{
		currentUser.setUsername(CUN[1].value);
		currentUser.save().then(function(e){
			alert('用户名修改成功！')
			$('GUI').style.display='none'
			$('GUIA').style.display='none'
			$('CUN').style.display='none'
		},function(e){
			if(e.code==202){
				CUN[2].innerText='该用户已存在'
				CUN[2].style.color='#FF4444'
				CUN[1].style.borderColor='#FF4444'
			}else{
				CUN[2].innerText='用户名修改失败'
				CUN[2].style.color='#FF4444'
			}
		})
	}
}
const deleteGame=function(id,inf=''){
	if(!confirm('确定要删除此游戏吗？\n这会删除此游戏所有数据，玩家已获得的经验也会扣除'))return false;
	var data={"id":id,"inf":inf}
	AV.Cloud.run('delGame',data).then(
		function(r){alert('删除成功！');window.location.reload()},
		function(e){
			if(e.rawMessage=='R'){alert('访问遭到拒绝！')}
			else{alert('操作失败！')}
		}
	);
}
function jq(){
	var o=$('GUI')
	o.style.display='block';o.style.background='rgba(0,0,0,0.3)'
	o.onclick=function(){
		$('GUIB').style.display='none'
		this.style.background='';this.style.display='none';this.onclick=null
	}
	$('GUIB').style.display='block';$('GUIB').firstChild.src='img/0.webp'
}
$('fk').onclick=jq;
$('zz').onclick=function(){
	var o=$('GUI')
	o.style.display='block';o.style.background='rgba(0,0,0,0.3)'
	o.onclick=function(){
		$('GUIC').style.display='none'
		this.style.background='';this.style.display='none';this.onclick=null
	}
	$('GUIC').style.display='block'
	$('GUIC').style.backgroundImage='url(img/1.webp)'
}