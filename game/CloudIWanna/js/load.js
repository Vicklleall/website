scrn=$('screen');
scr=scrn.getContext('2d');
scr.imageSmoothingEnabled=false;
tiles=document.createElement('canvas')
tiles.width=scrn.width;
tiles.height=scrn.height;
$('surface').insertBefore(tiles,scrn)
scrT=tiles.getContext('2d');
scrT.imageSmoothingEnabled=false;
colSF=document.createElement('canvas');
colSF.width=832;colSF.height=640;
colsf=colSF.getContext('2d');
colsf.imageSmoothingEnabled=false;
scref=document.createElement('canvas');
scref.width=800;scref.height=608;
sfscr=scref.getContext('2d');
sfscr.imageSmoothingEnabled=false;
GUI=$('GUI');
GO=$('GO');GC=$('GC');Bd=$('Bd');
GCobj=GC.children;EF=[];
$('WYY').onclick=function(){
	if(BGMid.value){
		var a=document.createElement('a');
		a.style.display='none';a.target='_blank';
		a.href='https://music.163.com/#/song?id='+BGMid.value;
		document.body.appendChild(a);
		a.click();
		document.body.removeChild(a);
	}
}
BGMplay=$('BGMplay');
BGMplay.onclick=function(){
	if(oBGM.paused){
		oBGM.play();this.style.backgroundImage='url(css/pause.png)';
	}else{
		oBGM.pause();this.style.backgroundImage='url(css/play.png)';
	}
}
BGMBox=$('BGMBox');oBGM=$('BGM');oBGM.loop=true;
BGMVa=$('BGMVa');BGMVb=$('BGMVb');
oBGM.onerror=function(e){
	this.pause();BGMplay.style.backgroundImage='url(css/play.png)'
	console.log(e);if(edit)alert('音乐载入出错！')
}
BGMVa.onmousedown=function(e){
	this.on=true;
	this.dX=e.clientX-this.offsetLeft;
}
BGMBox.onmousemove=function(e){
	if(BGMVa.on){
		setVolume(Math.min(Math.max(0,e.clientX-BGMVa.dX)/BGMVb.offsetWidth,1));
	}
}
function setVolume(a){
	oBGM.volume=2**a-1;localStorage.setItem('Volume',a);
	objAudio.vol.gain.setValueAtTime(a,objAudio.currentTime);
	a*=100;BGMVa.style.left=a+'%';
	BGMVb.style.backgroundImage=`linear-gradient(90deg,#e12828 ${a}%,transparent 0)`;
}
BGMBox.onmouseup=function(){BGMVa.on=false}
BGMBox.onmouseleave=BGMBox.onmouseup;
AudioContext=AudioContext||webkitAudioContext;
objAudio=new AudioContext();
objAudio.vol=objAudio.createGain();
objAudio.vol.connect(objAudio.destination);
if(localStorage.getItem('Volume')!==null){
	setVolume(Number(localStorage.getItem('Volume')));
}else{setVolume(0.5)}
const LOAD=function(){
//Audio
sndJump=loadAudio('sndJump.ogg');
sndShoot=loadAudio('sndShoot.ogg');
sndDeath=loadAudio('sndDeath.ogg');
sndWallJump=loadAudio('sndWallJump.ogg');
sndExtraJump=null;
sndGainJump=null;
sndLoseJump=null;
sndSpring=null;
sndBreak=null;
//Sprite
sprPlayerStart=[loadSpr('sprPlayerStart')];
sprRCherry=[loadSpr('sprRCherry')];
sprShooter=[loadSpr('sprShooter1'),loadSpr('sprShooter2'),loadSpr('sprShooter3'),loadSpr('sprShooter4'),loadSpr('sprShooter5')];
sprSG=[loadSpr('sprSG')];
sprSJump=[loadSpr('sprSJump')];
sprDJump=[loadSpr('sprDJump')];
sprTJump=[loadSpr('sprTJump')];
sprIJump=[loadSpr('sprIJump')];
sprPStar=[loadSpr('sprPStar')];
sprLGrav=[loadSpr('sprLGrav')];
sprHGrav=[loadSpr('sprHGrav')];
sprFSpd=[loadSpr('sprFSpd')];
sprSSpd=[loadSpr('sprSSpd')];
sprTrigger=[loadSpr('sprTrigger')];
sprPt=[loadSpr('ptCloud'),loadSpr('ptSmoke'),loadSpr('ptSnow')]
sprFake=[loadSpr('sprFBlock'),loadSpr('sprFPf')]
}
Sprites=['sprBlock',
'sprFloor',
'sprSpike',
'sprPlatform',
'sprWater',
'sprBlood3',
'sprBullet2',
'sprSave2',
'sprCherry2',
'sprPlayerIdle4',
'sprPlayerRun4',
'sprPlayerJump2',
'sprPlayerFall2',
'sprPlayerSlide2',
'sprWallJump',
'sprJR',
'sprCrate',
'sprWarp',
'sprIce',
'sprSpring2',
'sprBoard',
'sprTp',
'sprConveyor',
'sprBBlock'];
loadCount=0;Loading=$('loading');
sprNumber=62;
function sndInit(s){
	if(!window[s])window[s]=loadAudio(s+'.ogg');
}
function loadAudio(f){
	var o={};
	var xml=new XMLHttpRequest();
	xml.responseType='arraybuffer';
	xml.open('GET','snd/'+f,true);
	xml.onload=function(){
		objAudio.decodeAudioData(xml.response,function(res){o.src=res},function(e){console.log(e)});
	}
	xml.send();
	return o;
}
function sndPlay(o,p){
	o.snd=objAudio.createBufferSource();
	o.snd.buffer=o.src;
	if(p)o.snd.playbackRate.value=p;
	o.snd.connect(objAudio.vol);
	o.snd.start(0);
}
function loadSpr(s,t='',col){
	var i=new Image();
	i.src='img/spr/'+t+s+'.png';
	if(col){
		var c=document.createElement('canvas');
		c.src=i.src;
	}
	i.onload=function(){
		if(col){
			c.width=this.width;
			c.height=this.height;
			let d=c.getContext('2d');
			d.drawImage(this,0,0);
			d.globalCompositeOperation='source-in';
			d.fillStyle=col;
			d.fillRect(0,0,c.width,c.height);
			if(this.src.includes('Conveyor'))fillCoveyor=scr.createPattern(c,'repeat-x');
		}else{
			if(this.src.includes('Conveyor')){
				fillCoveyor=scr.createPattern(this,'repeat-x');
			}else if(this.src.includes('sprLGrav')){
				fillLGrav=scr.createPattern(this,'repeat');
			}else if(this.src.includes('sprHGrav')){
				fillHGrav=scr.createPattern(this,'repeat');
			}else if(this.src.includes('sprSSpd')){
				fillSSpd=scr.createPattern(this,'repeat');
			}else if(this.src.includes('sprFSpd')){
				fillFSpd=scr.createPattern(this,'repeat');
			}
		}
		loadCount+=1;
		Loading.innerHTML=`<pre>Loading...
${(loadCount/sprNumber*100).toFixed(0)}%</pre>`;
	};
	i.onerror=function(){
		if(s==='sprFloor'){
			s='sprBlock';
			this.src='img/spr/'+t+s+'.png';
		}else if(!this.er){
			this.src='img/spr/'+s+'.png';
			this.er=true;
		}
	}
	if(col)return c;
	return i;
}
function loadSkin(th,col){
	for(var ii=0;ii<Sprites.length;ii++){
		var o=Sprites[ii];
		var c=col && col[ii];
		var l=Number(o.charAt(o.length-1));
		if(l){
			o=o.slice(0,-1);
			if(window[o]){
				window[o].splice(0);
			}else{
				window[o]=[];
			}
			for(let i=1;i<=l;i++){
				let s=o+i;
				window[o].push(loadSpr(s,th[ii],c));
			}
		}else{
			if(window[o]){
				window[o][0]=loadSpr(o,th[ii],c);
			}else{
				window[o]=[loadSpr(o,th[ii],c)];
			}
		}
	}
}
function setSprCol(col,col2,ind){
	var c=Array.from(Sprites,o=>{
		switch(o){
			case 'sprWallJump':case 'sprConveyor':case 'sprJR':return col2;
		};
		return col;
	});
	loadSkin(AllTheme[ind],c);
}
function setColSpr(spr,col){
	var sf=document.createElement('canvas');
	sf.width=spr.width;
	sf.height=spr.height;
	sf.src=spr.src;
	var s=sf.getContext('2d');
	s.drawImage(spr,0,0);
	s.globalCompositeOperation='source-in';
	s.fillStyle=col;
	s.fillRect(0,0,sf.width,sf.height);
	return sf;
}
//Theme
AllTheme=[];
AllTheme[2]=Array.from(Sprites,o=>{
	switch(o){
		case 'sprWarp':case 'sprSave2':case 'sprCherry2':case 'sprCrate':case 'sprJR':case 'sprPlatform':case 'sprSpring2':case 'sprBoard':case 'sprConveyor':case 'sprBBlock':return 'RMJ/';
	};
	return '';
});
AllTheme[3]=Array.from(Sprites,o=>{
	switch(o){
		case 'sprBlock':case 'sprSpike':case 'sprWarp':case 'sprConveyor':case 'sprFloor':case 'sprPlatform':case 'sprSave2':case 'sprCrate':case 'sprJR':return 'NANG/';
	};
	return '';
});
AllTheme[4]=Array.from(Sprites,o=>{
	switch(o){
		case 'sprBlock':case 'sprSpike':case 'sprFloor':case 'sprIce':return 'Guy01/';
	};
	return '';
});
AllTheme[5]=Array.from(Sprites,o=>{
	switch(o){
		case 'sprSpike':return 'Guy01/';break;
		case 'sprBlock':case 'sprFloor':return 'Guy02/';break;
	};
	return '';
});
AllTheme[6]=Array.from(Sprites,o=>{
	switch(o){
		case 'sprBlock':case 'sprSpike':case 'sprFloor':case 'sprCherry2':case 'sprPlatform':case 'sprWarp':return 'K3S5/';
	};
	return '';
});
AllTheme[7]=Array.from(Sprites,o=>{
	switch(o){
		case 'sprBlock':case 'sprSpike':case 'sprFloor':case 'sprPlatform':case 'sprJR':case 'sprPlayerIdle4':case 'sprPlayerRun4':case 'sprPlayerJump2':case 'sprPlayerFall2':case 'sprPlayerSlide2':return 'Crimson/';
	};
	return '';
});
AllTheme[8]=Array.from(Sprites,o=>{
	switch(o){
		case 'sprBlock':case 'sprSpike':case 'sprFloor':case 'sprWater':case 'sprWallJump':case 'sprBullet2':case 'sprCherry2':case 'sprPlatform':case 'sprSave2':case 'sprCrate':return 'Darejar/';
	};
	return '';
});
AllTheme[9]=Array.from(Sprites,o=>{
	switch(o){
		case 'sprBlock':case 'sprSpike':case 'sprFloor':case 'sprPlatform':case 'sprWallJump':case 'sprWarp':case 'sprSave2':case 'sprCrate':case 'sprJR':case 'sprWater':case 'sprCherry2':case 'sprBoard':return 'Adventure/';
	};
	return '';
});
AllTheme[10]=Array.from(Sprites,o=>{
	switch(o){
		case 'sprBlock':case 'sprFloor':case 'sprPlatform':return 'Hanamogeta/';break;
		case 'sprSpike':case 'sprWater':case 'sprWallJump':case 'sprBullet2':case 'sprCherry2':case 'sprSave2':case 'sprCrate':return 'Darejar/';break;
	};
	return '';
});
AllTheme[11]=Array.from(Sprites,o=>{
	switch(o){
		case 'sprBlock':case 'sprFloor':case 'sprPlatform':case 'sprSpike':return 'Shadowlink57/';
	};
	return '';
});
AllTheme[12]=Array.from(Sprites,o=>{
	switch(o){
		case 'sprBlock':case 'sprSpike':case 'sprFloor':case 'sprWater':case 'sprCherry2':case 'sprPlatform':case 'sprSave2':case 'sprCrate':case 'sprConveyor':case 'sprSpring2':return 'Hibikusuku/';
	};
	return '';
});
AllTheme[13]=Array.from(Sprites,o=>{
	switch(o){
		case 'sprBlock':case 'sprFloor':case 'sprCherry2':case 'sprWater':case 'sprCrate':return 'Kamirin/';break;
		case 'sprSpike':return 'K3S5/';break;
	};
	return '';
});
AllTheme[14]=Array.from(Sprites,o=>{
	switch(o){
		case 'sprBlock':case 'sprFloor':case 'sprSpike':case 'sprSave2':case 'sprPlatform':case 'sprWater':case 'sprWarp':case 'sprBoard':case 'sprWallJump':case 'sprJR':return 'MELTDOWN/';break;
		case 'sprCherry2':case 'sprCrate':return 'Kamirin/';break;
	};
	return '';
})
AllTheme[15]=Array.from(Sprites,o=>{
	switch(o){
		case 'sprBlock':case 'sprSpike':case 'sprFloor':case 'sprPlatform':return 'NANGS8/';break
		case 'sprWarp':case 'sprPlatform':case 'sprSave2':case 'sprCrate':case 'sprJR':return 'NANG/';break
	};
	return '';
})
AllTheme[16]=Array.from(Sprites,o=>{
	switch(o){
		case 'sprBlock':case 'sprSpike':case 'sprFloor':return 'uhuhu3/';
	}
	return '';
})
AllTheme[17]=Array.from(Sprites,o=>{
	switch(o){
		case 'sprBlock':case 'sprSpike':case 'sprFloor':case 'sprSave2':return 'Midnight/';break
		case 'sprCherry2':return 'K3S5/';break
	}
	return '';
})
AllTheme[0]=Array.from(Sprites,o=>{
	switch(o){
		case 'sprSave2':case 'sprWater':case 'sprCrate':case 'sprJR':case 'sprBBlock':return 'Color/';break;
		case 'sprBoard':return 'RMJ/';break;
	};
	return '';
})
AllTheme[1]=Array.from(Sprites,o=>{
	switch(o){
		case 'sprWater':return 'Color/';break;
		case 'sprBlood3':case 'sprBullet2':case 'sprWallJump':return '';break;
	};
	return 'Line/';
})