const edit=false;const nowObj=null;
REC=false;REP=false;
RECT=0;SAVE=undefined;
KD=[];KU=[];
KD[vk_shift]='SH';KU[vk_shift]='sh';
KD[vk_left]='LF';KU[vk_left]='lf';
KD[vk_right]='RT';KU[vk_right]='rt';
KD[vk_Z]='Z';KU[vk_Z]='z';
KD[vk_S]='S';KU[vk_S]='s';
KDR=[];KUR=[];
const objTime=function(){
	var cTime=Date.now();
	var pTime=Date.now();
	Time.time=0;Death.innerText='0';
	var deaths=0;RECT=0;SRECT=0;
	RECO={SH:'',sh:'',LF:'',lf:'',RT:'',rt:'',Z:'',z:'',S:'',s:'',0:'',1:'',2:'',3:'',4:'',5:'',6:'',7:'',8:'',9:''};
	SRECO={...RECO};UPRK=false;
	this.getTime=function(){return (Date.now()-cTime)/1000}
	this.addDeaths=function(){deaths+=1}
	this.getDeaths=function(){return deaths}
	this.pause=function(){
		pause=!pause;
		if(pause){
			Loading.style.display='block';
			pTime=Date.now();
		}else{
			cTime+=Date.now()-pTime;
			Loading.style.display='none';
		}
	}
	this.strep=function(o){
		repl=o.ind+1;pTime=Date.now();
	}
	this.resume=function(){
		saveX=REPBF.x;saveY=REPBF.y;
		JumpNum=REPBF.j;
		room_load(true,true);
		cTime+=Date.now()-pTime;
		RECT=REPBF.r;SRECT=REPBF.s;
		RECO={...REPBF.o};
		SRECO={...RECO};
	}
}
function room_load(f,r,pl){
	roomInit();loadRoom();
	if(pl)return false;
	if(!r){
		saveX=startX;saveY=startY;JumpNum=1;BKEF=[]
	}
	blood=null
	scrT.clearRect(0,0,tiles.width,tiles.height);
	drawInit();resetGUI();
	GCB.style.transform='translateX(-50%) translateY(-50%) scale(0) rotate(0deg)';
	$('ach').style.top='400px';
	if(player){
		if(ViewMode===0){
			toVX=Math.min(RoomW-800,Math.max(0,(player.x/800<<0)*800));
			toVY=Math.min(RoomH-608,Math.max(0,(player.y/608<<0)*608));
		}else{
			toVX=Math.max(400,Math.min(player.x,RoomW-400))-400;
			toVY=Math.max(304,Math.min(player.y,RoomH-304))-304;
		}
		RViewX=toVX;RViewY=toVY;
		ViewX=Math.round(RViewX);
		ViewY=Math.round(RViewY);
		activate();
	}
	if(shooter.length>9)oBGM.currentTime=0
}
END=false;
BGx=0;BGy=0;BKEF=[];
scr.textAlign='center';scr.font='bold 13px sans-serif'
function evDraw(){
	drawBegin();drawBGEF()
	draw_obj(field);
	spike.forEach(o=>{
		if(o.__proto__.constructor.name!=='objSpike')o.draw_self(scr)
	})
	block.forEach(o=>{
		if(o.__proto__.constructor.name==='objCrate'||o.__proto__.constructor.name==='objBreakableBlock')o.draw_self();
	})
	draw_obj(conveyor);
	draw_obj(GMStepObj);
	draw_obj(GMColObj,scr);
	draw_obj(platform);
	draw_obj(cherry,scr);
	draw_obj(bullet,0,true);
	draw_obj(objEf,0,true);
	if(player){
		player.draw_self();
	}
	if(blood){
		if(blood.length<50){
			for(var i=5;i--;){
				blood.push(new objBlood(bloodX,bloodY));
			}
		}
		blood.forEach(o=>o.draw_self());
	}
	warpEnd.draw_self(scr);
	draw_obj(warp,scr);draw_obj(water);
	drawEf()
	for(let k in USERS){
		let [spr,ind,x,y,sc]=USERS[k]
		switch(spr){
			case 'i':spr=sprPlayerIdle;break
			case 'r':spr=sprPlayerRun;break
			case 'j':spr=sprPlayerJump;break
			case 'f':spr=sprPlayerFall;break
			case 's':spr=sprPlayerSlide;break
		}
		scr.globalAlpha=0.5
		if(sc<0){
			scr.save();scr.scale(-1,1);
			scr.drawImage(spr[ind],-x-17,y-23);
			scr.restore();
		}else{
			scr.drawImage(spr[ind],x-17,y-23);
		}
		scr.fillStyle='#FFFFFF'
		scr.fillText(k,x,y-24)
		scr.strokeStyle='#000000'
		scr.strokeText(k,x,y-24)
		scr.globalAlpha=1
	}
	if(!END && !REP && oTime){
		Time.time=oTime.getTime();
		let s=Time.time%60;if(s-(s<<0)<0.03)Time.innerText=cov((Time.time-s)/60)+':'+cov(s<<0);
	}
}
repl=false;REPIND=undefined;
keyShow=tab[4].getElementsByClassName('key')
function GameRun(){
	if(repl){
		replay(RANK[repl-1].R);repl=false;REPIND=repl;
	}
	key_check(vk_P);
	if(keyboard_check_pressed(vk_P))oTime.pause();
	if(pause){
		keyPause();
	}else{
		if(keyDown[82]){
			if(!edit && !REP && !OnUp){
				keyPause()
				if(END){
					room_load();oTime=new objTime();END=false
				}else{
					room_load(true,true)
					RECT=SRECT;RECO={...SRECO};
				}
			};keyDown[82]=false
		}else if(keyDown[116]){
			if(!edit && !REP && !OnUp){
				keyPause();room_load();oTime=new objTime();END=false
			};keyDown[116]=false
		}
	}
	key_check(vk_right);key_check(vk_left);
	key_check(vk_up);key_check(vk_down);
	key_check(vk_shift);
	key_check(vk_Z);key_check(vk_S);
	if(!pause){
		if(!END){evStep();BGEF();evMove()};
		evDraw();RECT+=1;
	}
	if(nowTab===4){
		keyShow[0].style.background=key[vk_shift]?'#dddddd':''
		keyShow[1].style.background=key[vk_Z]?'#dddddd':''
		keyShow[2].style.background=key[vk_left]?'#dddddd':''
		keyShow[3].style.background=key[vk_right]?'#dddddd':''
		keyShow[4].style.background=''
		for(let i=0;i<10;i++){
			let t=RECO[i].charCodeAt(0)-100
			if(t<=RECT&&t>RECT-10){
				keyShow[4].style.background='#dddddd';RECO[i]=RECO[i].slice(1);break
			}
		}
	}
	key_clear(vk_right);key_clear(vk_left);
	key_clear(vk_up);key_clear(vk_down);
	key_clear(vk_shift);key_clear(vk_Z);
	key_clear(vk_S);key_clear(vk_P);
	if(!REP&&!END&&!pause&&Date.now()%50<20){
		let c=0;for(let k in USERS)c++;if(c)pushData()
	}
}
function replay(o){
	keyPause();
	key_check(vk_right);key_check(vk_left);
	key_check(vk_shift);
	key_check(vk_Z);key_check(vk_S);
	REPBF={};REPBF.o={...RECO};
	REPBF.r=RECT;REPBF.s=SRECT;
	REPBF.x=saveX;REPBF.y=saveY;
	REPBF.j=JumpNum;
	RECO={...o};RECT=0;SRECT=0;
	REC=false;REP=true;END=false;
	KDR[vk_shift]=0;KUR[vk_shift]=0;
	KDR[vk_left]=0;KUR[vk_left]=0;
	KDR[vk_right]=0;KUR[vk_right]=0;
	KDR[vk_Z]=0;KUR[vk_Z]=0;
	KDR[vk_S]=0;KUR[vk_S]=0;
	room_load();keyPause();
}