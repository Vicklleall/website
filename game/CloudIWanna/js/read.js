function readRoom(rr){
	var Version=rr['V'];
	var str=rr['B'];var o;
	if(str.length>0){
		if(Version<0.5){
		o={
			n:2,o:block,obj:objBlock,
			change:function(v){
				this.f=undefined;
				switch(v){
					case 10:this.o=water;this.obj=objWater;break;
					case 11:this.o=block;this.obj=objIceBlock;break;
					case 12:case 13:case 14:case 15:this.o=field;this.obj=objField;this.f=v-12;break;
				}
			},
			set:function(a){this.w=a[0];this.h=a[1]},
			create:function(x,y){
				if(this.obj==objField){
					this.o.push(new this.obj(x,y,this.w,this.h,this.f))
				}else{
					this.o.push(new this.obj(x,y,this.w,this.h))
				}
			},
		}
		}else{
		o={
			n:1,o:block,obj:objBlock,
			change:function(v){
				switch(v){
					case 10:this.o=water;this.obj=objWater;break;
					case 11:this.o=block;this.obj=objIceBlock;break;
					case 12:case 13:case 14:case 15:this.o=field;this.obj=objField;this.f=v-12;break;
					case 16:this.o=trigger;this.obj=objTrigger;break;
				}
			},
			set:function(a){this.w=(a[0]>>5)*32;this.h=(a[0]-this.w)*32},
			create:function(x,y){
				if(this.obj==objField){
					this.o.push(new this.obj(x,y,this.w,this.h,this.f))
				}else{
					this.o.push(new this.obj(x,y,this.w,this.h))
				}
			},
		}
		}
		readStr(str,o);
	}
	str=rr['S'];
	if(str.length>0){
		o={
			n:3,set:function set(a){this.w=a[0];this.h=a[1];this.ang=a[2]},
			create:function create(x,y){
				let l=spike.push(new objSpike(x,y,this.ang,spike.length));
				spike[l-1].setScale(this.w,this.h);
			},
		}
		readStr(str,o);
	}
	str=rr['O'];
	if(str.length>0){
		o={
			n:0,o:GMStepObj,
			change:function(v){
				switch(v){
					case 10:this.obj=objSavePoint;break;
					case 11:this.obj=objWallJump;this.o=Walljump;this.side=-1;break;
					case 13:this.obj=objWallJump;this.o=Walljump;this.side=1;break;
					case 12:this.obj=objCrate;this.o=block;break;
					case 14:
						this.obj=objJumpRefresher;this.o=GMColObj;
						sndInit('sndExtraJump');
					break;
					case 15:case 16:case 17:case 18:
						this.obj=objJumpChanger;this.o=GMColObj;
						if(v===18){this.num=Infinity}else{this.num=v-15};
						sndInit('sndGainJump');sndInit('sndLoseJump');
					break;
					case 19:case 20:case 21:
						this.obj=objFake;this.o=GMColObj;
						this.num=v-19;sndInit('sndBreak');
					break;
					case 22:
						this.obj=objBreakableBlock;
						this.o=block;sndInit('sndBreak');
					break;
				}
			},
			create:function(x,y){
				if(this.o===Walljump){
					this.o.push(new this.obj(x,y,this.side));
				}else if(this.obj===objJumpChanger||this.obj===objFake){
					this.o.push(new this.obj(x,y,this.num));
				}else{
					this.o.push(new this.obj(x,y));
				}
			},
		}
		readStr(str,o);
	}
	str=rr['O2'];
	if(str.length>0){
		o={
			n:2,o:platform,obj:objPlatform,
			change:function(v){
				switch(v){
					case 10:this.o=cherry;this.obj=objCherry;break;
					case 11:
						this.o=GMColObj;this.obj=objSpring;
						sndInit('sndSpring');
					break;
					case 12:
						this.o=GMColObj;this.obj=objTrampoline;
						sndInit('sndSpring');
					break;
					case 13:this.o=conveyor;this.obj=objConveyor;this.fx=0;break;
					case 14:this.o=conveyor;this.obj=objConveyor;this.fx=1;break;
				}
			},
			set:function(a){this.hspd=a[0]/10;this.vspd=a[1]/10},
			create:function(x,y){
				var l=this.o.push(new this.obj(x,y));
				if(this.obj===objSpring || this.obj===objTrampoline){
					this.o[l-1].ang=this.hspd*10;
					this.o[l-1].spd=this.vspd*10;
				}else if(this.o===conveyor){
					this.o[l-1].fx=this.fx;
					this.o[l-1].CW=this.hspd*10;
					this.o[l-1].spd=this.vspd;
				}else{
					this.o[l-1].hspeed=this.hspd;
					this.o[l-1].vspeed=this.vspd;
				}
			},
		}
		readStr(str,o);
	}
	str=rr['O3'] || '';
	if(str.length>0){
		o={
			n:3,o:warp,obj:objWarp,
			change:function(v){
				switch(v){}
			},
			set:function(a){this.a1=a[0];this.a2=a[1];this.a3=a[2]},
			create:function(x,y){
				var l=this.o.push(new this.obj(x,y));
				if(this.obj===objWarp){
					this.o[l-1].warpX=this.a1;
					this.o[l-1].warpY=this.a2;
					this.o[l-1].k=this.a3;
				}
			},
		}
		readStr(str,o);
	}
	str=rr['O4'];
	if(str.length>0){
		o={
			n:4,o:Rcherry,obj:objRoundingCherry,i:0,
			change:function(v){
				switch(v){
					case 10:this.obj=objRoundingCherry;break;
					case 11:this.obj=objSpikeGear;break;
					default:this.o=shooter;this.obj=objShooter;this.i=v-12;break;
				}
			},
			set:function(a){
				this.a1=a[0];this.a2=a[1];this.a3=a[2];this.a4=a[3];
			},
			create:function(x,y){
				this.o.push(new this.obj(x,y,this.i));
				if(this.o===Rcherry){
					var o=Rcherry[Rcherry.length-1];
					o.n=this.a1;o.r=this.a2;o.spd=this.a3/10;o.ang=this.a4;
				}else{
					var o=shooter[shooter.length-1];
					o.n=this.a1;o.spd=this.a2/10;o.t=this.a3;o.ang=this.a4;
				}
			}
		}
		readStr(str,o);
	}
	str=rr['Bd'] || '';
	if(str.length>0){
		for(var i=0;i<str.length;i++){
			var v=StN(str.charAt(i));
			if(v===-100){
				i+=1;var s=1;
				if(StN(str.charAt(i))===1){s=-1;i+=1};
				var x=cNum(str.charAt(i))*s;s=1;i+=1;
				if(StN(str.charAt(i))===1){s=-1;i+=1};
				var y=cNum(str.charAt(i))*s;
				var txt='';
				while(i+1<str.length && StN(str.charAt(i+1))!==-100){
					i+=1;txt+=str.charAt(i);
				}
				board.push(new objBoard(x,y,txt));
			}
		}
	}
}
const readStr=function (str,o){
	for(var i=0;i<str.length;i++){
		var s=1;var v=StN(str.charAt(i));
		if(v!==1 && v<100){
			i+=1;var a=[];
			for(var ii=0;ii<o.n;ii++){
				var iis=1;
				var iiv=StN(str.charAt(i));
				if(iiv===1){iis=-1;i+=1};
				a.push(cNum(str.charAt(i))*iis);i+=1;
			}
			if(o.change)o.change(v);
			if(o.set)o.set(a);
		}
		if(StN(str.charAt(i))===1){s=-1;i+=1};
		var x=cNum(str.charAt(i))*s;s=1;i+=1;
		if(StN(str.charAt(i))===1){s=-1;i+=1};
		var y=cNum(str.charAt(i))*s;
		o.create(x,y);
	}
}
saveEf=[]
function roomInit(){
	block=[];spike=[];bullet=[];
	platform=[];water=[];field=[];
	cherry=[];cherry.ind=0;cherry.img_spd=1/15;
	Rcherry=[];shooter=[];Walljump=[];warp=[];
	GMStepObj=[];GMColObj=[];conveyor=[];
	objEf=[];board=[];trigger=[];
}
function evStep(){
	platform.forEach(o=>o.evStep());
	if(player)player.evStep();
	bullet.forEach(o=>o.evStep());
	GMStepObj.forEach(o=>o.evStep());
}
function evMove(){
	cherry.ind+=cherry.img_spd;
	cherry.ind=cherry.ind%2;
	BKEF.forEach(o=>o.move());
	warpEnd.move();
	warp.forEach(o=>o.move());
	move(field);move(platform);
	move(block);move(Rcherry);move(shooter);
	spike.forEach(o=>{
		if(o.rid!==undefined){
			if(Rcherry[o.rid].inView)o.move()
		}
	});
	cherry.forEach(o=>{
		if(o.rid!==undefined){
			o.inView=true;
			if(Rcherry[o.rid].inView)o.move()
		}else if(o.inView){o.move()}
	});
	move(conveyor);
	if(player)player.move();
	board.forEach(o=>o.move());
	objEf.forEach(o=>o.move());
	if(!edit){
	PViewX=ViewX;PViewY=ViewY;
	if(ViewMode===0){
		if(player){
			ViewX=Math.min(RoomW-800,Math.max(0,(player.x/800<<0)*800));
			ViewY=Math.min(RoomH-608,Math.max(0,(player.y/608<<0)*608));
		}
	}else{
		if(player){
			toVX=Math.max(400,Math.min(player.x,RoomW-400))-400;
			toVY=Math.max(304,Math.min(player.y,RoomH-304))-304;
		}
		RViewX+=(toVX-RViewX)/20;
		RViewY+=(toVY-RViewY)/20;
		ViewX=Math.round(RViewX);
		ViewY=Math.round(RViewY);
	}
	if(BGhspd!==0 || BGvspd!==0){
		BGx+=BGhspd;BGy+=BGvspd;
	}
	if(PViewX!==ViewX||PViewY!==ViewY)activate()
	}
}
function drawBegin(){
	drawVX=ViewX-50;drawVY=ViewY-50;
	if(BGMode===3){
		if(BackGround.style.backgroundPosition!=='center center'){
			BackGround.style.backgroundPosition=`${BGx-ViewX}px ${BGy-ViewY}px`;
		}
	}
	scr.setTransform(1,0,0,1,-ViewX,-ViewY)
	scr.clearRect(ViewX,ViewY,scrn.width,scrn.height)
}
function drawInit(){
	player=new objPlayer(saveX,saveY);
	Rcherry.forEach(o=>{o.createR()});
	spike.forEach(o=>{
		if(o.__proto__.constructor.name==='objSpike')o.draw_self(scrT);
	})
	block.forEach(o=>{
		if(o.__proto__.constructor.name!=='objCrate'&&o.__proto__.constructor.name!=='objBreakableBlock')o.draw_self();
	})
	draw_obj(Walljump,0,true);
	draw_obj(board,0,true);
	if(OutMode===1){
		block.push(new objBlock(-32,-32,RoomW+64,32));
		block.push(new objBlock(-32,0,32,RoomH));
		block.push(new objBlock(RoomW,0,32,RoomH));
	}
	if(SAVE!==undefined){
		GMStepObj[SAVE].ind=1;
		GMStepObj[SAVE].img_spd=0.02;
		SAVE=undefined;
	}
	FlashSpd=0;FlashAl=0;toFlAl=0;FlashCol='#000';WarpR=null;
}
function resetGUI(){
	Bd.style.visibility='hidden';
	GUI.style.visibility='hidden';
	GUI.style.opacity='0';
	GUI.style.transition="none";
	GCobj[0].style.right='880px';
	GCobj[1].style.left='880px';
	GCobj[0].style.top='0';
	GCobj[1].style.top='0';
	GCobj[2].style.top='400px';
}
EFtimer=0;
function BGEF(){
	if(EF.length<8)return false;
	EFtimer+=1;
	if(EFtimer>=EF[6]){
		var o;var as=(Math.random()-0.5)/6;
		var s=EF[1]+(Math.random()*2-1)*EF[2];
		switch(EF[7]){
			case 0:
				o=BKEF[BKEF.push(new objBKEF(Math.random()*800,608+(s>>1),EF[0],s))-1];
				o.v=-EF[3]+(Math.random()*2-1)*EF[4];
			break;
			case 1:
				o=BKEF[BKEF.push(new objBKEF(Math.random()*800,-(s>>1),EF[0],s))-1];
				o.v=EF[3]+(Math.random()*2-1)*EF[4];
			break;
			case 2:
				o=BKEF[BKEF.push(new objBKEF(-(s>>1),Math.random()*608,EF[0],s))-1];
				o.h=EF[3]+(Math.random()*2-1)*EF[4];
			break;
			case 3:
				o=BKEF[BKEF.push(new objBKEF(800+(s>>1),Math.random()*608,EF[0],s))-1];
				o.h=-EF[3]+(Math.random()*2-1)*EF[4];
			break;
			case 4:
				o=BKEF[BKEF.push(new objBKEF(400,304,EF[0],s))-1];
				let spd=EF[3]+(Math.random()*2-1)*EF[4];
				let dir=Math.random()*2*Math.PI;
				o.h=spd*Math.cos(dir);
				o.v=spd*Math.sin(dir);
			break;
		}
		o.aspd=as;EFtimer=0;
	}
}
function drawBGEF(){
if(BackGround.style.background[13]==='u'){
	if(Date.now()%80<20){
		let xx=6,h=0;sfbg.clearRect(0,0,800,608)
		sfbg.fillStyle='#F2F4'
		for(;xx<96;xx+=32){
			sfbg.fillRect(xx,0,20,Math.random()*200)
			h=Math.random()*200;sfbg.fillRect(780-xx,608-h,20,h)
		}
		sfbg.fillStyle='#FF24'
		for(;xx<192;xx+=32){
			sfbg.fillRect(xx,0,20,Math.random()*200)
			h=Math.random()*200;sfbg.fillRect(780-xx,608-h,20,h)
		}
		sfbg.fillStyle='#22F4'
		for(;xx<352;xx+=32){
			sfbg.fillRect(xx,0,20,Math.random()*200)
			h=Math.random()*200;sfbg.fillRect(780-xx,608-h,20,h)
		}
		sfbg.fillStyle='#2F24'
		for(;xx<512;xx+=32){
			sfbg.fillRect(xx,0,20,Math.random()*200)
			h=Math.random()*200;sfbg.fillRect(780-xx,608-h,20,h)
		}
		sfbg.fillStyle='#2FF4'
		for(;xx<672;xx+=32){
			sfbg.fillRect(xx,0,20,Math.random()*200)
			h=Math.random()*200;sfbg.fillRect(780-xx,608-h,20,h)
		}
		sfbg.fillStyle='#F224'
		for(;xx<800;xx+=32){
			sfbg.fillRect(xx,0,20,Math.random()*200)
			h=Math.random()*200;sfbg.fillRect(780-xx,608-h,20,h)
		}
	}
}
if(EF.length>7){
	let s=scr
	s.fillStyle=EF[8];s.strokeStyle=EF[8];
	s.globalAlpha=EF[5];
	switch(EF[0]){
		case 0:s.beginPath();BKEF.forEach(o=>o.draw0());s.stroke();break;
		case 1:s.beginPath();BKEF.forEach(o=>o.draw1());s.stroke();break;
		case 2:BKEF.forEach(o=>o.draw2());break;
		case 3:s.beginPath();BKEF.forEach(o=>o.draw1());s.fill();break;
		case 4:case 5:case 6:
			s.imageSmoothingEnabled=true;
			BKEF.forEach(o=>o.draw3());
			s.imageSmoothingEnabled=false;
		break;
	}
	s.globalAlpha=1;
}
}
function drawEf(){
	if(saveEf.length>0 && saveEf[0].al<=0)saveEf=[]
	draw_obj(saveEf,0,true)
	if(FlashSpd!==0){
		if(FlashAl>toFlAl){FlashAl-=FlashSpd}
		else if(FlashAl<toFlAl){FlashAl+=FlashSpd}
		else{FlashSpd=0}
	}
	if(FlashAl>0){
		scr.globalAlpha=FlashAl/100;
		scr.fillStyle=FlashCol;
		scr.fillRect(ViewX,ViewY,800,608);
		scr.globalAlpha=1;
	}
	if(WarpR){
		scr.save();
		scr.translate(ViewX+400,ViewY+304);
		scr.rotate(WarpR.ang);
		scr.fillStyle='#000';
		scr.fillRect(-WarpR.sc*1.32,-WarpR.sc,WarpR.sc*1.32<<1,WarpR.sc<<1);
		scr.restore();
	}
}
function EFSprinit(){
	for(var i=sprPt.length;i--;){
		let c=document.createElement('canvas');
		c.width=64;c.height=64;
		let cc=c.getContext('2d');
		cc.drawImage(sprPt[i],0,0);
		cc.globalCompositeOperation='source-in';
		cc.fillStyle=EF[8];
		cc.fillRect(0,0,64,64);
		sprPt[i]=c;
	}
}