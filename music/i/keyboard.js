window.AudioContext=window.AudioContext||window.webkitAudioContext;
audio=new AudioContext();
const objKey=class{
	constructor(o,i){
		o.p=this;o.onmousedown=function(){this.p.play()};
		o.ontouchstart=function(){this.p.play()};
		o.ontouchend=function(e){if(e)e.preventDefault();this.p.release()};
		this.html=o;this.wb=o.className;this.ind=i;
		this.onplay=false;this.note=[];this.onload=false;
	}
	play(){
		if(!this.onload||this.onplay)return;
		this.html.className=this.wb+' on down';
		this.onplay=true;
		this.vol=audio.createGain();
		this.vol.connect(conv[this.ind]);
		this.vol.connect(audioVol[this.ind]);
		this.on=audio.createBufferSource();
		this.on.buffer=keyboard[this.ind].src[0];
		this.on.connect(this.vol);
		this.vol.gain.setValueAtTime(0,audio.currentTime);
		this.vol.gain.linearRampToValueAtTime(1,audio.currentTime+0.03);
		this.on.playbackRate.value=this.rate;
		this.on.start(audio.currentTime,this.offset,3.95);
	}
	release(){
		if(!this.onload||!this.onplay)return;
		this.html.className=this.wb+' on';
		this.onplay=false;
		this.vol.gain.setValueAtTime(1,audio.currentTime);
		this.vol.gain.linearRampToValueAtTime(0,audio.currentTime+0.2);
		this.on.stop(audio.currentTime+0.2);
	}
}
keyboard=$$('keyboard');
audioVol=[];conv=[];convVol=[];config=[];
for(let i=keyboard.length;i--;){
	keyboard[i].innerHTML+=`<div class='wk'></div><div class='bk'></div><div class='wk'></div>`;
	for(let j=7;j--;){
		for(let k=2;k--;){
			keyboard[i].innerHTML+=`<div class='wk'></div><div class='bk'></div>`;
		}
		keyboard[i].innerHTML+=`<div class='wk'></div>`;
		for(let k=3;k--;){
			keyboard[i].innerHTML+=`<div class='wk'></div><div class='bk'></div>`;
		}
		keyboard[i].innerHTML+=`<div class='wk'></div>`;
	}
	keyboard[i].innerHTML+=`<div class='wk'></div>`;
	keyboard[i].key=[];keyboard[i].src=[];
	for(let j=0;j<88;j++){
		keyboard[i].key.push(new objKey(keyboard[i].children[j],i));
	}
	keyboard[i].nb=$$('knob',keyboard[i].parentNode);
	keyboard[i].nb[0].t=0;keyboard[i].nb[0].ind=i;
	keyboard[i].nb[1].t=1;keyboard[i].nb[1].ind=i;
	audioVol[i]=audio.createGain();
	conv[i]=audio.createConvolver();
	convVol[i]=audio.createGain();
	convVol[i].gain.value=0.8;
	conv[i].connect(convVol[i]);
	convVol[i].connect(audioVol[i]);
	audioVol[i].connect(audio.destination);
	audioVol[i].gain.value=0.8;
	config[i]={v:0.8,c:0.8};
}
var xml=new XMLHttpRequest();
xml.responseType='arraybuffer';
xml.open('GET','../Hall.ogg',true);
xml.onload=()=>{audio.decodeAudioData(xml.response,r=>{conv.forEach(o=>{o.buffer=r});},()=>{})};
xml.send();
window.onmouseup=function(){
	for(let i=keyboard.length;i--;){
		keyboard[i].key.forEach(o=>{if(o.onplay)o.release();})
	}
}
function load(ind,f,o){
	var xml=new XMLHttpRequest();
	xml.responseType='arraybuffer';
	xml.open('GET',f,true);
	xml.onload=function(){
		audio.decodeAudioData(
		xml.response,function(r){
			keyboard[ind].src.push(r);
			for(let i=o.length;i--;){
				for(var j=o[i][1];j<=o[i][2];j++){
					keyboard[ind].key[j].rate=Math.pow(2,(j-o[i][0])/12)+0.008;
					keyboard[ind].key[j].onload=true;
					keyboard[ind].key[j].html.className+=' on';
					keyboard[ind].key[j].offset=i*4+0.01;
				}
			}
		},function(){}
	)}
	xml.send();
}
knob=$$('knob');
for(let i=knob.length;i--;){
	knob[i].val=0.8;
	knob[i].className='knob s2';
	knob[i].onmousedown=function(e){
		var a=(((e.clientX-this.getBoundingClientRect().left)>this.offsetWidth/2)-0.5)/5;
		var v=Math.max(0,Math.min(1.6,this.val+a));this.val=v;
		this.firstChild.style.transform=`rotate(${Math.min(270,v/1.6*270)}deg)`;
		this.className='knob '+(v<0.05?'s0':v<0.55?'s1':v<1.05?'s2':'s3');
		if(this.t){convVol[this.ind].gain.value=v;}
		else{audioVol[this.ind].gain.value=v;}
	}
	knob[i].ontouchend=e=>e.preventDefault();
}
keymap=[];keyS=[];
document.onkeydown=function(e){
	if(e){
		if(e.keyCode===32){
			if(PLAY){
				clearInterval(PLAY);PLAY=0;
			}else{
				pl();
			}
			return;
		}
		var p;
		for(var n=0;n<keyS.length;n++){
			p=-1;
			keymap[n].forEach((o,i)=>{
				if(Array.isArray(o)){
					if(o[0]===e.keyCode||o[1]===e.keyCode)p=i;
				}else{
					if(o===e.keyCode)p=i;
				}
			})
			if(p!==-1){
				keyboard[n].key[p+keyS[n]].play();
			}
		}
	}
}
document.onkeyup=function(e){
	if(e){
		var p;
		for(var n=0;n<keyS.length;n++){
			p=-1;
			keymap[n].forEach((o,i)=>{
				if(Array.isArray(o)){
					if(o[0]===e.keyCode||o[1]===e.keyCode)p=i;
				}else{
					if(o===e.keyCode)p=i;
				}
			})
			if(p!==-1){
				keyboard[n].key[p+keyS[n]].release();
			}
		}
	}
}
sc=[];nowk=[];
ind=0;PLAY=0;
function play(i){
	var s=sc[i];
	if(s[ind]===undefined)return;
	var n=s[ind]+s[ind+1],len=2;
	while(s[ind+len]===' ')len++;
	if(keyboard[i%5].key[n]){
		nowk[i]=keyboard[i%5].key[n];
		nowk[i].play();
		setTimeout(`nowk[${i}].release()`,len*time-10);
	}
}