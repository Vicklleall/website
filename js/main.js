var ROOT=location.pathname.charAt(1);
document.body.innerHTML+=`<div id='BG'></div><div id='fixed'><div id='head'></div><div id='nav'><p>主页</p><p>文章</p><p>游戏</p><p>音乐</p><p>工具</p><p>下载</p><div id='hide'><span></span></div></div><div id='nowBorder'></div></div>`;
function $(id){return document.getElementById(id)}
function $$(cn,o){o=o||document;return o.getElementsByClassName(cn)}
function TAG(t,o){return o.getElementsByTagName(t)}
function css(o,s,ei=-1,ss){
	if(!o)return;
	if(o.length){
		for(var i=o.length;i--;){if(!o[i])continue;Object.assign(o[i].style,i===ei?ss:s);}
	}else{Object.assign(o.style,s)}
}
function openURL(url,tab){
	var a=document.createElement('a');
	a.style.display='none';if(tab)a.target='_blank';
	a.href=url;document.body.appendChild(a);
	a.click();document.body.removeChild(a);
}
(function(){
var hide=false;
var nav=$('nav');
$('hide').onclick=function(){
	hide=!hide;
	if(hide){
		this.firstChild.style.transform='translateX(-30%) translateY(-50%) rotate(180deg)';
		nav.style.width='64px';$('nowBorder').style.width='64px';
		css($('main'),{left:'68px',marginTop:'0'})
		$('head').className='hide';
	}else{
		this.firstChild.style.transform='';
		nav.style.width='';$('nowBorder').style.width='';
		css($('main'),{left:'',marginTop:''})
		$('head').className='';
	}
}
navBtn=TAG('p',nav);var ind=0;
switch(ROOT){
	case 'p':ind=1;break;case 'g':ind=2;break;case 'm':ind=3;break;case 't':ind=4;break;case 'd':ind=5;break;
}
navBtn[ind].id='now';
$('nowBorder').style.top=`${ind*57}px`;
navBtn[0].url='/';
navBtn[1].url='/p/';
navBtn[2].url='/game/';
navBtn[3].url='/music/';
navBtn[4].url='/tool/';
navBtn[5].url='/download/';
for(let i=navBtn.length;i--;){
	navBtn[i].onclick=function(){openURL(this.url)};
}
if(screen.availWidth){
	document.body.style.zoom=Math.max(100,screen.availWidth/12.8)+'%';
}
window.onresize=function(){
	if(screen.availWidth){
		document.body.style.zoom=Math.max(100,screen.availWidth/12.8)+'%';
	}
}
})();
var _hmt=_hmt||[];(function(){var hm=document.createElement('script');hm.src='https://hm.baidu.com/hm.js?2638514c2f9430119fe5f7e443cde785';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(hm,s)})();