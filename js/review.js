$('RT').innerHTML+=`<div id='review' class='S'><h4>评论</h4><textarea id='rText' placeholder='说点儿什么吧...'></textarea><div id='post'><div id='emoji'>😀</div><input id='rName' maxlength='20' placeholder='这里填名称...'/><div id='ok'>发表</div></div><ul id='emojiS' class='S'><span>😀</span><span>🙂</span><span>🙃</span><span>😂</span><span>😮</span><span>🤔</span><span>🙄</span><span>😏</span><span>👍</span><span>（￣▽￣）</span><span>(=・ω・=)</span><span>(〜￣△￣)〜</span><span>←◡←</span><span>("▔□▔)/</span><span>（￣へ￣）</span><span>(╯°口°)╯</span><span>(･∀･)</span><span>_(:3」∠)_</span></ul><div id='rArea'></div></div>`;
function appendReview(o){
	var a=document.createElement('div');
	a.innerHTML=`<p></p><pre></pre>`
	a.firstChild.innerText=o.get('Name');
	a.firstChild.innerHTML+=`<span>${o.createdAt.getFullYear()}-${o.createdAt.getMonth()+1}-${o.createdAt.getDate()} ${String(o.createdAt.getHours()).padStart(2,'00')}:${String(o.createdAt.getMinutes()).padStart(2,'00')}</span>`;
	a.lastChild.innerText=o.get('Words');
	$('rArea').insertBefore(a,$('rArea').firstChild);
	$('rArea').style.display='block';
}
(function(){
	var post=$('ok');
	css(post,{background:'#CCC',cursor:'auto'});
	var rText=$('rText');
	rText.oninput=function(){
		if(rText.value.replace(/ /g,'')&&rName.value.replace(/ /g,'')){
			css(post,{background:'',cursor:''});
		}else{
			css(post,{background:'#CCC',cursor:'auto'});
		}
	}
	var rName=$('rName');
	rName.oninput=rText.oninput;
	var query=new AV.Query('RHome');
	query.equalTo('URL',location.pathname);
	query.ascending('createdAt');
	query.find().then(function(r){r.forEach(o=>{appendReview(o);})});
	post.onclick=function(){
		if(!this.style.cursor){
			css(this,{background:'#CCC',cursor:'auto'});
			var rv=new AV.Object('RHome');
			rv.set('Name',rName.value);
			rv.set('Words',rText.value);
			rv.set('URL',location.pathname);
			rv.save().then(o=>{
				appendReview(o);rName.value='';rText.value='';
			},e=>{
				console.log(e);alert('发表失败！');
				css(this,{background:'',cursor:''});
			});
		}
	}
	var emoji=$('emoji');var emojiS=$('emojiS');
	emoji.onclick=function(){
		if(emojiS.style.opacity){
			css(emojiS,{opacity:'',transform:'',pointerEvents:''})
		}else{
			css(emojiS,{opacity:1,transform:'translateY(0)',pointerEvents:'auto'});
		}
	}
	var emojiI=TAG('span',emojiS);
	for(let i=emojiI.length;i--;){
		emojiI[i].onclick=function(){
			rText.value+=this.innerText;rText.oninput();
		}
	}
})();